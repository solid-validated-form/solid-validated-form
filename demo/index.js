import { render } from 'solid-js/web'
import ValidatedForm from 'solid-validated-form'

function App () {
  const F = ValidatedForm({
    firstName (value) {
      if (value.length < 3 || value.length > 40) { return 'must be between 3 and 40 characters' }
    },
    lastName (value) {
      if (value.length < 3 || value.length > 40) { return 'must be between 3 and 40 characters' }
    },
    aboutMe (value) {
      if (value.length > 200) return 'is too long (maximum is 200 characters)'
    },
    country (value) {
      if (value === '') return "can't be blank"
    },
    gender (value) {
      if (value === '') return "can't be blank"
    }
  }).configure({
    errorCssClass: 'invalid-feedback',
    groupCssClass: 'mb-3',
    inputCssClass: 'form-control',
    invalidCssClass: 'is-invalid',
    labelCssClass: 'form-label'
  })

  return (
    <F.Form class="m-3" style="width: 450px;">
      <F.Text name="firstName" />

      <F.Text name="lastName" />

      <F.Textarea name="aboutMe" rows="4" placeholder="Optional..." />

      <F.Select
        name="country"
        options={{
          '': 'Please choose...',
          ca: 'Canada',
          us: 'United State of America',
          zz: 'Other'
        }}
      />

      <F.RadioList
        id="gender"
        name="gender"
        options={{
          male: 'Male',
          female: 'Female'
        }}
        classes={{
          component: {
            group: 'form-check',
            input: 'form-check-input',
            label: 'form-check-label'
          }
        }}
      />

      <F.Submit class="btn btn-primary" label="Save" />
      <F.Reset class="btn btn-light ms-3" />
    </F.Form>
  )
}

render(() => <App />, document.getElementById('app'))

// ---
// Injecting CSS
// ---

const href =
  'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css'
document
  .getElementsByTagName('head')[0]
  .appendChild(
    Object.assign(document.createElement('link'), { href, rel: 'stylesheet' })
  )
