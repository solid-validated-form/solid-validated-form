
- [Input States](#input-states)
  - [Dirty State](#dirty-input)
  - [Invalid State](#invalid-input)
- [Form States](#form-states)
  - [Dirty State](#dirty-state)
  - [Dirty Signal](#dirty-signal)
  - [Errors State](#errors-state)
  - [Valid Signal](#valid-signal)
  - [Values State](#values-state)
- [Internal Commit State](#internal-commit-state)
- [Partial Form Saving](#partial-form-saving)

## Input States

Inputs have two implicit states which manages certain CSS classes.

### Dirty Input

Inputs are deemed dirty when their current value differs from the previously saved. If configured, the [`dirtyCssClass`](tutorial-2-configuring.html#global-dirty-class) will be added to the input. The saved value can be updated using the [`save`](ValidatedForm.html#save) method or a [`Submit`](ValidatedForm.html#Submit) button.

### Invalid Input

Inputs have the [`invalidCssClass`](tutorial-2-configuring.html#global-invalid-class) added to them when their validation fails.

## Form States

Forms have access to three states for their inputs: [`dirty`](#dirty-state), [`errors`](#errors-state), and [`values`](#values-state). Forms also have a [`isDirty`](#dirty-signal) and [`isValid`](#valid-signal) signal.

### Dirty State

```js
// Example state
F.dirty == {
  password: {
    old: '',
    new: '********'
  },
  nickname: {
    old: '',
    new: 'root'
  }
}
```

The [`dirty`](ValidatedForm.html#dirty) state is linked to an internal commit state that contains the form's default values or whatever values last set by [`save`](ValidatedForm.html#save). Keys are only added to the [`dirty`](ValidatedForm.html#dirty) state when input values differ from their committed state. The changes are provided as an object containing both the old and new value(e.g. `{ old: '', new: 'user@domain.tld' }`).

There are two ways of updating the commit state. The first and easiest being [`Submit`](ValidatedForm.html#Submit) which will save the commit state when the form is valid. The other option is using the [`save`](ValidatedForm.html#save) method and a button which will save the form regardless of its validity. If there's a need to check the current commit state, it can be done with a combination of [`dirty`](#dirty-state) and [`values`](#values-state)(see [Internal Commit State](#internal-commit-state)).

Calling [`reset`](ValidatedForm.html#reset) or using the [`Reset`](ValidatedForm.html#Reset) button will discard all changes. This includes inputs that are otherwise valid. Users wanting to save valid inputs and reset invalid ones can do so with [`dirty`](#dirty-state), [`errors`](#errors-state), and [`save`](ValidatedForm.html#save)(see [Partial Form Saving](#partial-form-saving)).

### Dirty Signal

*(<ins>Since v1.3.0</ins>)*  
If any input is deemed dirty then the signal returns `true` otherwise it returns `false`.

```tsx
<Show when={F.isDirty}>You have unsaved changes</Show>
```

### Errors State

```js
// Example state
F.errors == {
  username: 'is not an email address',
  password: 'is required'
}
```

Validation happens on...

- inputs using the [`onBlur`](tutorial-3-inputs.html#input-onblur) and [`onChange`](tutorial-3-inputs.html#input-onchange) events,
- forms using the [`onSubmit`](tutorial-4-components.html#form-onsubmit) event and a [`Submit`](ValidatedForm.html#Submit) button,
- and/or using the [`validate`](ValidatedForm.html#validate) method.

Whenever an input fails validation, the [`errors`](#errors-state) state will contain the name of invalid inputs as keys and the first error they encountered as values. Keys are added to the state when an input becomes invalid and they are removed when the input becomes valid.

### Valid Signal

*(<ins>Since v1.3.0</ins>)*  
If any input is deemed invalid then the signal returns `false` otherwise it returns `true`.

```tsx
<Show when={!F.isValid}>Please correct the errors below</Show>
```

### Values State

```js
// Example state
F.values == {
  username: 'user@domain.tld',
  password: '********',
  nickname: 'root'
}
```

The values of inputs is always available through the [`values`](ValidatedForm.html#values) state. This state is primarily updated using the [`onKeyUp`](tutorial-3-inputs.html#input-onkeyup) event on inputs which simply copies `event.target.value`. It's also updated using the [`onBlur`](tutorial-3-inputs.html#input-onblur) and [`onChange`](tutorial-3-inputs.html#input-onchange) events which transform values before setting them.

## Internal Commit State

ValidatedForm instances don't have access to the internal commit state but, since the dirty state is based off of the commit state, the internal commit state can easily be derived with a bit of logic. Simple implementation of commit state logging.

```js
const F = ValidatedForm({
  username (value) {
    if (value === '') return 'is required'
  },
  nickname () {
    // Accept any value
  }
})

// Outputs the commit state to console
const log = () => {
  const state = {}

  Object.keys(F.values).forEach((name) => {
    // Note we're accessing the old value of dirty
    state[name] = F.dirty[name] ? F.dirty[name].old : F.values[name]
  })

  console.log('current commit state:')
  console.dir(state)
}
```

```tsx
<F.Form>
  <F.Text name='username' />
  <F.Text name='nickname' />
  <F.Submit />
  <F.Reset />
  <F.Button onClick={log} label='Log State' />
</F.Form>
```

## Partial Form Saving

Simple implementation of partial form saving.

```js
const F = ValidatedForm({
  username (value) {
    if (value === '') return 'is required'
  },
  nickname () {
    // Accept any value
  }
})

// Saves all the dirty inputs that are valid
const save = () => {
  Object.keys(F.dirty).forEach((name) => {
    // Saving one input at a time
    if (!F.errors[name]) F.save(name)
  })
}
```

```tsx
<F.Form>
  <F.Text name='username' />
  <F.Text name='nickname' />
  <F.Submit />
  <F.Reset />
  <F.Button onClick={save} label='Save' />
</F.Form>
```
