
- [`clear`](#clear(...inputs))
- [`configure`](#configure(settings))
- [`fresh`](#fresh())
- [`get`](#get(...inputs))
- [`id`](#id(name))
- [`reset`](#reset(...inputs))
- [`save`](#save(...inputs))
- [`set`](#set(input%2C-value))
- [`validate`](#validate(...inputs))

### `clear(...inputs)`

Clears the form inputs and errors. This is not the same as [`reset`](#reset(...inputs)) or [`fresh`](#fresh()).

Clearing the entire form:

```js
F.clear()
```

Clearing a single input:

```js
F.clear('username')
```

Clearing multiple inputs:

```js
F.clear('username', 'nickname')
```

### `configure(settings)`

Configures the form settings.

These are not meant to be reactive attributes.

This method also exists on the [ValidatedForm.globals](ValidatedForm.html#.globals) object to help configure global settings as well.

Configuring all forms:

```js
ValidatedForm.globals.configure({
  inputCssClass: 'form-control'
})
```

Configuring all forms manually:

```js
ValidatedForm.globals.inputCssClass = 'form-control'
```

Configuring a single form on creation:

```js
const F = ValidatedForm({
  username () {}
}).configure({
  id: 'custom-form-id'
})
```

Configuring a form at a later time:

```js
F.configure({
  id: 'custom-form-id'
})
```

Configuring a form manually:

```js
F.instance.id = 'custom-form-id'
```

### `fresh(initialValues?)`

Sets a form's initial values and/or resets the form to said values. This is not the same as [reset](#reset(...inputs)) or [clear](#clear(...inputs)).

Basic form with default starting values:

```js
const F = ValidatedForm({
  username () {},
  nickname () {}
}, {
  username: 'user@domain.tld'
})
```

Resets username to "user@domain.tld" and nickname to "":

```js
F.fresh()
```

Initial values passed to [ValidatedForm](ValidatedForm.html) can be overridden using the optional argument:

```js
F.fresh({ username: '', nickname: 'root' })

// Now reset username to "" and nickname to "root"
F.fresh()
```

### get(...inputs)

Returns one or more values for the given input names.

Values can also be accessed using the [`values`](tutorial-5-state.html#values-state) state.

Getting all form inputs and values:

```js
// { username: 'user@domain.tld', password: '********', nickname: '' }
F.get()
```

Getting a single input value:

```js
// { username: 'user@domain.tld' }
F.get('username')
```

Getting multiple input values:

```js
// { username: 'user@domain.tld', nickname: '' }
F.get('username', 'nickname')
```

### `id(name?)`

Helper function to generate the input ID for a given input name.

Getting the form's ID:

```js
// 'f-00000'
F.id()
F.instance.id
```

Getting an input's ID:

```js
// 'f-00000-username'
F.id('username')
F.instance.id + '-username'
```

### `reset(...inputs)`

Resets the form data to the previously committed one. This is not the same as [clear](#clear(...inputs)) or [fresh](#fresh()).

To persist data between resets, use either [Submit](ValidatedForm.html#Submit) or [save](#save(...inputs)).

Resetting the entire form:

```js
F.reset()
```

Resetting a single input:

```js
F.reset('username')
```

Resetting multiple inputs:

```js
F.reset('username', 'nickname')
```

### `save(...inputs)`

Saves the state of the form or a set of inputs for resetting forms.

Note that this function does not run validation and will save invalid inputs.

Saving the entire form:

```js
F.save()
```

Saving a single input:

```js
F.save('username')
```

Saving multiple inputs:

```js
F.save('username', 'nickname')
```

### `set(input, value)`

Sets a value or a set of values on related inputs.

This will trigger a dirty computation and the [`dirtyCssClass`](tutorial-2-configuring.html#global-dirty-class) to be added.

Since the value argument is passed onto `setState`, it can also be a function that returns the new value. The same cannot be done by using an input object.

Using set with the value as a string:

```js
F.set('username', 'user@domain.tld')
```

Using set with the value as a function:

```js
F.set('username', (name) => name.toLowerCase())
```

Using set with the input as an object:

```js
F.set({ username: 'user@domain.tld' })
```

### `validate(...inputs)`

Validates the form or a set of inputs.

Validating the entire form:

```js
F.validate()
```

Validating a single input:

```js
F.validate('username')
```

Validating multiple inputs:

```js
F.validate('username', 'nickname')
```
