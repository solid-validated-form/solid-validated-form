These are not meant to be reactive properties.

- [Global Configuration](#global-configuration)
  - [CSS Classes](#global-css-classes)
    - [Dirty Class](#global-dirty-class)
    - [Error Class](#global-error-class)
    - [Group Class](#global-group-class)
    - [Input Class](#global-input-class)
    - [Invalid Class](#global-invalid-class)
    - [Label Class](#global-label-class)
   - [Prevent Defaults](#global-prevent-defaults)
     - [Prevent Reset](#global-prevent-reset)
     - [Prevent Submit](#global-prevent-submit)
   - [Validate Save](#global-validate-save)
   - [Component Visibility](#global-component-visibility)
     - [Visible Labels](#global-visible-labels)
     - [Visible Errors](#global-visible-errors)
- [Instance Configuration](#instance-configuration)
 - [Disabling Inheritance](#disabling-inheritance)
   - [Via Instance Configurations](#disabling-via-instance-configurations)
   - [Via Component Properties](#disabling-via-component-properties)

## Global Configuration

Form configuration for all ValidatedForm instances. Overriding these properties will change the configuration for ***all*** validated forms and their inputs.

| Configuration                              | Type           | Default     |
| ------------------------------------------ | -------------- | ----------- |
| [`dirtyCssClass`](#global-dirty-class)     | `string|false` | `false`     |
| [`errorCssClass`](#global-error-class)     | `string|false` | `"error"`   |
| [`groupCssClass`](#global-group-class)     | `string|false` | `false`     |
| [`inputCssClass`](#global-input-class)     | `string|false` | `false`     |
| [`invalidCssClass`](#global-invalid-class) | `string|false` | `"invalid"` |
| [`labelCssClass`](#global-label-class)     | `string|false` | `false`     |
| [`preventReset`](#global-prevent-reset)    | `boolean`      | `true`      |
| [`preventSubmit`](#global-prevent-submit)  | `boolean`      | `true`      |
| [`validateSave`](#global-validate-save)    | `boolean`      | `true`      |
| [`visibleErrors`](#global-visible-errors)  | `boolean`      | `true`      |
| [`visibleLabels`](#global-visible-labels)  | `boolean`      | `true`      |

### Global CSS Classes

Configuring classes globally makes it easy to incorporate CSS libraries and paradigms.

#### Global Dirty Class

**Type:** `string|false`  
**Default:** `false`

CSS class for dirty `<input>` elements. The class is added when an input's state differs from its reset value. The dirty class can also be disabled by setting it to `false`.

```js
ValidatedForm.globals.dirtyCssClass = 'is-dirty'
```

```tsx
<F.Text name='username' />
```

```html
<!-- Generated DOM when inputs are dirty -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" type="text" name="username" class="is-dirty" />
```

This class is also applied to the group `<div>` if grouping is enabled.

```tsx
<F.Text name='username' group='mb-3' />
```

```html
<!-- Generated DOM when inputs are dirty -->
<div class="mb-3 is-dirty">
  <label for="f-00000-username">Username</label>
  <input id="f-00000-username" type="text" name="username" class="is-dirty" />
</div>
```

#### Global Error Class

**Type:** `string|false`  
**Default:** `"error"`

CSS class for error `<span>` elements that display the error messages when validation fails. The error class can also be disabled by setting it to `false`.

```js
ValidatedForm.globals.errorCssClass = 'invalid-feedback'
```

```html
<!-- Generated DOM when validation fails -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" type="text" name="username" class="invalid" />
<span class="invalid-feedback">Username is required</span>
```

#### Global Group Class

**Type:** `string|false`  
**Default:** `false`

CSS class for wrapping all input components.

```js
ValidatedForm.globals.groupCssClass = 'mb-3'
```

```html
<!-- Generated group DOM -->
<div class="mb-3">
  <label for="f-00000-username">Username</label>
  <input id="f-00000-username" name="username" type="text" />
</div>
```

```html
<!-- Generated group DOM when inputs are invalid -->
<div class="mb-3 invalid">
  <label for="f-00000-username">Username</label>
  <input id="f-00000-username" name="username" type="text" class="invalid" />
  <span class="error">Username is required</span>
</div>
```

#### Global Input Class

**Type:** `string|false`  
**Default:** `false`

CSS class for all inputs.

```js
ValidatedForm.globals.inputCssClass = 'form-control'
```

```html
<!-- Generated DOM -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" name="username" type="text" class="form-control" />
```

#### Global Invalid Class

**Type:** `string|false`  
**Default:** `"invalid"`

CSS class for invalid inputs. The invalid class is also added to the wrapping div if a group class was specified.

```js
ValidatedForm.globals.invalidCssClass = 'is-invalid'
```

```html
<!-- Generated DOM when inputs are invalid -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" name="username" type="text" class="is-invalid" />
<span class="error">Username is required</span>
```

```html
<!-- Generated group DOM when inputs are invalid -->
<div class="custom-group-class is-invalid">
  <label for="f-00000-username">Username</label>
  <input id="f-00000-username" name="username" type="text" class="is-invalid" />
  <span class="error">Username is required</span>
</div>
```

#### Global Label Class

**Type:** `string|false`  
**Default:** `false`

CSS class for labels.

```js
ValidatedForm.globals.labelCssClass = 'form-label'
```

```html
<!-- Generated DOM -->
<label for="f-00000-username" class="form-label">Username</label>
<input id="f-00000-username" name="username" type="text" />
```

### Global Prevent Defaults

Calls `event.preventDefault()` on certain form events.

#### Global Prevent Reset

**Type:** `boolean`  
**Default:** `true`

Prevent forms from clearing inputs when using `<button type=reset />`.

```js
ValidatedForm.globals.preventReset = false

const F = ValidatedForm({
  username () {}
})

const reset = (event) => {
  // Manually call event.preventDefault() or the reset behaviour will be inconsistent
}
```

```tsx
<F.Form onReset={reset}>
  <F.Text name='username' />
  <F.Reset />
  <F.Submit />
</F.Form>
```

#### Global Prevent Submit

**Type:** `boolean`  
**Default:** `true`

Prevent forms from submitting when using `<button type=submit />`.

```js
ValidatedForm.globals.preventSubmit = false

const F = ValidatedForm({
  username () {}
})

const submit = (event) => {
  // Manually call event.preventDefault() or the form will redirect
}
```

```tsx
<F.Form onSubmit={submit}>
  <F.Text name='username' />
  <F.Reset />
  <F.Submit />
</F.Form>
```

### Global Validate Save

**Type:** `boolean`  
**Default:** `true`

Requires all inputs to be valid before the form state is saved.

```js
ValidatedForm.globals.validateSave = false

const F = ValidatedForm({
  username (value) {
    if (value === '') return 'is required'
    if (value.length < 4) return 'must contain at least 4 characters'
  }
})
```

```tsx
<F.Form>
  <F.Text name='username' />
  <F.Reset />
  <F.Submit />
</F.Form>
```

### Global Component Visibility

The `<label>` and error `<span>` can be disabled.

#### Global Visible Labels

**Type:** `boolean`  
**Default:** `true`

Enable or disable the rendering of `<label>` elements.

```html
<!-- Default DOM -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" type="text" name="username" />
```

```js
ValidatedForm.globals.visibleLabels = false
```

```html
<!-- Generated DOM when labels are disabled -->
<input id="f-00000-username" type="text" name="username" />
```

#### Global Visible Errors

**Type:** `boolean`  
**Default:** `true`

Enables or disables the rendering of error `<span>` elements.

```html
<!-- Default DOM when validation fails -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" type="text" name="username" class="invalid" />
<span class="error">Username is required</span>
```

```js
ValidatedForm.globals.visibleErrors = false
```

```html
<!-- Generated DOM when errors are disabled -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" type="text" name="username" class="invalid" />
```

## Instance Configuration

Form configuration for a single instance. These override the [Global Configuration](#global-configuration).

All of the global configuration options are also available as instance configuration options.

Setting a custom input CSS class using the `configure` method:

```js
const F = ValidatedForm({
  username () {}
}).configure({
  inputCssClass: 'custom-input-class'
})
```

Using the `instance` properties:

```js
const F = ValidatedForm({
  username () {}
})

F.instance.inputCssClass = 'custom-input-class'
```

```html
<!-- Generated DOM -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" name="username" type="text" class="custom-input-class" />
```

### Disabling Inheritance

Any [Global Configuration](#global-configuration) can be disabled using an instance configuration or the right component properties.

```js
ValidatedForm.globals.groupCssClass = 'global-group-class'
```

```tsx
<F.Text name='username' />
```

```html
<!-- Generated DOM with global configuration -->
<div class="global-group-class">
  <label for="f-00000-username">Username</label>
  <input id="f-00000-username" name="username" type="text" />
</div>
```

#### Disabling Via Instance Configurations

Disabling global inheritance can be done per form using the [`configure`](ValidatedForm.html#configure) method and [`instance`](ValidatedForm.html#~InstanceConfiguration) properties.

Disabling group inheritance using the `configure` method:

```js
const F = ValidatedForm({
  username () {}
}).configure({
  groupCssClass: false
})
```

Using `instance` properties:

```js
const F = ValidatedForm({
  username () {}
})

F.instance.groupCssClass = false
```

```html
<!-- Generated DOM with instance configuration -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" name="username" type="text" />
```

#### Disabling Via Component Properties

Certain inheritance can also be disabled using the right JSX component properties. Not all of the configuration options are available as component properties.

Disabling group inheritance using the `group` component property:

```tsx
<F.Text name='username' group={false} />
```

```html
<!-- Generated DOM with component properties -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" name="username" type="text" />
```
