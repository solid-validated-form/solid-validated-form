
- [Forms](#forms)
  - [`classList`](#form-classlist)
  - [`id`](#form-id)
  - [`values`](#form-values)
- [Form Events](#form-events)
  - [`onReset`](#form-onreset)
  - [`onSubmit`](#form-onsubmit)
  - [`preventReset`](#form-preventreset)
  - [`preventSubmit`](#form-preventsubmit)
- [Buttons](#buttons)
- [Labels](#labels)
- [Errors](#errors)
- [Groups](#groups)

## Forms

### Form `classList`

The `classList` property of the form inherits both the [`dirtyCssClass`](tutorial-2-configuring.html#global-dirty-class) and the [`invalidCssClass`](tutorial-2-configuring.html#global-invalid-class) based on the overall form state. If any form input is dirty and the [`dirtyCssClass`](tutorial-2-configuring.html#global-dirty-class) is configured, the `<form>` tag will receive the specified class. Same goes for the [`invalidCssClass`](tutorial-2-configuring.html#global-invalid-class) depending if any of the form inputs are invalid.

### Form `id`

All forms have a unique ID. It's a randomly generated, 5 character, hexadecimal based ID prefix with `f-`. This can be overridden using the [`configure`](ValidatedForm.html#configure) method, by setting the [`instance`](ValidatedForm.html#instance) configuration's `id` property manually, or by specifying one via the [`Form`](ValidatedForm.html#Form) component's `id`.

Setting an ID using the `configure` method:

```js
const F = ValidatedForm({
  username () {}
}).configure({
  id: 'custom-form-id'
})
```

Using the `instance` properties:

```js
const F = ValidatedForm({
  username () {}
})

F.instance.id = 'custom-form-id'
```

Setting an ID on the form component:

```tsx
<F.Form id='custom-form-id'>
  <F.Text name='username' />
</F.Form>
```

```html
<!-- Generated DOM -->
<form id="custom-form-id">
  <label for="custom-form-id-username">Username</label>
  <input id="custom-form-id-username" name="username" />
</form>
```

### Form `values`

*(<ins>Since v1.3.0</ins>)*  
Initial values can be specified using the `values` property on the [Form](ValidatedForm.html#Form) component. This, in combination with [resources](https://www.solidjs.com/docs/latest/api#createresource), will trigger [Suspense](https://www.solidjs.com/docs/latest/api#%3Csuspense%3E) to fallback.

Passing an object of strings as initial values:

```tsx
<F.Form values={{ username: 'user@domain.tld' }}>
  <F.Text name='username' />
</F.Form>
```

Using [resources](https://www.solidjs.com/docs/latest/api#createresource) and [Suspense](https://www.solidjs.com/docs/latest/api#%3Csuspense%3E):

```js
// Mock resource that emulates a two second API call
const [payload] = createResource(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve({ username: 'user@domain.tld' }), 2000)
  })
})
```

```tsx
<Suspense fallback={<div>Loading...</div>}>
  <F.Form values={payload()}>
    <F.Text name='username' />
  </F.Form>
</Suspense>
```

## Form Events

Unlike input events, form events are forwarded **before** they change the state.
The form's [`onReset`](#form-onreset) and [`onSubmit`](#form-onsubmit) events call `event.preventDefault()`. This can be disabled globally or per instance with the [`preventReset`](#form-preventreset) and [`preventSubmit`](#form-preventsubmit) configurations respectively. Note that disabling this can lead to unexpected behaviour with the [`Reset`](ValidatedForm.html#Reset) and [`Submit`](ValidatedForm.html#Submit) buttons.

### Form `onReset`

Calls [`reset`](ValidatedForm.html#reset).

### Form `onSubmit`

Calls [`save`](ValidatedForm.html#save) if the form is valid(using [`validate`](ValidatedForm.html#validate)) or if the [`validateSave`](tutorial-2-configuring.html#global-validate-save) configuration is set to `false`.

### Form `preventReset`

Disabling the call to `event.preventDefault()` during an [`onReset`](#form-onreset) event will cause the default HTML behaviour to conflict with the bound behaviour(e.g. one tries to set a value but the other tries to clear it).

```js
// Disable globally
ValidatedForm.preventReset = false

// Disable per form `instance`
F.instance.preventReset = false

// Disable using the `configure()` method
F.configure({ preventReset: false })
```

Disable using JSX:

```tsx
<F.Form preventReset={false} />
```

### Form `preventSubmit`

Disabling the call to `event.preventDefault()` during an [`onSubmit`](#form-onsubmit) event will cause the form's default behaviour to redirect user's browser. Unless specified via the form's `action` and `method` properties, forms will make a GET request to the current URL.

```js
// Disable globally
ValidatedForm.preventSubmit = false

// Disable per form `instance`
F.instance.preventSubmit = false

// Disable using the `configure()` method
F.configure({ preventSubmit: false })
```

```tsx
<F.Form preventSubmit={false} />
```

## Buttons

The button labels can be specified via the `label` property or as a child contents.

```tsx
<F.Button label='Checkout' />
```

```tsx
<F.Button>Checkout</F.Button>
```

Note that the [`Submit`](ValidatedForm.html#Submit) and [`Reset`](ValidatedForm.html#Reset) button functionality is bound to the form's [`onSubmit`](#form-onsubmit) and [`onReset`](#form-onreset) events respectively.

## Labels

Displays the auto-generated username label:

```tsx
<F.Label name='username' />
```

```html
<!-- Generated DOM -->
<label for="f-00000-username">Username</label>
```

Specifying a custom label:

```tsx
<F.Label name='username' label='Username or Email' />
```

```tsx
<F.Label name='username'>Username or Email</F.Label>
```

```html
<!-- Generated DOM -->
<label for="f-00000-username">Username or Email</label>
```

## Errors

Displays the username error message:

```tsx
<F.Error name='username' />
```

```html
<!-- Generated DOM when input is invalid -->
<span class="error">Username is required</span>
```

Specifying a custom error label:

```tsx
<F.Error name='username' label='Username or Email' />
```

```tsx
<F.Error name='username'>Username or Email</F.Error>
```

```html
<!-- Generated DOM when input is invalid -->
<span class="error">Username or Email is required</span>
```

## Groups

The [`Group`](ValidatedForm.html#Group) component can be bound to an input name to make its class property reactive to the input state. The configuration classes used are [`dirtyCssClass`](tutorial-2-configuring.html#global-dirty-class), [`groupCssClass`](tutorial-2-configuring.html#global-group-class), and [`invalidCssClass`](tutorial-2-configuring.html#global-invalid-class). These classes can be set [globally](tutorial-2-configuring.html#global-configuration) or per [instance](tutorial-2-configuring.html#instance-configuration).

### Default Group Behaviour

The default [`Group`](ValidatedForm.html#Group) behaviour is to wrap its contents with the above mentioned classes only if the [`groupCssClass`](tutorial-2-configuring.html#global-group-class) is not set to `false` when the `name` property is a string.

```js
const F = ValidatedForm({
  username (value) {
    if (value === '') return 'is required'
  }
}).configure({
  dirtyCssClass: 'dirty',
  groupCssClass: 'group',
  invalidCssClass: 'invalid'
})
```

Example of reactive grouping:

```tsx
<F.Group name='username'>
  <p>Reactively wrapped content!</p>
</F.Group>
```

```html
<!-- Generated DOM when input is not dirty -->
<div class="group">
  <p>Reactively wrapped content!</p>
</div>

<!-- Generated DOM when input is dirty but valid -->
<div class="group dirty">
  <p>Reactively wrapped content!</p>
</div>

<!-- Generated DOM when input is invalid -->
<div class="group dirty invalid">
  <p>Reactively wrapped content!</p>
</div>
```

### Extended Group Behaviour

The [`Group`](ValidatedForm.html#Group) component can be bound to multiple inputs by specifying an array of names as the `name` property. The content is wrapped regardless of the [`groupCssClass`](tutorial-2-configuring.html#global-group-class) value.

```js
const F = ValidatedForm({
  username (value) {
    if (value === '') return 'is required'
  }
}).configure({
  dirtyCssClass: 'dirty',
  invalidCssClass: 'invalid'
})
```

Example of reactivity to multiple inputs:

```tsx
<F.Group name={['username', 'nickname']}>
  <p>Reactively wrapped content!</p>
</F.Group>
```

```html
<!-- Generated DOM when username and nickname are valid -->
<div>
  <p>Reactively wrapped content!</p>
</div>

<!-- Generated DOM when username or nickname is dirty but neither are invalid -->
<div class="dirty">
  <p>Reactively wrapped content!</p>
</div>

<!-- Generated DOM when username or nickname is dirty and invalid -->
<div class="dirty invalid">
  <p>Reactively wrapped content!</p>
</div>
```
