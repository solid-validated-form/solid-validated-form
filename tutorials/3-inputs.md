- [CSS Class Inheritance](#css-class-inheritance)
- [Input Types](#input-types)
- [Input Properties](#input-properties)
  - [`class`](#input-class)
  - [`classes`](#input-classes)
  - [`classList`](#input-classlist)
  - [`error`](#input-error)
  - [`group`](#input-group)
  - [`id`](#input-id)
  - [`label`](#input-label)
  - [`name`](#input-name)
  - [`options`](#input-options)
  - [`value`](#input-value)
- [Input Events](#input-events)
  - [`onBlur`](#input-onblur)
  - [`onChange`](#input-onchange)
  - [`onKeyUp`](#input-onkeyup)
- [Input Lists](#input-lists)
  - [`class`](#list-class)
  - [`classes`](#list-classes)
  - [`error`](#list-error)
  - [`group`](#list-group)
  - [`id`](#list-id)
  - [`label`](#list-label)
  - [`options`](#list-options)

## CSS Class Inheritance

*(<ins>Since v1.4.0</ins>)*  
CSS classes for group `<div>`, `<label>`, `<input>`, and error `<span>` elements can be specified in multiple ways. The `class`, `classes`, and `group` properties allows each component to override the global or instance configured classes.

This is a snippet of how classes are inherited in input components:

```tsx
<Group class={props.classes.group ?? props.group ?? instance.groupCssClass ?? globals.groupCssClass}>
  <Label class={props.classes.label ?? instance.labelCssClass ?? globals.labelCssClass} />
  <Input class={props.classes.input ?? props.class ?? instance.inputCssClass ?? globals.inputCssClass} />
  <Error class={props.classes.error ?? instance.errorCssClass ?? globals.errorCssClass} />
</Group>
```

## Input Types

All of the HTML input types have their own component. For example, a `number` input can be created using the [`Number`](ValidatedForm.html#Number) component.

```tsx
<F.Number name='numeric' />
```

```html
<!-- Generated DOM -->
<label for="f-00000-numeric">Numeric</label>
<input id="f-00000-numeric" type="number" name="numeric" />
```

Note that the [`Reset`](ValidatedForm.html#Reset) and [`Submit`](ValidatedForm.html#Submit) components are actually `<button>` tags.

```tsx
<F.Submit />
```

```html
<!-- Generated DOM -->
<button type="submit">Submit</button>
```

Here is the list of all the available `<input>` types:

| Input Type       | JSX Component                                       |
| ---------------- | --------------------------------------------------- |
| `checkbox`       | [`Checkbox`](ValidatedForm.html#Checkbox)           |
| `color`          | [`Color`](ValidatedForm.html#Color)                 |
| `date`           | [`Date`](ValidatedForm.html#Date)                   |
| `datetime-local` | [`DatetimeLocal`](ValidatedForm.html#DatetimeLocal) |
| `email`          | [`Email`](ValidatedForm.html#Email)                 |
| `file`           | [`File`](ValidatedForm.html#File)                   |
| `hidden`         | [`Hidden`](ValidatedForm.html#Hidden)               |
| `image`          | [`Image`](ValidatedForm.html#Image)                 |
| `month`          | [`Month`](ValidatedForm.html#Month)                 |
| `number`         | [`Number`](ValidatedForm.html#Number)               |
| `password`       | [`Password`](ValidatedForm.html#Password)           |
| `radio`          | [`Radio`](ValidatedForm.html#Radio)                 |
| `range`          | [`Range`](ValidatedForm.html#Range)                 |
| `reset`          | [`Reset`](ValidatedForm.html#Reset)                 |
| `search`         | [`Search`](ValidatedForm.html#Search)               |
| `submit`         | [`Submit`](ValidatedForm.html#Submit)               |
| `tel`            | [`Tel`](ValidatedForm.html#Tel)                     |
| `text`           | [`Text`](ValidatedForm.html#Text)                   |
| `time`           | [`Time`](ValidatedForm.html#Time)                   |
| `url`            | [`Url`](ValidatedForm.html#Url)                     |
| `week`           | [`Week`](ValidatedForm.html#Week)                   |

Additional form inputs:

| DOM Element |               JSX Component               |
| ----------- | ----------------------------------------- |
| `textarea`  | [`Textarea`](ValidatedForm.html#Textarea) |
| `select`    | [`Select`](ValidatedForm.html#Select)     |

*(<ins>Since v1.4.0</ins>)*  
The `checkbox` and `radio` input types have a helper component that simplifies creating [input lists](#input-lists):

| Input Type | JSX Component                                     |
| ---------- | ------------------------------------------------- |
| `checkbox` | [`CheckboxList`](ValidatedForm.html#CheckboxList) |
| `radio`    | [`RadioList`](ValidatedForm.html#RadioList)       |

## Input Properties

The various input components all have the same configuration properties except for [`Select`](ValidatedForm.html#Select) which accept a custom [`options`](#input-options) property.

Here is the list of available input properties:

| Property                        | Type           |
| ------------------------------- | -------------- |
| [`class`](#input-class)         | `string`       |
| [`classes`](#input-classes)     | `object`       |
| [`classList`](#input-classlist) | `object`       |
| [`error`](#input-error)         | `false|string` |
| [`group`](#input-group)         | `false|string` |
| [`id`](#input-id)               | `string`       |
| [`label`](#input-label)         | `false|string` |
| [`name`](#input-name)           | `string`       |
| [`options`](#input-options)     | `object`       |
| [`value`](#input-value)         | `string`       |

The following events are also handled by inputs:

- [`onBlur`](#input-onblur)
- [`onChange`](#input-onchange)
- [`onKeyUp`](#input-onkeyup)

### Input `class`

**Type:** `string`

Overrides the global or instance configured CSS class for the `<input>` element.

```tsx
<F.Text name='username' class='form-control' />
```

```html
<!-- Generated DOM -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" type="text" name="username" class="form-control">
```

### Input `classes`

**Type:** `object`

*(<ins>Since v1.4.0</ins>)*  
Overrides the global or instance configured CSS classes for group `<div>`, `<label>`, `<input>`, and/or error `<span>` elements.

```tsx
<F.Text name='username' classes={{
  group: 'mb-3',
  label: 'form-label',
  input: 'form-control',
  error: 'invalid-feedback'
}} />
```

```html
<!-- Generated DOM -->
<div class="mb-3">
  <label for="f-00000-username" class="form-label">Username</label>
  <input id="f-00000-username" type="text" name="username" class="form-control">
</div>
```

```html
<!-- Generated DOM when validation fails -->
<div class="mb-3 invalid">
  <label for="f-00000-username" class="form-label">Username</label>
  <input id="f-00000-username" type="text" name="username" class="form-control invalid">
  <span class="invalid-feedback">Username is required</span>
</div>
```

### Input `classList`

**Type:** `object`

Be mindful of the global and instance configured CSS classes when using the `classList` property. The [`dirtyCssClass`](tutorial-2-configuring.html#global-dirty-class) and the [`invalidCssClass`](tutorial-2-configuring.html#global-invalid-class) classes can be overridden which may lead to unexpected behaviour.

### Input `error`

**Type:** `false|string`

***@todo:*** *Allow dynamic components to be passed as errors*

Overrides the auto-generated label for the error `<span>` which uses a capitalization function that splits *camelCase*, *kebab-case*, and *snake_case* words accordingly.

```tsx
<F.Text name='username' error='Email' />
```

```html
<!-- Generated DOM when validation fails -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" type="text" name="username" class="invalid" />
<span class="error">Email is required</span>
```

The error `<span>` can be disabled by passing `false` as the `error` value.

```tsx
<F.Text name='username' error={false} />
```

```html
<!-- Generated DOM when validation fails -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" type="text" name="username" class="invalid" />
```

### Input `group`

**Type:** `false|string`

Overrides the global or instance configured CSS class for the group `<div>` element.

```tsx
<F.Text name='username' group='mb-3' />
```

```html
<!-- Generated DOM -->
<div class="mb-3">
  <label for="f-00000-username">Username</label>
  <input id="f-00000-username" type="text" name="username" />
</div>
```

The group `<div>` can be disabled by passing `false` as the `group` value.

```tsx
<F.Text name='username' group={false} />
```

```html
<!-- Generated DOM -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" type="text" name="username" />
```

### Input `id`

**Type:** `string`

Overrides the auto-generated ID which is a kebab-cased concatenation of the form's [`id`](tutorial-4-components.html#form-id) and the input's [`name`](#input-name) property.

```tsx
<F.Text id='user' name='username' />
```

```html
<!-- Generated DOM -->
<label for="user">Username</label>
<input id="user" type="text" name="username" />
```

The [`Checkbox`](ValidatedForm.html#Checkbox) and [`Radio`](ValidatedForm.html#Radio) components are also suffixed with an incrementing number per input.

```tsx
<>
  <F.Checkbox name='language' value='en' label='English' />
  <F.Checkbox name='language' value='fr' label='francais' />
</>
```

```html
<!-- Generated DOM -->
<label for="f-00000-language-1">English</label>
<input id="f-00000-language-1" type="checkbox" name="language" value="en" />

<label for="f-00000-language-2">francais</label>
<input id="f-00000-language-2" type="checkbox" name="language" value="fr" />
```

### Input `label`

**Type:** `string|false`

***@todo:*** *Allow dynamic components to be passed as labels*

Overrides the auto-generated attribute label which uses a capitalization function that splits *camelCase*, *kebab-case*, and *snake_case* words accordingly.

```tsx
<>
  <F.Text name='first' label='First Name' />
  <F.Text name='firstName' />
  <F.Text name='first_name' />
  <F.Text name='first-name' />
</>
```

```html
<!-- Generated DOM -->
<label for="f-00000-first">First Name</label>
<input id="f-00000-first" type="text" name="first" />

<label for="f-00000-firstName">First Name</label>
<input id="f-00000-firstName" type="text" name="firstName" />

<label for="f-00000-first_name">First Name</label>
<input id="f-00000-first_name" type="text" name="first_name" />

<label for="f-00000-first-name">First Name</label>
<input id="f-00000-first-name" type="text" name="first-name" />
```

Further demonstration of case processing:

```tsx
<F.Text name='aStrange_attribute-name' />
```

```html
<!-- Generated DOM -->
<label for="f-00000-aStrange_attribute-name">A Strange Attribute Name</label>
<input id="f-00000-aStrange_attribute-name" type="text" name="aStrange_attribute-name" />
```

The `<label>` can be disabled by passing `false` as the `label` value:

```tsx
<F.Text name='username' label={false} />
```

```html
<!-- Generated DOM -->
<input id="f-00000-username" type="text" name="username" />
```

### Input `name`

**Type:** `string`

The `name` property is what makes inputs reactive; it binds the input to a validator defined when creating the [`ValidatedForm`](ValidatedForm.html) instance. Attempting to use a `name` with no validator will throw *TypeError: getValidator(...) is not a function* when validation runs on the input.

```js
// Creating a "username" input
const F = ValidatedForm({
  username () {}
})
```

```tsx
<F.Text name='username' />
```

### Input `options`

**Type:** `object`

Used for [`Select`](ValidatedForm.html#Select) components to create a list of `<option>` elements: object keys become option values; and object values become option labels.

```tsx
<F.Select name='language' options={{
  en: 'English',
  fr: 'francais'
}} />
```

```html
<!-- Generated DOM -->
<label for="f-00000-language">Language</label>
<select id="f-00000-language">
  <option value="en">English</option>
  <option value="fr">francais</option>
</select>
```

To create an `<optgroup>` element: object keys become optgroup labels; and object values are objects of option labels and values.

```tsx
<F.Select name='language' options={{
  English: { 'en-ca': 'Canada', 'en-us': 'United States of America' },
  francais: { 'fr-ca': 'canada', 'fr-fr': 'france' }
}} />
```

```html
<!-- Generated DOM -->
<label for="f-00000-language">Language</label>
<select id="f-00000-language">
  <optgroup label="English">
    <option value="en-ca">Canada</option>
    <option value="en-us">United States of America</option>
  </optgroup>
  <optgroup label="francais">
    <option value="fr-ca">canada</option>
    <option value="fr-fr">france</option>
  </optgroup>
</select>
```

Using a mix of both options and optgroups is valid.

```tsx
<F.Select options={{
  '': 'Please select an option',
  English: { 'en-ca': 'Canada', 'en-us': 'United States of America' },
  francais: { 'fr-ca': 'canada', 'fr-fr': 'france' },
  'c++': 'c++'
}} />
```

```html
<!-- Generated DOM -->
<label for="f-00000-language">Language</label>
<select id="f-00000-language">
  <option value="">Please select an option</option>
  <optgroup label="English">
    <option value="en-ca">Canada</option>
    <option value="en-us">United States of America</option>
  </optgroup>
  <optgroup label="francais">
    <option value="fr-ca">canada</option>
    <option value="fr-fr">france</option>
  </optgroup>
  <option value="c++">c++</option>
</select>
```

### Input `value`

**Type:** `string`

Sets the static value for [`Checkbox`](ValidatedForm.html#Checkbox) and [`Radio`](ValidatedForm.html#Radio) components. All other input types have their `value` property overridden to be made reactive to the input state which can be updated outside of a [`onKeyUp`](#input-onkeyup) event(e.g. using [`set`](ValidatedForm.html#set)).

## Input Events

The following events are bound to inputs to make them reactive though they are still available to use for custom logic:

- [`onBlur`](#input-onblur)
- [`onChange`](#input-onchange)
- [`onKeyUp`](#input-onkeyup)

All of the input events are forwarded **after** they change the state.

### Input `onBlur`

**Type:** `Function`

The `onBlur` event triggers transformation and validation of the related input except when the initial value is blank and the input is also left blank. This prevents "is required" errors from shifting the form around as users click or tab through it.

### Input `onChange`

**Type:** `Function`

The `onChange` event handles two primary tasks: setting the state for checkbox, radio, and select inputs; and triggering transformation and validation for the related input.

### Input `onKeyUp`

**Type:** `Function`

The `onKeyUp` event handles setting the state for all inputs except a `<select multiple>`.

## Input Lists

*(<ins>Since v1.4.0</ins>)*  
Helper components that simplify creating lists of `checkbox` or `radio` inputs:

| Input Type | JSX Component                                     |
| ---------- | ------------------------------------------------- |
| `checkbox` | [`CheckboxList`](ValidatedForm.html#CheckboxList) |
| `radio`    | [`RadioList`](ValidatedForm.html#RadioList)       |

The list components have mostly the same properties as single inputs:

| Property                   | Type           |
| -------------------------- | -------------- |
| [`class`](#list-class)     | `string`       |
| [`classes`](#list-classes) | `object`       |
| [`error`](#list-error)     | `false|string` |
| [`group`](#list-group)     | `false|string` |
| [`id`](#list-id)           | `string`       |
| [`label`](#list-label)     | `false|string` |
| [`options`](#list-options) | `object`       |

### List `class`

**Type:** `string`

Overrides the CSS class for each `<input>` element. Only affects the `<input>` elements.

### List `classes`

**Type:** `object`

The `classes` property for list components differs a bit from the normal inputs. Instead of an `input` CSS class, it has a `component` object to specify classes for each input.

```tsx
<F.CheckboxList name='accept' options={...} classes={{
  group: 'mb-3',
  label: 'form-label',
  component: {
    group: 'form-check',
    label: 'form-check-label',
    input: 'form-check-input'
  },
  error: 'invalid-feedback'
}} />
```

```html
<!-- Generated DOM when validation fails -->
<div class="mb-3 invalid">
  <label for="f-00000-accept" class="form-label">Accept</label>

  <div class="form-check">
    <label for="f-00000-accept-1" class="form-check-label">I accept the Terms of Use</label>
    <input id="f-00000-accept-1" type="checkbox" name="accept" value="tos" class="form-check-input invalid" />
  </div>

  <div class="form-check">
    <label for="f-00000-accept-2" class="form-check-label">I accept cookies for site performance</label>
    <input id="f-00000-accept-2" type="checkbox" name="accept" value="metrics" class="form-check-input invalid" />
  </div>

  <div class="form-check">
    <label for="f-00000-accept-3" class="form-check-label">I accept cookies for 3rd party tracking</label>
    <input id="f-00000-accept-3" type="checkbox" name="accept" value="third" class="form-check-input invalid" />
  </div>

  <span class="invalid-feedback">Accept must include Terms of Use</span>
</div>
```

### List `error`

**Type:** `false|string`

Overrides the auto-generated name label for the error `<span>` element or disables. Only affects the outer most error.

### List `group`

**Type:** `false|string`

Overrides the CSS class for the group `<div>` element or disables it. Only affects the outer most group.

### List `id`

Each input has a numbered suffix even when specifying a custom ID.

```tsx
<F.CheckboxList id='check' name='accept' options={...} />
```

```html
<!-- Generated DOM -->
<label for="check">Accept</label>

<label for="check-1">I accept the Terms of Use</label>
<input id="check-1" type="checkbox" name="accept" value="tos" />

<label for="check-2">I accept cookies for site performance</label>
<input id="check-2" type="checkbox" name="accept" value="metrics" />

<label for="check-3">I accept cookies for 3rd party tracking</label>
<input id="check-3" type="checkbox" name="accept" value="third" />
```

### List `label`

**Type:** `false|string`

Overrides the auto-generated name label for `<label>` elements or disables it. Only affects the outer most label.

### List `options`

Like the [`Select`](ValidatedForm.html#Select) component, inputs are created using an [`options`](#list-options) property.

```tsx
<F.CheckboxList name='accept' options={{
  tos: 'I accept the Terms of Use',
  metrics: 'I accept cookies for site performance',
  third: 'I accept cookies for 3rd party tracking'
}} />
```

```html
<!-- Generated DOM -->
<label for="f-00000-accept">Accept</label>

<label for="f-00000-accept-1">I accept the Terms of Use</label>
<input id="f-00000-accept-1" type="checkbox" name="accept" value="tos" />

<label for="f-00000-accept-2">I accept cookies for site performance</label>
<input id="f-00000-accept-2" type="checkbox" name="accept" value="metrics" />

<label for="f-00000-accept-3">I accept cookies for 3rd party tracking</label>
<input id="f-00000-accept-3" type="checkbox" name="accept" value="third" />
```
