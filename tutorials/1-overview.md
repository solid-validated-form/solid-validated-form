 - [Form Inputs](#form-inputs)
   - [Validation](#input-validation)
   - [Transformation](#input-transformation)
   - [Defaults](#input-defaults)

## Form Inputs

The ValidatedForm function takes a set of validators, transformers, and initial values then returns reactive form components. Its <abbr title="TypeScript signature">signature</abbr> is as follows:

```ts
ValidatedForm(validators: object, transformers?: object, initialValues?: object): object
```

Form input attributes are specified as validator keys. Attributes are case sensitive and must share the same name in each object. Names are normalized and capitalized when converted to labels(see [Input `label`](tutorial-3-inputs.html#input-label)).

```js
const F = ValidatedForm({
  username (value) {
    if (value === '') return 'is required'
  }
})
```

All forms have a randomly generated, 5 character, hexadecimal based ID prefix with `f-`. This unique ID is used to link labels to inputs since labels don't wrap their associated inputs. The ID can be changed for the entire form using [`Form`](ValidatedForm.html#Form) [`id`](tutorial-4-components.html#form-id) or per component using [`Field`](ValidatedForm.html#Field) [`id`](tutorial-3-inputs.html#input-id).

```tsx
<F.Text name='username' />
```

```html
<!-- Generated DOM -->
<label for="f-00000-username">Username</label>
<input id="f-00000-username" type="text" name="username" />
```

### Input Validation

Validators have the following <abbr title="TypeScript signature">signature</abbr>:

```ts
const validators: object = {
  [name: string]: (value: string) => string | void
}
```

Validators return an error message when their input is invalid and don't return anything otherwise. Validation happens on the [`onBlur`](tutorial-3-inputs.html#input-onblur) and [`onChange`](tutorial-3-inputs.html#input-onchange) events. Error messages are prefixed with their input label when displayed.

### Input Transformation

Transformers have a similar <abbr title="TypeScript signature">signature</abbr> to validators but always return a string:

```ts
const transformers: object = {
  [name: string]: (value: string) => string
}
```

Transformers take the input values and modifies them before running validation. This is particularly useful for enforcing certain formatting(e.g. *h0h0h0* => *H0H 0H0*).

```js
const F = ValidatedForm({
  username () {}
}, {
  username (value) {
    // Input will always be converted to lower case and trimmed when validation runs
    return value.toLowerCase().trim()
  }
})
```

### Input Defaults

Initial values have the following <abbr title="TypeScript signature">signature</abbr>:

```ts
const initialValues: object = {
  [name: string]: string
}
```

To populate the form inputs with default values, simply pass an object of attribute names and values.

```js
const F = ValidatedForm({
  username () {}
}, {
  username: 'user@domain.tld'
})
```

The `transformers` argument can be omitted so long as none of the values in the `initialValues` object are functions.
