# solid-validated-form

Easily create forms with client side validation for [Solid](https://www.solidjs.com/). Inspired by [Ember Validated Form](https://github.com/adfinis-sygroup/ember-validated-form).

[![NPM Version](https://badge.fury.io/js/solid-validated-form.svg)](https://badge.fury.io/js/solid-validated-form)
[![Pipeline Status](https://gitlab.com/solid-validated-form/solid-validated-form/badges/master/pipeline.svg)](https://gitlab.com/solid-validated-form/solid-validated-form/-/commits/master)
[![Coverage Status](https://coveralls.io/repos/gitlab/solid-validated-form/solid-validated-form/badge.svg?branch=master)](https://coveralls.io/gitlab/solid-validated-form/solid-validated-form?branch=master)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

- [Getting Started](#getting-started)
	- [Example](#example)
- [Documentation](#documentation)
	- [Tutorials](#tutorials)

![Solid Validated Form Demo GIF](https://gitlab.com/solid-validated-form/solid-validated-form/-/raw/master/tutorials/solid-validated-form-demo.gif)

## Getting Started

Add `solid-validated-form` to your `package.json`.

```sh
npm install --save solid-js solid-validated-form
```

This package has full TypeScript support.

### Example

See [CodeSandbox demo](https://codesandbox.io/s/solid-validated-form-demo-j8bfm?file=/src/main.tsx).

```tsx
import { render } from 'solid-js/web'
import ValidatedForm from 'solid-validated-form'

function App() {
  const F = ValidatedForm({
    firstName (value) {
      if (value.length < 3 || value.length > 40) return 'must be between 3 and 40 characters'
    },
    lastName (value) {
      if (value.length < 3 || value.length > 40) return 'must be between 3 and 40 characters'
    },
    aboutMe (value) {
      if (value.length > 200) return 'is too long (maximum is 200 characters)'
    },
    country (value) {
      if (value === '') return 'can\'t be blank'
    },
    gender (value) {
      if (value === '') return 'can\'t be blank'
    }
  })

  return (
    <F.Form>
      <F.Text name='firstName' />

      <F.Text name='lastName' />

      <F.Textarea name='aboutMe' placeholder='Optional...' />

      <F.Select name='country' options={{
        '': 'Please choose...',
        ca: 'Canada',
        us: 'United State of America',
        zz: 'Other'
      }} />

      <F.RadioList name='gender' options={{
        m: 'Male',
        f: 'Female'
      }} />

      <F.Submit label='Save' />
      <F.Reset />
    </F.Form>
  )
}

render(() => <App />, document.getElementById('app'))
```

## Documentation

Extensive work has been done to [document](https://solid-validated-form.gitlab.io/solid-validated-form/ValidatedForm.html) and [test](https://gitlab.com/solid-validated-form/solid-validated-form/-/tree/master/src/__tests__) this library.

### Tutorials

- [Overview](https://solid-validated-form.gitlab.io/solid-validated-form/tutorial-1-overview.html)
- [Configuring Validated Forms](https://solid-validated-form.gitlab.io/solid-validated-form/tutorial-2-configuring.html)
- [Input Components](https://solid-validated-form.gitlab.io/solid-validated-form/tutorial-3-inputs.html)
- [Non-Input Components](https://solid-validated-form.gitlab.io/solid-validated-form/tutorial-4-components.html)
- [Form State](https://solid-validated-form.gitlab.io/solid-validated-form/tutorial-5-state.html)
- [Form Methods](https://solid-validated-form.gitlab.io/solid-validated-form/tutorial-6-methods.html)
