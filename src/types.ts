import type { JSX, ParentProps } from 'solid-js'
import type { StoreSetter } from 'solid-js/store'

type AttributeProps = JSX.InputHTMLAttributes<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>

export interface ButtonProps extends ParentProps {
  label?: string
  type?: ButtonType
}

export type ButtonType =
  | 'button'
  | 'reset'
  | 'submit'

export interface ClassList {
  [className: string]: boolean
}

export interface CommitStore {
  [name: FormInput]: FormValue
}

export interface ContentsProps extends ParentProps {
  label?: false | string
  name?: string
}

export interface CssClasses {
  component?: string | CssClasses
  error?: string
  group?: string
  input?: string
  label?: string
}

export interface CssClassProps {
  dirtyCssClass: false | string
  errorCssClass: false | string
  groupCssClass: false | string
  inputCssClass: false | string
  invalidCssClass: false | string
  labelCssClass: false | string
}

export type CssClassType =
  | 'dirty'
  | 'error'
  | 'group'
  | 'input'
  | 'invalid'
  | 'label'

export interface CustomFieldProps {
  classes?: CssClasses
  error?: false | string
  group?: false | string
  label?: false | string
}

export interface DirtyStore {
  [name: FormInput]: DirtyValue | undefined
}

export interface DirtyValue {
  new: FormValue
  old: FormValue
}

export interface ElementProps extends Omit<FieldProps, 'type'> {}

export interface ErrorProps {
  name?: string
  class?: string
  label?: false | string
  visible?: boolean
}

export interface ErrorStore {
  [name: FormInput]: string | undefined
}

export interface FieldProps extends CustomFieldProps, InputProps {}

export type FieldType =
  | InputType
  | 'select'
  | 'textarea'

export interface FormGlobals extends GlobalsConfiguration {
  configure: (settings: Partial<GlobalsConfiguration>) => void
}

export type FormInput = keyof Validators & string

export type FormInputs = FormInput[]

export interface FormInstance {
  instance: InstanceConfiguration
  readonly dirty: DirtyStore
  readonly errors: ErrorStore
  readonly values: InputStore
  readonly isDirty: boolean
  readonly isValid: boolean
  clear: (() => void) & ((...inputs: FormInputs | [FormInputs]) => void)
  configure: (settings: Partial<InstanceConfiguration>) => FormInstance
  fresh: (() => void) & ((values: FormValues) => void)
  get: (() => FormValues) & ((...inputs: FormInputs | [FormInputs]) => FormValues)
  id: (() => string) & ((name: string) => string)
  reset: (() => void) & ((...inputs: FormInputs | [FormInputs]) => void)
  save: (() => void) & ((...inputs: FormInputs | [FormInputs]) => void)
  set: ((input: FormInput, value: FormValue | InputSetter) => void) & ((input: FormValues) => void)
  validate: (() => boolean) & ((...inputs: FormInputs | [FormInputs]) => boolean)
  Button: (props: ButtonProps) => JSX.Element
  Checkbox: (props: ElementProps) => JSX.Element
  CheckboxList: (props: ListElementProps) => JSX.Element
  Color: (props: ElementProps) => JSX.Element
  Date: (props: ElementProps) => JSX.Element
  DatetimeLocal: (props: ElementProps) => JSX.Element
  Email: (props: ElementProps) => JSX.Element
  Error: (props: ErrorProps) => JSX.Element
  Field: (props: FieldProps) => JSX.Element
  File: (props: ElementProps) => JSX.Element
  Form: (props: FormProps) => JSX.Element
  Group: (props: GroupProps) => JSX.Element
  Hidden: (props: ElementProps) => JSX.Element
  Image: (props: ElementProps) => JSX.Element
  Input: (props: InputProps) => JSX.Element
  Label: (props: LabelProps) => JSX.Element
  List: (props: ListProps) => JSX.Element
  Month: (props: ElementProps) => JSX.Element
  Number: (props: ElementProps) => JSX.Element
  Password: (props: ElementProps) => JSX.Element
  Radio: (props: ElementProps) => JSX.Element
  RadioList: (props: ListElementProps) => JSX.Element
  Range: (props: ElementProps) => JSX.Element
  Reset: (props: ButtonProps) => JSX.Element
  Search: (props: ElementProps) => JSX.Element
  Select: (props: SelectProps) => JSX.Element
  Submit: (props: ButtonProps) => JSX.Element
  Tel: (props: ElementProps) => JSX.Element
  Text: (props: ElementProps) => JSX.Element
  Textarea: (props: ElementProps) => JSX.Element
  Time: (props: ElementProps) => JSX.Element
  Url: (props: ElementProps) => JSX.Element
  Week: (props: ElementProps) => JSX.Element
}

export interface FormProps extends PreventDefaultProps, ParentProps {
  id?: string
  values?: FormValues
  onReset?: false | Function | [Function, unknown?]
  onSubmit?: false | Function | [Function, unknown?]
}

export interface FormStore {
  commits: CommitStore
  dirty: DirtyStore
  errors: ErrorStore
  inputs: InputStore
}

export type FormValue = string | string[]

export interface FormValues {
  [name: FormInput]: FormValue
}

export interface GlobalsConfiguration extends CssClassProps, PreventDefaultProps, VisibleProps {
  validateSave: boolean
}

export interface GroupProps extends ParentProps {
  class?: false | string
  classList?: ClassList
  name?: FormInput | FormInput[]
}

export interface InitialValues {
  [name: FormInput]: FormValue
}

export interface InputProps extends InputSpread {
  options?: SelectOptions
  type?: FieldType
}

export type InputSetter = StoreSetter<FormValue, [string, 'inputs']>

export interface InputSpread extends AttributeProps, ParentProps {
  name: FormInput
  value?: FormValue
}

export interface InputStore {
  [name: FormInput]: FormValue
}

export type InputType =
  | 'button'
  | 'checkbox'
  | 'color'
  | 'date'
  | 'datetime-local'
  | 'email'
  | 'file'
  | 'hidden'
  | 'image'
  | 'month'
  | 'number'
  | 'password'
  | 'radio'
  | 'range'
  | 'reset'
  | 'search'
  | 'submit'
  | 'tel'
  | 'text'
  | 'time'
  | 'url'
  | 'week'

export interface InstanceConfiguration extends Partial<GlobalsConfiguration> {
  id: string
}

export interface LabelProps extends ParentProps {
  class?: string
  for?: string
  label?: false | string
  name?: string
  visible?: boolean
}

export interface ListElementProps extends ElementProps {
  options: ListOptions
}

export interface ListOptions {
  [value: string]: string
}

export interface ListProps extends CustomFieldProps, Omit<InputSpread, 'type' | 'value'> {
  component: (props: FieldProps) => JSX.Element
  options: ListOptions
}

export interface MergeClassListProps {
  name?: string
  classList?: object
}

export interface OptgroupProps {
  [value: string]: string
}

export interface OptionProps {
  name: FormInput
  value: string
  children: string | Element | OptgroupProps
}

export interface PreventDefaultProps {
  preventReset?: boolean
  preventSubmit?: boolean
}

export type PreventDefaultType =
  | 'reset'
  | 'submit'

export interface SelectOptions {
  [value: string]: string | { [value: string]: string }
}

export interface SelectProps extends FieldProps {
  options?: SelectOptions
}

export type Transform<Input extends FormValue, Output extends Input> = (input: Input) => Output

export interface Transformer extends Transform<FormValue, FormValue> {}

export interface Transformers {
  [name: FormInput]: Transformer
}

// no-invalid-void-type
export type Validator = (value: string | string[]) => string | void // eslint-disable-line

export interface Validators {
  [name: string]: Validator
}

export interface ValueProps {
  value?: FormValue
  name: FormInput
}

export interface VisibleProps {
  visibleErrors: boolean
  visibleLabels: boolean
}

export type VisibleType =
  | 'errors'
  | 'labels'
