/**
 * Namespaced functions checking values for certain criterias.
 */
export const is = {
  /**
   * Checks if a value is an array.
   */
  arr (value: unknown): value is unknown[] {
    return Array.isArray(value)
  },
  /**
   * Checks if a value can be called as a function using our `call` helper.
   */
  call (value: unknown): value is Function | [Function, unknown?] {
    return typeof value === 'function' || (Array.isArray(value) && typeof value[0] === 'function')
  },
  /**
   * Checks if a value is a function.
   */
  fn (value: unknown): value is Function {
    return typeof value === 'function'
  },
  /**
   * Checks if an object has a certain key.
   */
  key (obj: object, key: string | number): boolean {
    if (typeof obj !== 'object') throw new TypeError('is.key requires `obj` to be an object')
    return Object.prototype.hasOwnProperty.call(obj, key)
  },
  /**
   * Checks if a value is a number and ensures the value isn't NaN.
   */
  num (value: unknown): value is number {
    return typeof value === 'number' && !Number.isNaN(value)
  },
  /**
   * Checks if a value is an object.
   */
  obj (value: unknown): value is object {
    if (value === null || typeof value !== 'object') return false
    return !Array.isArray(value)
  },
  /**
   * Checks if a value is a string with optional length.
   */
  str (value: unknown, requireLength: boolean = false): value is string {
    return typeof value === 'string' && (!requireLength || value.length !== 0)
  }
}

/**
 * Attempts to call a function based on Solid's JSX paradigm.
 *
 * @function call
 */
export function call (fn: Function | [Function, unknown?], event: Event): unknown {
  if (typeof fn === 'function') return fn(event)
  if (Array.isArray(fn)) {
    if (typeof fn[0] === 'function') {
      return fn.length > 1 ? fn[0](fn[1], event) : fn[0](event)
    }
  }
}

/**
 * Capitalize each word of a string.
 *
 * <p>This function also handles various casing:</p>
 *
 * <ul>
 *  <li><code>kebab-cased-string</code> becomes <code>Kebab Cased String</code></li>
 *  <li><code>snake_cased_string</code> becomes <code>Snake Cased String</code></li>
 *  <li><code>camelCasedString</code> becomes <code>Camel Cased String</code></li>
 * </ul>
 *
 * @function capitalize
 */
export function capitalize (str: string): string {
  // Ensuring we have a string to work with
  let out = String(str)

  // Ensuring it's not all upper case
  if (out.toUpperCase() === out) out = out.toLowerCase()

  // Doing the actual capitalization
  return out
    .replace(/([A-Z])/g, ' $1')
    .replace(/[-_]/g, ' ')
    .replace(/\s+/g, ' ')
    .trim()
    .split(' ')
    .map((word) => word[0].toUpperCase() + word.slice(1))
    .join(' ')
}
