//
// Package imports
//

import {
  batch,
  createComputed,
  createRenderEffect,
  createSignal,
  For,
  Match,
  Show,
  splitProps,
  Switch
} from 'solid-js'

import {
  createStore
} from 'solid-js/store'

import {
  Dynamic
} from 'solid-js/web'

import type {
  JSX
} from 'solid-js'

//
// Project imports
//

import {
  call,
  capitalize,
  is
} from './utils'

import type {
  ButtonProps,
  ClassList,
  ContentsProps,
  CssClasses,
  CssClassProps,
  CssClassType,
  DirtyValue,
  ElementProps,
  ErrorProps,
  FieldProps,
  FormGlobals,
  FormInput,
  FormInputs,
  FormInstance,
  FormProps,
  FormStore,
  FormValue,
  FormValues,
  GlobalsConfiguration,
  GroupProps,
  InitialValues,
  InputProps,
  InputSetter,
  InputSpread,
  InstanceConfiguration,
  LabelProps,
  ListElementProps,
  ListProps,
  MergeClassListProps,
  OptgroupProps,
  OptionProps,
  PreventDefaultProps,
  PreventDefaultType,
  SelectOptions,
  SelectProps,
  Transformer,
  Transformers,
  Validator,
  Validators,
  ValueProps,
  VisibleType,
  VisibleProps
} from './types'

export type {
  ButtonProps,
  ButtonType,
  ClassList,
  CommitStore,
  ContentsProps,
  CssClasses,
  CssClassProps,
  CssClassType,
  CustomFieldProps,
  DirtyStore,
  DirtyValue,
  ElementProps,
  ErrorProps,
  ErrorStore,
  FieldProps,
  FieldType,
  FormGlobals,
  FormInput,
  FormInputs,
  FormInstance,
  FormProps,
  FormStore,
  FormValue,
  FormValues,
  GlobalsConfiguration,
  GroupProps,
  InitialValues,
  InputProps,
  InputSetter,
  InputSpread,
  InputStore,
  InputType,
  InstanceConfiguration,
  LabelProps,
  ListElementProps,
  ListOptions,
  ListProps,
  MergeClassListProps,
  OptgroupProps,
  OptionProps,
  PreventDefaultProps,
  PreventDefaultType,
  SelectOptions,
  SelectProps,
  Transform,
  Transformer,
  Transformers,
  Validator,
  Validators,
  ValueProps,
  VisibleProps,
  VisibleType
} from './types'

//
// Definitions
//

// DRYing up the same lengthy errors messages
const ERROR_TRANSFORMERS = 'ValidatedForm requires `transformers` to be an object of transformer functions'

// Error messages are used for validating contructor params
const ERROR_VALIDATORS = 'ValidatedForm requires `validators` to be an object of validation functions'

// Validating properties in the constructor params uses the same messages
const ERROR_VALUES = 'ValidatedForm requires `initialValues` to be an object of strings and/or arrays of strings'

// Need to rename error constructor since we redeclare it as a JSX component
const GenericError = Error

/**
 * Form configuration for all ValidatedForm instances.
 *
 * These are not meant to be reactive attributes.
 *
 * Overriding this property will change the CSS classes for ***all*** validated forms
 * and their inputs. Useful for incorperating CSS libraries and paradigms.
 *
 * @see ValidatedForm~GlobalsConfiguration
 * @see ValidatedForm#configure
 */
const globals: FormGlobals = {
  dirtyCssClass: false,
  errorCssClass: 'error',
  groupCssClass: false,
  inputCssClass: false,
  invalidCssClass: 'invalid',
  labelCssClass: false,
  preventReset: true,
  preventSubmit: true,
  validateSave: true,
  visibleErrors: true,
  visibleLabels: true,
  /**
   * Configures the global settings for all forms and inputs.
   *
   * These are not meant to be reactive attributes.
   */
  configure (settings: Partial<GlobalsConfiguration>): void {
    // Don't need to do anything fancy to copy values into our configuration object
    Object.assign(globals, settings)
  }
}

// Setting the globals instance on our form export
ValidatedForm.globals = globals

/**
 * Easily create forms with client side validation.
 *
 * Form attributes are specified as validator keys. Attributes are case sensitive
 * and must share the same name in each object.
 *
 * The `initialValues` can be specified as the `transformers` argument.
 *
 * @namespace ValidatedForm
 * @see ValidatedForm#Field
 * @example
 * //
 * // Creating basic inputs
 * //
 * import ValidatedForm from 'solid-validated-form'
 *
 * const F = ValidatedForm({
 *   username (value) {
 *     if (value === '') return 'is required'
 *   }
 * })
 *
 * return (
 *   <F.Text name='username' />
 * )
 * @example
 * //
 * // Transforming a value before validation
 * //
 * const F = ValidatedForm({
 *   username () {}
 * }, {
 *   username (value) {
 *     return value.toLowerCase().trim()
 *   }
 * })
 * @example
 * //
 * // Specifying a default value for an input
 * //
 * const F = ValidatedForm({
 *   username () {}
 * }, {
 *   username: 'user@domain.tld'
 * })
 * @example
 * //
 * // Specifying a transformer and a default value for an input
 * //
 * const F = ValidatedForm({
 *   username () {}
 * }, {
 *   username (value) {
 *     return value.toLowerCase().trim()
 * }, {
 *   username: 'user@domain.tld'
 * })
 */
export default function ValidatedForm (
  validators: Validators,
  transformers: Transformers | InitialValues = {},
  initialValues: InitialValues | Transformers = {}
): FormInstance {
  // Ensure validators is an object
  if (!is.obj(validators)) throw new TypeError(ERROR_VALIDATORS)

  // Ensure transformers is also an object
  if (!is.obj(transformers)) throw new TypeError(ERROR_TRANSFORMERS)

  // Same for initial values
  if (!is.obj(initialValues)) throw new TypeError(ERROR_VALUES)

  // Check if we need to potentially flip initial values and transformers -
  // - only flip if all values of the transformers object are not functions
  const flip: boolean = Object.values(transformers).every((value: unknown): boolean => !is.fn(value))

  // Flip transformers and initial values
  if (flip) [transformers, initialValues] = [initialValues as Transformers, transformers as InitialValues]

  // List of attributes in our form
  const attributes: FormInputs = Object.keys(validators)

  // Iterating over each attribute to validate the construction params
  attributes.forEach((name: FormInput): void => {
    // Ensure validators are functions
    if (!is.fn(validators[name])) throw new TypeError(ERROR_VALIDATORS)

    // Ensure transformers are functions
    if (is.key(transformers, name) && !is.fn(transformers[name])) throw new TypeError(ERROR_TRANSFORMERS)
  })

  // Creating the instance configuration object
  const instance: InstanceConfiguration = {
    // Generate unique ID per form
    id: 'f-' + Math.random().toString(16).slice(2, 7)
  }

  // Create our form state
  const [state, setState] = createStore<FormStore>({ commits: {}, dirty: {}, errors: {}, inputs: {} })

  // Create a signal for checking if all inputs are valid
  const [getValid, setValid] = createSignal<boolean>(true)

  // Create a signal for checking if any input is dirty
  const [getDirty, setDirty] = createSignal<boolean>(false)

  // Additional suffix to distinguish checkbox and radio IDs
  let iid: number = 1

  // Create dirty state computation
  createComputed((): void => {
    // Batching updates to dirty state
    batch(() => {
      // Check every attribute whenever a computation is triggered
      attributes.forEach((name: FormInput): void => {
        // Shortcut to the current input values
        const input: FormValue | undefined = state.inputs[name] ?? ''

        // Shortcut to the committed input values
        const commit: FormValue | undefined = state.commits[name] ?? ''

        // Checking the input type since select multiple uses an array of string values
        if (is.arr(input)) {
          // Sorting the current selected options
          input.sort()

          // Ensuring the old values is an array
          const old: string[] = is.arr(commit)
            ? commit.sort()
            : commit === '' ? [] : [commit]

          // Converting arrays to strings to simplify comparison since values are all strings
          if (input.toString() !== old.toString()) {
            // Values don't match, set dirty state
            setState('dirty', name, { old, new: input })
          } else {
            // Input array matches committed array so clear any dirty state data
            setState('dirty', name, undefined)
          }
        } else if (input !== commit) {
          // Input differs from committed so set the dirty state of the input name
          setState('dirty', name, { new: input, old: commit })
        } else {
          // Input matches committed so clear any dirty state data
          setState('dirty', name, undefined)
        }
      })
    })
  })

  // Create dirty signal computation
  createComputed((): void => {
    // Ensure none of the attributes have dirty state or mark the form as dirty
    setDirty(attributes.some((name: FormInput): boolean => state.dirty[name] !== undefined))
  })

  // Create valid signal computation
  createComputed((): void => {
    // Ensure none of the attributes have an error or mark the form as invalid
    setValid(attributes.every((name: FormInput): boolean => state.errors[name] === undefined))
  })

  // Populates the state objects with the attribute names and initial values
  fresh(initialValues as InitialValues)

  // ---------------------------------------------------------------------------
  // Helper functions

  /**
   * Transformer that simply forwards the initial value.
   *
   * @private
   */
  function carry (value: FormValue): FormValue {
    return value
  }

  /**
   * Gets a reactive class list configuration based on a given input name.
   *
   * @private
   */
  function getClassList (name: FormInput): ClassList {
    // Defining an empty class list and conditionally adding keys
    const list: ClassList = {}

    // Get the dirty CSS class to use if any
    const dirty: false | string = getCssClass('dirty')

    // Same goes for the invalid class
    const invalid: false | string = getCssClass('invalid')

    // Add dirty class if set, reacting to the attribute's dirty state
    if (is.str(dirty, true)) list[dirty] = state.dirty[name] !== undefined

    // Add invalid class if set, reacting to the attribute's error state
    if (is.str(invalid, true)) list[invalid] = state.errors[name] !== undefined

    // Return the class list
    return list
  }

  /**
   * Gets a form configuration, returning the instance value and falling back to the
   * global one.
   *
   * @private
   */
  function getConfigured<Type> (key: keyof GlobalsConfiguration): Type {
    // Using is.key instead of is.set so null values can disable features
    return (is.key(instance, key) ? instance[key] : ValidatedForm.globals[key]) as Type
  }

  /**
   * Gets the label for a given component.
   *
   * @private
   */
  function getContents (props: ContentsProps): string | JSX.Element {
    // Always prefer child DOM as contents
    if (props.children !== undefined) return props.children

    // Allow passing a label for basic text contents
    if (is.str(props.label)) return props.label

    // Allow passing a name related to our inputs which gets capitalized
    return is.str(props.name) ? capitalize(props.name) : ''
  }

  /**
   * Gets a CSS class configuration based on a given type(e.g. 'error').
   *
   * @private
   */
  function getCssClass (type: CssClassType): false | string {
    // Generating class key(e.g. "inputCssClass")
    return getConfigured<false | string>(`${type}CssClass` as keyof CssClassProps)
  }

  /**
   * Gets a list of inputs from an array of strings.
   *
   * @private
   */
  function getInputs (inputs: FormInputs | [FormInputs]): FormInputs {
    if (inputs.length !== 0) {
      return is.arr(inputs[0]) ? inputs[0] : inputs as FormInputs
    } else {
      return attributes
    }
  }

  /**
   * Gets the event.preventDefault configuration for a given type.
   *
   * @private
   */
  function getPreventDefault (type: PreventDefaultType, props: FormProps): boolean {
    // Converting type to configuration key(e.g. reset => preventReset)
    const prevents = `prevent${type[0].toUpperCase()}${type.slice(1)}` as keyof PreventDefaultProps

    // Prefer form props and fallback to instance/global settings
    return props[prevents] ?? getConfigured<boolean>(prevents)
  }

  /**
   * Gets the transformer for a given input attribute if any.
   *
   * @private
   */
  function getTransformer (name: keyof Transformers): Transformer {
    return transformers[name] as Transformer ?? carry
  }

  /**
   * Gets the validator for a given input attribute.
   *
   * @private
   */
  function getValidator (name: FormInput): Validator {
    if (validators[name] === undefined) throw new GenericError(`Validator "${name}" does not exist`)
    return validators[name]
  }

  /**
   * Gets the value for a given set of input properties.
   *
   * @private
   */
  function getValue (props: ValueProps): FormValue {
    // Always prefer direct values then fall back to state input values
    return props.value ?? state.inputs[props.name]
  }

  /**
   * Gets the visibility configuration for a given element.
   *
   * @private
   */
  function getVisible (type: VisibleType): boolean {
    // Convert the text (e.g.) 'errors' to 'visibleErrors' configuration key
    return getConfigured<boolean>(`visible${type[0].toUpperCase()}${type.slice(1)}` as keyof VisibleProps)
  }

  /**
   * Merges reactive classes with a component's `classList` property.
   *
   * @private
   */
  function mergeClassList (props: MergeClassListProps): ClassList {
    return {
      // Get reactive classes if a name is provided
      ...(props.name !== undefined ? getClassList(props.name) : {}),
      // Always allow component class list to override ours
      ...(props.classList !== undefined ? props.classList : {})
    }
  }

  /**
   * Transforms, updates, and validates a given input attribute.
   *
   * @private
   */
  function updateState (name: FormInput): void {
    // Always transform input before validation
    const value: FormValue = getTransformer(name)(state.inputs[name] ?? '')

    // Update the state if the transformed value isn't the same
    if (value !== (state.inputs[name] ?? '')) setState('inputs', name, value)

    // Undefined inputs get validated as empty strings
    setState('errors', name, getValidator(name)(value) as string | undefined)
  }

  // ---------------------------------------------------------------------------
  // Form components

  /**
   * Creates a button tag.
   *
   * The button label can be specified as a component property or as child contents.
   *
   * @function ValidatedForm#Button
   * @example
   * <F.Button label='Save' />
   * @example
   * <F.Button>Save</F.Button>
   * @example
   * <!-- Generated button -->
   * <button type="button">Save</button>
   */
  function Button (props: ButtonProps): JSX.Element {
    // Removing invalid button properties from spread
    const [, spread] = splitProps(props, ['label'])

    // Returns a configurable button
    return (
      <button type='button' {...spread}>
        {getContents(props)}
      </button>
    )
  }

  /**
   * Creates a checkbox input.
   *
   * @function ValidatedForm#Checkbox
   * @see ValidatedForm#Field
   * @example
   * <F.Checkbox name='accept' />
   * @example
   * <!-- Generated input -->
   * <input type="checkbox" name="accept" />
   */
  function Checkbox (props: ElementProps): JSX.Element {
    // Alias checkbox input
    return (
      <Field type='checkbox' {...props} />
    )
  }

  /**
   * Creates a list of checkbox inputs.
   *
   * @function ValidatedForm#CheckboxList
   * @see ValidatedForm#List
   * @example
   * <F.CheckboxList name='accept' options={{
   *   tos: 'I accept the Terms of Use',
   *   metrics: 'I accept cookies for site performance',
   *   third: 'I accept cookies for 3rd party tracking'
   * }} />
   * @example
   * <!-- Generated inputs -->
   * <label>I accept the Terms of Use</label>
   * <input type="checkbox" name="accept" value="tos" />
   *
   * <label>I accept cookies for site performance</label>
   * <input type="checkbox" name="accept" value="metrics" />
   *
   * <label>I accept cookies for 3rd party tracking</label>
   * <input type="checkbox" name="accept" value="third" />
   */
  function CheckboxList (props: ListElementProps): JSX.Element {
    // Require props.options object even if it's empty
    if (!is.obj(props.options)) throw new TypeError('CheckboxList "options" must be an object')

    // Alias listing checkbox inputs
    return (
      <List component={Checkbox} {...props} />
    )
  }

  /**
   * Creates a color input.
   *
   * @function ValidatedForm#Color
   * @see ValidatedForm#Field
   * @example
   * <F.Color name='colour' />
   * @example
   * <!-- Generated input -->
   * <input type="color" name="colour" />
   */
  function Color (props: ElementProps): JSX.Element {
    // Alias color input
    return (
      <Field type='color' {...props} />
    )
  }

  /**
   * Creates a date input.
   *
   * @function ValidatedForm#Date
   * @see ValidatedForm#Field
   * @example
   * <F.Date name='birthday' />
   * @example
   * <!-- Generated input -->
   * <input type="date" name="birthday" />
   */
  function Date (props: ElementProps): JSX.Element {
    // Alias date input
    return (
      <Field type='date' {...props} />
    )
  }

  /**
   * Creates a datetime-local input.
   *
   * @function ValidatedForm#DatetimeLocal
   * @see ValidatedForm#Field
   * @example
   * <F.DatetimeLocal name='timestamp' />
   * @example
   * <!-- Generated input -->
   * <input type="datetime-local" name="timestamp" />
   */
  function DatetimeLocal (props: ElementProps): JSX.Element {
    // Alias datetime-local input
    return (
      <Field type='datetime-local' {...props} />
    )
  }

  /**
   * Creates an email input.
   *
   * @function ValidatedForm#Email
   * @see ValidatedForm#Field
   * @example
   * <F.Email name='contact' />
   * @example
   * <!-- Generated input -->
   * <input type="email" name="contact" />
   */
  function Email (props: ElementProps): JSX.Element {
    // Alias email input
    return (
      <Field type='email' {...props} />
    )
  }

  /**
   * Form input error component.
   *
   * This component will render an error label preceeding the given input's error
   * message. If there is no error then the component is not shown.
   *
   * @function ValidatedForm#Error
   * @todo Allow placement of the error message in the label text(e.g. "Username {error}!")
   * @example
   * <F.Error name='username' label='The username(your email address)' />
   * @example
   * <F.Error name='username'>The username(your email address)</F.Error>
   * @example
   * <!-- Generated DOM when the input is invalid -->
   * <span class="error">The username(your email address) is required</span>
   */
  function Error (props: ErrorProps): JSX.Element {
    // Removing invalid properties and properties we define from error span spread
    const [, spread] = splitProps(props, ['class', 'label', 'name', 'visible'])

    // Reactive toggle for showing or hididng the error span
    const show: () => boolean = () => props.visible !== false &&
      getVisible('errors') &&
      props.name !== undefined &&
      state.errors[props.name] !== undefined

    // Getting the CSS class for the error span
    const css: false | string = getCssClass('error')

    // Returns optional error allowing arbitrary placing of error messages
    return (
      <Show when={show()}>
        <span class={props.class ?? (css === false ? undefined : css)} {...spread}>
          {getContents(props)}
          {' '}
          {state.errors[props.name as string] as string}
        </span>
      </Show>
    )
  }

  /**
   * Form input field component.
   *
   * This component handles changes to inputs before forwarding `onBlur`,
   * `onChange`, and `onKeyUp` events.
   *
   * Any `props.value` is dropped as the value property is bound to
   * our state. Use the `initialValues` parameter when calling ValidatedForm
   * to set default values for attributes.
   *
   * @function ValidatedForm#Field
   * @see ValidatedForm#Error
   * @see ValidatedForm#Input
   * @see ValidatedForm#Label
   * @see ValidatedForm#Group
   * @todo Allow custom components for labels and errors
   * @example
   * <!--
   *   - Creating a basic input
   *   -->
   * <F.Text name='username' />
   *
   * <!-- Generated DOM -->
   * <label for="f-00000-username">Username</label>
   * <input id="f-00000-username" name="username" type="text" />
   *
   * <!-- Generated DOM when input is invalid -->
   * <label for="f-00000-username">Username</label>
   * <input id="f-00000-username" name="username" type="text" class="invalid" />
   * <span class="error">Username is required</span>
   * @example
   * <!--
   *   - Groupping labels, inputs, and errors
   *   -->
   * <F.Text name='username' group='custom-group-class' />
   *
   * <!-- Generated DOM -->
   * <div class="custom-group-class">
   *   <label for="f-00000-username">Username</label>
   *   <input id="f-00000-username" name="username" type="text" />
   * </div>
   *
   * <!-- Generated DOM when input is invalid -->
   * <div class="custom-group-class invalid">
   *   <label for="f-00000-username">Username</label>
   *   <input id="f-00000-username" name="username" type="text" class="invalid" />
   *   <span class="error">Username is required</span>
   * </div>
   * @example
   * <!--
   *   - Specifying a custom label
   *   -->
   * <F.Text name='username' label='Email' />
   *
   * <!-- Generated DOM -->
   * <label for="f-00000-username">Email</label>
   * <input id="f-00000-username" name="username" type="text" />
   *
   * <!-- Generated DOM when input is invalid -->
   * <label for="f-00000-username">Email</label>
   * <input id="f-00000-username" name="username" type="text" class="invalid" />
   * <span class="error">Username is required</span>
   * @example
   * <!--
   *   - Disabling labels for inputs
   *   -->
   * <F.Text name='username' label={false} />
   *
   * <!-- Generated DOM -->
   * <input id="f-00000-username" name="username" type="text" />
   *
   * <!-- Generated DOM when input is invalid -->
   * <input id="f-00000-username" name="username" type="text" class="invalid" />
   * <span class="error">Username is required</span>
   * @example
   * <!--
   *   - Specifying a custom error label
   *   -->
   * <F.Text name='username' error='Username or Email' />
   *
   * <!-- Generated DOM -->
   * <label for="f-00000-username">Username</label>
   * <input id="f-00000-username" name="username" type="text" />
   *
   * <!-- Generated DOM when input is invalid -->
   * <label for="f-00000-username">Username</label>
   * <input id="f-00000-username" name="username" type="text" class="invalid" />
   * <span class="error">Username or Email is required</span>
   * @example
   * <!--
   *   - Disabling errors for inputs
   *   -->
   * <F.Text name='username' error={false} />
   *
   * <!-- Generated DOM -->
   * <label for="f-00000-username">Username</label>
   * <input id="f-00000-username" name="username" type="text" />
   *
   * <!-- Generated DOM when input is invalid -->
   * <label for="f-00000-username">Username</label>
   * <input id="f-00000-username" name="username" type="text" class="invalid" />
   */
  function Field (props: FieldProps): JSX.Element {
    // Enforce classes being an object
    if (props.classes !== undefined && !is.obj(props.classes)) throw new TypeError('Field "classes" must be an object')

    // Removing invalid input properties from spread
    const [, spread] = splitProps(props, ['class', 'classes', 'error', 'group', 'id', 'label'])

    // Get the list of classes to apply to each component
    const classes: CssClasses = props.classes ?? {}

    // Getting a custom suffix for checkbox and radio inputs since they use the same name
    const suffix: false | number = props.type === 'checkbox' || props.type === 'radio' ? iid++ : false

    // Getting the passed ID or generating one for referencing labels to inputs
    function fid (): string {
      return props.id ?? `${id(props.name)}${is.num(suffix) ? `-${suffix}` : ''}`
    }

    // Returns a common form input layout
    return (
      <Group
        name={props.name}
        class={classes.group ?? props.group}
      >
        <Label
          for={fid()}
          name={props.name}
          label={props.label}
          class={classes.label}
          visible={props.label !== false}
        />
        <Input
          id={fid()}
          class={classes.input ?? props.class}
          {...spread}
        />
        <Error
          name={props.name}
          label={props.error}
          class={classes.error}
          visible={props.error !== false}
        />
      </Group>
    )
  }

  /**
   * Creates a file input.
   *
   * @function ValidatedForm#File
   * @see ValidatedForm#Field
   * @example
   * <F.File name='upload' />
   * @example
   * <!-- Generated input -->
   * <input type="file" name="upload" />
   */
  function File (props: ElementProps): JSX.Element {
    // Alias file input
    return (
      <Field type='file' {...props} />
    )
  }

  /**
   * Creates a form tag.
   *
   * The form tag automatically recieves a unique, five character, hexadecimal based
   * ID prefixed with `f-` unless one is provided.
   *
   * Passing an `initialValues` object will override the values specified when initially
   * constructing the ValidatedForm. Leveraging
   * [resources]{@link https://www.solidjs.com/docs/latest/api#createresource}
   * to populate this object will trigger the right async loading for use with
   * [<Suspense>]{@link https://www.solidjs.com/docs/latest/api#%3Csuspense%3E}.
   *
   * By default, the form's `onReset` and `onSubmit` events call `event.preventDefault()`.
   * This can be disabled globally using {@link ValidatedForm~GlobalsConfiguration}
   * or per form using {@link ValidatedForm~InstanceConfiguration}'s `preventReset`
   * and `preventSubmit` respectively.
   *
   * @function ValidatedForm#Form
   * @see ValidatedForm#fresh
   * @example
   * <F.Form>
   *   <F.Text name='nickname' />
   * </F.Form>
   * @example
   * <!-- Generated DOM -->
   * <form id="f-00000">
   *   <label for="f-00000-nickname">Nickname</label>
   *   <input id="f-00000-nickname" name="nickname" type="text" />
   * </form>
   * @example
   * <!-- Setting a custom ID -->
   * <F.Form id='custom-id'>
   *   <F.Text name='nickname' />
   * </F.Form>
   * @example
   * <!-- Generated DOM -->
   * <form id="custom-id">
   *   <label for="custom-id-nickname">Nickname</label>
   *   <input id="custom-id-nickname" name="nickname" type="text" />
   * </form>
   * @example
   * <!-- Setting initial values -->
   * <F.Form initialValues={{ nickname: 'root' }}>
   *   <F.Text name='nickname' />
   * </F.Form>
   * @example
   * <!-- Generated DOM -->
   * <form id="f-00000">
   *   <label for="f-00000-nickname">Nickname</label>
   *   <input id="f-00000-nickname" name="nickname" type="text" value="root" />
   * </form>
   */
  function Form (props: FormProps): JSX.Element {
    // Allow setting the form instance ID via component property
    if (is.str(props.id)) instance.id = props.id

    // Removing invalid form properties or properties we define from spread
    const [, spread] = splitProps(props, [
      // Removing properties we bind
      'id',
      'preventReset',
      'preventSubmit',
      'values',
      // Removing events we forward
      'onReset',
      'onSubmit'
    ])

    // Custom event handler for resetting form values
    function onReset (event: Event): unknown | undefined {
      // Don't clear form attributes by default but allow disabling of this behaviour
      if (getPreventDefault('reset', props)) event.preventDefault()

      // Forward the event to the original handler before resetting values
      const results: unknown = is.call(props.onReset) ? call(props.onReset, event) : undefined

      // Reset form values
      reset()

      // Return the original handlers return value
      return results
    }

    // Custom event handler for submitting forms
    function onSubmit (event: SubmitEvent): unknown | undefined {
      // Prevent forms from being submitted by default but allow disabling of this also
      if (getPreventDefault('submit', props)) event.preventDefault()

      // Forward the event to the original handler before saving values
      const results: unknown = is.call(props.onSubmit) ? call(props.onSubmit, event) : undefined

      // Commit form values
      if (!getConfigured<boolean>('validateSave') || validate()) save()

      // Return the original handlers return value
      return results
    }

    // Creating a render effect to trigger <Suspense>
    createRenderEffect(() => {
      // Update initial values if the form has a values property
      if (is.obj(props.values)) fresh(props.values)
    })

    // Returns the form with reactive class list
    return (
      <form
        id={instance.id}
        onReset={onReset}
        onSubmit={onSubmit}
        {...spread}
      >
        {props.children}
      </form>
    )
  }

  /**
   * Groups contents with reactive CSS classes.
   *
   * The Group component has two different behaviours based on the `props.name` value:
   *
   * 1. A string name will require the groupCssClass to be configured or the `props.class`
   *   property to be set and it will add the dirtyCssClass and invalidCssClass if
   *   configured as it reacts to the named input's state.
   *
   * 2. An array of one or multiple names will make the group react to those inputs
   *   states and wrap the contents with dirtyCssClass and invalidCssClass regardless
   *   of groupCssClass' value.
   *
   * @function ValidatedForm#Group
   * @example
   * <!-- Using the Field component's group property -->
   * <F.Text name='username' group='custom-group-class' />
   * @example
   * <!-- Using the Group component -->
   * <F.Group name='username' class='custom-group-class'>
   *   <F.Text name='username' />
   * </F.Group>
   * @example
   * <!-- Generated DOM -->
   * <div class="custom-group-class">
   *   <label for="f-00000-username">Username</label>
   *   <input id="f-00000-username" name="username" type="text" />
   * </div>
   * @example
   * <!-- Generated DOM when input is invalid -->
   * <div class="custom-group-class invalid">
   *   <label for="f-00000-username">Username</label>
   *   <input id="f-00000-username" name="username" type="text" class="invalid" />
   *   <span class="error">Username is required</span>
   * </div>
   * @example
   * <!-- Reacting to multiple inputs -->
   * <F.Group name={['username', 'nickname']}>...</F.Group>
   * @example
   * <!-- Generated DOM when any of the inputs are invalid -->
   * <div class="invalid">...</div>
   */
  function Group (props: GroupProps): JSX.Element {
    // Group divs get the dirty class if both are properly configured
    const dirty: false | string = getCssClass('dirty')

    // Get the configured default group class
    const group: false | string = getCssClass('group')

    // Group divs also get the invalid class if both are properly configured
    const invalid: false | string = getCssClass('invalid')

    // Configuring group via properties
    const hasProps: boolean = Boolean(props.class) || is.arr(props.name)

    // Configuring group via instance/global properties
    const hasGroup: boolean = group !== false && props.class !== false

    // Removing invalid div properties and properties we define from spread
    const [, spread] = splitProps(props, ['class', 'classList', 'name'])

    // Grouping can be disabled by passing false
    if (hasProps || hasGroup) {
      // Reactive class list
      function list (): ClassList {
        // Not specifying name as an array only make it reactive if groupCssClass is configured
        if (!is.arr(props.name)) return mergeClassList(props as & { name: string })

        // Conditionally assinging class list
        const assign: ClassList = {}

        // Using array functions to make group reactive to multiple inputs
        const names: FormInputs = props.name

        // Add dirty class if set, reacting to multiple attributes' dirty state
        if (is.str(dirty, true)) assign[dirty] = names.some((name: FormInput): DirtyValue | undefined => state.dirty[name])

        // Add invalid class if set, reacting to multiple attributes' error state
        if (is.str(invalid, true)) assign[invalid] = names.some((name: FormInput): string | undefined => state.errors[name])

        // Return the class list
        return assign
      }

      // Wraps JSX with a reactive class list
      return (
        <div
          class={(is.str(props.class) ? props.class : (group !== false ? group : undefined))}
          classList={list()}
          {...spread}
        >
          {props.children}
        </div>
      )
    } else {
      // No group no div
      return (
        <>{props.children}</>
      )
    }
  }

  /**
   * Creates a hidden input.
   *
   * @function ValidatedForm#Hidden
   * @see ValidatedForm#Field
   * @example
   * <F.Hidden name='csrf' />
   * @example
   * <!-- Generated input -->
   * <input type='hidden' name='csrf' />
   */
  function Hidden (props: ElementProps): JSX.Element {
    // Alias hidden input
    return (
      <Field type='hidden' label={false} error={false} {...props} />
    )
  }

  /**
   * Creates an image input.
   *
   * @function ValidatedForm#Image
   * @see ValidatedForm#Field
   * @example
   * <F.Image name='avatar' />
   * @example
   * <!-- Generated input -->
   * <input type='image' name='avatar' />
   */
  function Image (props: ElementProps): JSX.Element {
    // Alias image input
    return (
      <Field type='image' {...props} />
    )
  }

  /**
   * Returns an form input element(input, select, textarea).
   *
   * The following properties are bound by this component:
   *
   * - **class**: CSS class added to input on top of configured reactive classes
   * - **classList**: Reactive CSS classes added to input which can override state
   *   classes(dirty, invalid, etc.)
   * - **id**: Optional; defaults to the input name prefixed with the current form ID
   * - **type**: "select" and "textarea" generate their respective DOM, otherwise
   *   it's an input with the given type
   * - **value**: Optional; requires the "name" property to be reactive
   *
   * The following events are handled by this component if the "name" property is
   * set and then forwards the events to the user defined handlers:
   *
   * - **onBlur**: Runs validation except on empty inputs
   * - **onChange**: Runs validation
   * - **onKeyUp**: Updates the state
   *
   * All other properties are passed to the resulting HTML tag as a spread.
   *
   * @function ValidatedForm#Input
   */
  function Input (props: InputProps): JSX.Element {
    // Ensure the name property exists
    if (!is.str(props.name, true)) throw new TypeError('Input `name` property must be a string with length')

    // Ensure the options property on Select components is an object
    if (props.type === 'select' && props.options !== undefined) {
      // Throw TypeError if options isn't an object
      if (!is.obj(props.options)) throw new TypeError('Input `options` property must be an object for type `select`')
    }

    // Ensure the value property on Checkbox and Radio components are strings
    if (props.type === 'checkbox' || props.type === 'radio') {
      // Throw TypeError if value isn't a string
      if (!is.str(props.value)) throw new TypeError('Input `value` property must be a string for types `checkbox` and `radio`')
    }

    // Helper function to populate the state with input values
    function updateInput (event: Event): void {
      // DRYing up declaring event target types
      const target = event.target as HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement

      // DRYing up referencing the input name
      const name: FormInput = props.name

      // DRYing up referencing value
      const value: FormValue = target.value

      // DRYing up accessing the current input value
      let current: FormValue = state.inputs[name]

      // Need to handle different input types to properly set state
      switch (props.type) {
        case 'checkbox':
          // Ensure we're working with an array for checkboxes
          if (!is.arr(current)) current = current === '' ? [] : [current]

          // Add or remove value from state based on the checked attribute
          if ((target as HTMLInputElement).checked) {
            // Don't push the same value twice
            if (!current.includes(value)) {
              // Adding the checked value to our existing array then sorting it
              setState('inputs', name, [value, ...current].sort())
            }
          } else {
            // Getting the index of the value we just unchecked
            const index: number = current.findIndex((str) => str === value)

            // Can't modify an array that doesn't contain the checked value
            if (index !== -1) {
              // Creating a new sorted array by slicing out the old value
              setState('inputs', name, [...current.slice(0, index), ...current.slice(index + 1)].sort())
            }
          }

          break
        case 'radio':
          // Radio inputs only allow one value
          if ((target as HTMLInputElement).checked) setState('inputs', name, value)

          break
        case 'select':
          // Need to handle select multiple tags differently
          if (props.multiple ?? false) {
            // Multiple select elements return an array of selected values
            const values: FormValue = []

            // Iterate through each option available to the element
            for (const option of (target as HTMLSelectElement).options) {
              // Add the option as a value if it's selected
              if (option.selected && option.value !== '') values.push(option.value)
            }

            // Setting the option values and sorting them by default
            setState('inputs', name, values.sort())
          } else {
            // Select values are set using onChange so this kind of does nothing
            setState('inputs', name, value)
          }

          break
        default:
          // Handle updates to inputs using (e.g.) spinners
          setState('inputs', name, value)
      }
    }

    // Handler that validates input when they are blurred
    function onBlur (event: FocusEvent): unknown {
      // Don't trigger validation when blurring empty inputs
      if (
        props.type !== 'checkbox' &&
        props.type !== 'radio' &&
        (event.target as HTMLInputElement).value !== ''
      ) {
        // Transform values and run validation
        batch(() => updateState(props.name))
      }

      // Forward onBlur event
      if (is.call(props.onBlur)) return call(props.onBlur, event)
    }

    // Handler that validates inputs when they are changed
    function onChange (event: Event): unknown {
      // Batching changes to state
      batch(() => {
        // Update the inputs state
        updateInput(event)

        // Trigger validation on change when a name is present
        updateState(props.name)
      })

      // Forward onChange event
      if (is.call(props.onChange)) return call(props.onChange, event)
    }

    // Handler that updates our state when inputs receive a key up event.
    function onKeyUp (event: KeyboardEvent): unknown {
      // Don't update state when tabbing to or from other inputs
      if (event.key !== 'Tab') updateInput(event)

      // Forward onKeyUp event
      if (is.call(props.onKeyUp)) return call(props.onKeyUp, event)
    }

    // Reactive tag properties
    function spread (): InputSpread {
      // Getting the CSS class for the input
      const css: false | string = getCssClass('input')

      // JSX spread attributes that shouldn't be assigned
      const [, inherit] = splitProps(props, ['options', 'type', 'value'])

      // Extracting all props which makes this spread reactive to all its changes
      const assign: InputProps = {
        ...inherit,
        class: props.class ?? (css === false ? undefined : css),
        classList: mergeClassList(props),
        onBlur,
        onChange,
        onKeyUp
      }

      // Select tags don't accept a type or a value property
      if (props.type === 'select') return assign

      // Setting the required type attribute on input tags
      if (props.type !== 'textarea') assign.type = props.type

      // Handling checking a checkbox input since the state value is an array
      if (props.type === 'checkbox' || props.type === 'radio') {
        // Getting our current state value
        const value = state.inputs[props.name]

        // Making the checked attribute reactive to the input state instead of the value
        assign.checked = is.arr(value) ? value.includes(props.value as string) : value === props.value

        // Assigning the static value to the input
        assign.value = props.value
      } else {
        // Setting the reactive value attribute (note textarea initial value doesn't use this)
        assign.value = getValue(props) as string
      }

      // Returning our spread function without invalid element properties
      return assign
    }

    // Using <Switch> to generate different DOM elements based on input type
    return (
      <Switch fallback={(<input {...spread() as JSX.InputHTMLAttributes<HTMLInputElement>} />)}>
        <Match when={props.type === 'textarea'}>
          <textarea {...spread() as JSX.InputHTMLAttributes<HTMLTextAreaElement>}>
            {getValue(props)}
          </textarea>
        </Match>

        <Match when={props.type === 'select'}>
          <select {...spread() as JSX.InputHTMLAttributes<HTMLSelectElement>}>
            <Show when={props.options !== undefined} fallback={props.children}>
              <For each={Object.keys(props.options as SelectOptions)}>
                {(value: keyof SelectOptions & string) => (
                  <Option value={value} name={props.name}>{(props.options as SelectOptions)[value]}</Option>
                )}
              </For>
            </Show>
          </select>
        </Match>
      </Switch>
    )
  }

  /**
   * Form field label component.
   *
   * @function ValidatedForm#Label
   * @param {object} props Label properties
   * @param {string} props.name Input name
   * @param {string} [props.label] Custom label instead of capitalized attribute
   * @param {boolean} [props.visible=true] Toggle label DOM
   * @returns {JSX} Label component
   * @example
   * <F.Label name='username' label='Username or Email' />
   * @example
   * <F.Label name='username'>Username or Email</F.Label>
   * @example
   * <!-- Generated DOM -->
   * <label for="f-00000-username">Username or Email</label>
   */
  function Label (props: LabelProps): JSX.Element {
    // Removing label properties we handle
    const [, spread] = splitProps(props, [
      'class',
      'for',
      'label',
      'name',
      'visible'
    ])

    // Getting the CSS class for the label
    const css: string | false = getCssClass('label')

    // Returns an optional label related to an input
    return (
      <Show when={props.visible !== false && getVisible('labels')}>
        <label
          class={props.class ?? (css === false ? undefined : css)}
          for={props.for ?? id(props.name)}
          {...spread}
        >
          {getContents(props)}
        </label>
      </Show>
    )
  }

  /**
   * Creates a list of inputs.
   *
   * @function ValidatedForm#List
   */
  function List (props: ListProps): JSX.Element {
    // Ensure we have options to iterate
    if (!is.obj(props.options)) throw new TypeError('List "options" must be an object')

    // Ensure a component is hopefully renderable
    if (!is.fn(props.component)) throw new TypeError('List "component" must be a function')

    // Get the list of classes to apply to each component
    const classes: CssClasses = props.classes ?? {}

    // Making the checked attribute reative to the input state
    function checked (value: string): boolean {
      return is.arr(state.inputs[props.name])
        ? state.inputs[props.name].includes(value)
        : state.inputs[props.name] === value
    }

    // Using the iterated input index for generating unique IDs
    function fid (i: number): string {
      return `${props.id ?? id(props.name)}-${i}`
    }

    // Lists have their own wrapping group and associated label
    return (
      <Group
        name={props.name}
        class={classes.group ?? props.group}
      >
        <Label
          for={props.id}
          name={props.name}
          label={props.label}
          class={classes.label}
          visible={props.label !== false}
        />
        <For each={Object.keys(props.options)}>
          {(key, i) => (
            <Dynamic
              component={props.component}
              id={fid(i() + 1)}
              name={props.name}
              value={key}
              checked={checked(key)}
              label={props.options[key]}
              error={false}
              class={props.class}
              classes={classes.component as CssClasses ?? {}}
            />
          )}
        </For>
        <Error
          name={props.name}
          label={props.error}
          class={classes.error}
          visible={props.error !== false}
        />
      </Group>
    )
  }

  /**
   * Creates a month input.
   *
   * @function ValidatedForm#Month
   * @param {object} props Input properties
   * @returns {JSX} Input type month component
   * @see ValidatedForm#Field
   * @example
   * <F.Month name='favourite' />
   * @example
   * <!-- Generated input -->
   * <input type='month' name='favourite' />
   */
  function Month (props: ElementProps): JSX.Element {
    // Alias month input
    return (
      <Field type='month' {...props} />
    )
  }

  /**
   * Creates a number input.
   *
   * @function ValidatedForm#Number
   * @param {object} props Input properties
   * @returns {JSX} Input type number component
   * @see ValidatedForm#Field
   * @example
   * <F.Number name='age' />
   * @example
   * <!-- Generated input -->
   * <input type='number' name='age' />
   */
  function Number (props: ElementProps): JSX.Element {
    // Alias number input
    return (
      <Field type='number' {...props} />
    )
  }

  /**
   * Creates an option or optgroup.
   *
   * @function ValidatedForm~Option
   */
  function Option (props: OptionProps): JSX.Element {
    // Helper function to check if a value is the current selected one
    function selected (value: string): boolean | undefined {
      // DRYing up accessing the current input value
      const input = state.inputs[props.name]

      // Checking if the current value is equal to the input or is in the input array
      return is.arr(input) ? input.includes(value) : input === value
    }

    // Generates either an option or an optgroup with options
    return (
      <Show
        when={is.obj(props.children)} fallback={(
          <option selected={selected(props.value)} value={props.value}>{props.children as Element}</option>
      )}
      >
        <optgroup label={props.value}>
          <For each={Object.keys(props.children)}>
            {(value: string) => (
              <option selected={selected(value)} value={value}>{(props.children as OptgroupProps)[value]}</option>
            )}
          </For>
        </optgroup>
      </Show>
    )
  }

  /**
   * Creates a password input.
   *
   * @function ValidatedForm#Password
   * @see ValidatedForm#Field
   * @example
   * <F.Password name='secret' />
   * @example
   * <!-- Generated input -->
   * <input type='password' name='secret' />
   */
  function Password (props: ElementProps): JSX.Element {
    // Alias password input
    return (
      <Field type='password' {...props} />
    )
  }

  /**
   * Creates a radio input.
   *
   * @function ValidatedForm#Radio
   * @see ValidatedForm#Field
   * @example
   * <F.Radio name='choice' />
   * @example
   * <!-- Generated input -->
   * <input type='radio' name='choice' />
   */
  function Radio (props: ElementProps): JSX.Element {
    // Alias radio input
    return (
      <Field type='radio' {...props} />
    )
  }

  /**
   * Creates a list of radio inputs.
   *
   * @function ValidatedForm#RadioList
   * @see ValidatedForm#List
   * @example
   * <F.RadioList name='choice' options={{
   *   en: 'I speak English',
   *   fr: 'Je parle francais',
   *   both: 'I am bilingual / Je suis bilingue'
   * }} />
   * @example
   * <!-- Generated inputs -->
   * <label>I speak English</label>
   * <input type="radio" name="choice" value="en" />
   *
   * <label>Je parle francais</label>
   * <input type="radio" name="accept" value="fr" />
   *
   * <label>I am bilingual / Je suis bilingue</label>
   * <input type="radio" name="accept" value="both" />
   */
  function RadioList (props: ListElementProps): JSX.Element {
    // Require props.options object even if it's empty
    if (!is.obj(props.options)) throw new TypeError('RadioList "options" must be an object')

    // Alias listing radio inputs
    return (
      <List component={Radio} {...props} />
    )
  }

  /**
   * Creates a range input.
   *
   * @function ValidatedForm#Range
   * @see ValidatedForm#Field
   * @example
   * <F.Range name='through' />
   * @example
   * <!-- Generated input -->
   * <input type='range' name='through' />
   */
  function Range (props: ElementProps): JSX.Element {
    // Alias range input
    return (
      <Field type='range' {...props} />
    )
  }

  /**
   * Creates a reset button.
   *
   * @function ValidatedForm#Reset
   * @see ValidatedForm#Button
   * @example
   * <F.Reset />
   * @example
   * <!-- Generated button -->
   * <button type="reset">Reset</button>
   */
  function Reset (props: ButtonProps): JSX.Element {
    // Alias reset button
    return (
      <Button type='reset' label='Reset' {...props} />
    )
  }

  /**
   * Creates a search input.
   *
   * @function ValidatedForm#Search
   * @see ValidatedForm#Field
   * @example
   * <F.Search name='criteria' />
   * @example
   * <!-- Generated input -->
   * <input type='search' name='criteria' />
   */
  function Search (props: ElementProps): JSX.Element {
    // Alias search input
    return (
      <Field type='search' {...props} />
    )
  }

  /**
   * Creates a select input.
   *
   * @function ValidatedForm#Select
   * @see ValidatedForm#Field
   * @example
   * <F.Select name='language' options={{
   *   en: 'English',
   *   fr: 'francais'
   * }} />
   * @example
   * <F.Select name='language'>
   *   <option value='en'>English</option>
   *   <option value='fr'>francais</option>
   * </F.Select>
   * @example
   * <!-- Generated input -->
   * <select name="language">
   *   <option value="en">English</option>
   *   <option value="fr">francais</option>
   * </select>
   * @example
   * <F.Select name='language' options={{
   *   English: {
   *     'en-ca': 'Canada',
   *     'en-us': 'United States of America'
   *   },
   *   francais: {
   *     'fr-ca': 'canada',
   *     'fr-fr': 'france'
   *   }
   * }} />
   * @example
   * <!-- Generated input -->
   * <select name="language">
   *   <optgroup label="English">
   *     <option value="en-ca">Canada</option>
   *     <option value="en-us">United States of America</option>
   *   </optgroup>
   *   <optgroup label="francais">
   *     <option value="fr-ca">canada</option>
   *     <option value="fr-fr">france</option>
   *   </optgroup>
   * </select>
   */
  function Select (props: SelectProps): JSX.Element {
    // Alias select input
    return (
      <Field type='select' {...props} />
    )
  }

  /**
   * Creates a submit button.
   *
   * @function ValidatedForm#Submit
   * @see ValidatedForm#Button
   * @example
   * <F.Submit />
   * @example
   * <!-- Generated button -->
   * <button type="submit">Submit</button>
   */
  function Submit (props: ButtonProps): JSX.Element {
    // Alias submit button
    return (
      <Button type='submit' label='Submit' {...props} />
    )
  }

  /**
   * Creates a tel input.
   *
   * @function ValidatedForm#Tel
   * @see ValidatedForm#Field
   * @example
   * <F.Tel name='cellphone' />
   * @example
   * <!-- Generated input -->
   * <input type="tel" name="cellphone" />
   */
  function Tel (props: ElementProps): JSX.Element {
    // Alias tel input
    return (
      <Field type='tel' {...props} />
    )
  }

  /**
   * Creates a text input.
   *
   * @function ValidatedForm#Text
   * @see ValidatedForm#Field
   * @example
   * <F.Text name='username' />
   * @example
   * <!-- Generated input -->
   * <input type="text" name="username" />
   */
  function Text (props: ElementProps): JSX.Element {
    // Alias text input
    return (
      <Field type='text' {...props} />
    )
  }

  /**
   * Creates a textarea input.
   *
   * @function ValidatedForm#Textarea
   * @see ValidatedForm#Field
   * @example
   * <F.Textarea name='description' />
   * @example
   * <!-- Generated input -->
   * <textarea name="description"></textarea>
   */
  function Textarea (props: ElementProps): JSX.Element {
    // Alias textarea input
    return (
      <Field type='textarea' {...props} />
    )
  }

  /**
   * Creates a time input.
   *
   * @function ValidatedForm#Time
   * @see ValidatedForm#Field
   * @example
   * <F.Time name='eta' />
   * @example
   * <!-- Generated input -->
   * <input type="time" name="eta" />
   */
  function Time (props: ElementProps): JSX.Element {
    // Alias time input
    return (
      <Field type='time' {...props} />
    )
  }

  /**
   * Creates a url input.
   *
   * @function ValidatedForm#Url
   * @see ValidatedForm#Field
   * @example
   * <F.Url name='website' />
   * @example
   * <!-- Generated input -->
   * <input type="url" name="website" />
   */
  function Url (props: ElementProps): JSX.Element {
    // Alias url input
    return (
      <Field type='url' {...props} />
    )
  }

  /**
   * Creates a week input.
   *
   * @function ValidatedForm#Week
   * @see ValidatedForm#Field
   * @example
   * <F.Week name='yearweek' />
   * @example
   * <!-- Generated input -->
   * <input type="week" name="yearweek" />
   */
  function Week (props: ElementProps): JSX.Element {
    // Alias week input
    return (
      <Field type='week' {...props} />
    )
  }

  // ---------------------------------------------------------------------------
  // Public form methods

  /**
   * Clears the form inputs and errors. This is not the same as {@link ValidatedForm#reset}
   * or {@link ValidatedForm#fresh}.
   *
   * @function ValidatedForm#clear
   * @example
   * // Clearing the entire form
   * F.clear()
   * // or
   * F.clear([])
   * @example
   * // Clearing a single input
   * F.clear('username')
   * // or
   * F.clear(['username'])
   * @example
   * // Clearing multiple inputs
   * F.clear('username', 'nickname')
   * // or
   * F.clear(['username', 'nickname'])
   */
  const clear = (...inputs: FormInputs | [FormInputs]): void => {
    // Iterate over the given inputs or fallback to the entire form
    getInputs(inputs).forEach((name: FormInput): void => {
      // Clear sets inputs to blank
      setState('inputs', name, '')

      // And removes validation errors
      setState('errors', name, undefined)
    })
  }

  /**
   * Configures the form settings.
   *
   * These are not meant to be reactive attributes.
   *
   * This method also exists on the {@link ValidatedForm.globals} object
   * to help configure global settings as well.
   *
   * @function ValidatedForm#configure
   * @see ValidatedForm#instance
   * @see ValidatedForm~InstanceConfiguration
   * @see ValidatedForm.globals
   * @see ValidatedForm~GlobalsConfiguration
   * @example
   * // Configuring all forms
   * ValidatedForm.globals.configure({
   *   inputCssClass: 'form-control'
   * })
   * @example
   * // Configuring all forms manually
   * ValidatedForm.globals.inputCssClass = 'form-control'
   * @example
   * // Configuring a single form on creation
   * const F = ValidatedForm({
   *   username () {}
   * }).configure({
   *   id: 'custom-form-id'
   * })
   * @example
   * // Configuring a form at a later time
   * F.configure({
   *   id: 'custom-form-id'
   * })
   * @example
   * // Configuring a form manually
   * F.instance.id = 'custom-form-id'
   */
  function configure (settings: Partial<InstanceConfiguration>): FormInstance {
    // Don't need to do anything fancy
    Object.assign(instance, settings)

    // Returning the same object as the ValidatedForm function
    return api
  }

  /**
   * Resets a form to its initial state. This is not the same as {@link ValidatedForm#reset}
   * or {@link ValidatedForm#clear}.
   *
   * @function ValidatedForm#fresh
   * @throws {TypeError} Object values are not strings or arrays of strings
   * @example
   * // Basic form with default starting values
   * const F = ValidatedForm({
   *   username () {},
   *   nickname () {}
   * }, {
   *   username: 'user@domain.tld'
   * })
   *
   * // Setting a dirty value for the next example
   * F.set({ nickname: 'root' })
   *
   * // { username: 'user@domain.tld', nickname: 'root' }
   * F.get() // same as F.values
   * @example
   * // Without parameters, fresh will reset all form values and clear all errors
   * F.fresh()
   *
   * // { username: 'user@domain.tld', nickname: '' }
   * F.get()
   * @example
   * // Providing a parameter replaces the initial values and resets the form to said initial values
   * F.fresh({ username: '', nickname: 'root' })
   *
   * // { username: '', nickname: 'root' }
   * F.get()
   */
  function fresh (values?: FormValues): void {
    // Allow fresh to change the initial values
    if (is.obj(values)) {
      for (const name of Object.keys(values)) {
        // Get the new value for an input which can be set to undefined
        const value: FormValue | undefined = values[name]

        // Values can be strings or arrays of strings
        if (is.arr(value)) {
          // Ensure all entries are strings if the value is an array
          if (value.some((v) => !is.str(v))) throw new TypeError(ERROR_VALUES)
        } else {
          // Ensure the value is a string if it's set and isn't an array
          if (value !== undefined && !is.str(value)) throw new TypeError(ERROR_VALUES)
        }
      }

      // No errors thrown so update the initial values
      initialValues = values
    }

    batch(() => {
      // Calling fresh resets all attributes
      attributes.forEach((name: FormInput): void => {
        // Get the intial value or fall back to an empty string
        const value: FormValue = initialValues[name] as FormValue ?? ''

        // Reset the input state controlling form values
        setState('inputs', name, value)

        // Reset the commit state
        setState('commits', name, value)

        // Clear dirty computations
        setState('dirty', name, undefined)

        // Clear all validation errors
        setState('errors', name, undefined)
      })
    })
  }

  /**
   * Returns one or more values for the given input names.
   *
   * The values for inputs can also be accessed using {@link ValidatedForm#values}.
   *
   * @function ValidatedForm#get
   * @see ValidatedForm#values
   * @example
   * // Getting all form inputs and values
   * // { username: 'user@domain.tld', password: '********', nickname: '' }
   * F.get()
   * // or
   * F.get([])
   * @example
   * // Getting a single input value
   * // { username: 'user@domain.tld' }
   * F.get('username')
   * // or
   * F.get(['username'])
   * @example
   * // Getting multiple input values
   * // { username: 'user@domain.tld', nickname: '' }
   * F.get('username', 'nickname')
   * // or
   * F.get(['username', 'nickname'])
   */
  function gets (...inputs: FormInputs | [FormInputs]): FormValues {
    // Iterate over the given inputs or fallback to the entire form
    return getInputs(inputs).reduce((results: FormValues, name: FormInput) => {
      // Using reduce to return an object instead of building one with a loop
      return Object.assign(results, { [name]: state.inputs[name] })
    }, {})
  }

  /**
   * Helper function to generate the input ID for a given input name.
   *
   * @function ValidatedForm#id
   * @see ValidatedForm~InstanceConfiguration
   * @example
   * // Getting the form's ID
   * // 'f-00000'
   * F.id()
   * F.instance.id
   * @example
   * // Getting an input's ID
   * // 'f-00000-username'
   * F.id('username')
   * F.instance.id + '-username'
   */
  function id (name?: FormInput | string): string {
    // Returns the configured or generated form ID along with an optional input name
    return `${instance.id}${name !== undefined ? `-${name}` : ''}`
  }

  /**
   * Resets the form data to the previously committed one. This is not the same as
   * {@link ValidatedForm#clear} or {@link ValidatedForm#fresh}.
   *
   * To persist data between resets, use either {@link ValidatedForm#Submit} or
   * {@link ValidatedForm#save}.
   *
   * @function ValidatedForm#reset
   * @example
   * // Resetting the entire form
   * F.reset()
   * // or
   * F.reset([])
   * @example
   * // Resetting a single input
   * F.reset('username')
   * // or
   * F.reset(['username'])
   * @example
   * // Resetting multiple inputs
   * F.reset('username', 'nickname')
   * // or
   * F.reset(['username', 'nickname'])
   */
  function reset (...inputs: FormInputs | [FormInputs]): void {
    // Iterate over the given inputs or fallback to the entire form
    getInputs(inputs).forEach((name: FormInput): void => {
      // Reset updates the inputs to their last saved value
      setState('inputs', name, state.commits[name])

      // And also removes errors
      setState('errors', name, undefined)
    })
  }

  /**
   * Saves the state of the form or a set of inputs for resetting forms.
   *
   * Note that this function does not run validation and will save invalid inputs.
   *
   * @function ValidatedForm#save
   * @example
   * // Saving the entire form
   * F.save()
   * // or
   * F.save([])
   * @example
   * // Saving a single input
   * F.save('username')
   * // or
   * F.save(['username'])
   * @example
   * // Saving multiple inputs
   * F.save('username', 'nickname')
   * // or
   * F.save(['username', 'nickname'])
   */
  function save (...inputs: FormInputs | [FormInputs]): void {
    // Iterate over the given inputs or fallback to the entire form
    getInputs(inputs).forEach((name: FormInput): void => {
      // Save input values for use with reset()
      setState('commits', name, state.inputs[name])
    })
  }

  /**
   * Sets a value or a set of values on related inputs.
   *
   * This will trigger a dirty computation and the dirtyCssClass to be added.
   *
   * Since the value argument is passed onto setState, it can also be a function
   * that returns the new value. The same cannot be done by using an input object.
   *
   * @function ValidatedForm#set
   * @example
   * // Using set with the value as a string
   * F.set('username', 'user@domain.tld')
   * @example
   * // Using set with the value as a function
   * F.set('username', (name) => name.toLowerCase())
   * @example
   * // Using set with the input as an object
   * F.set({ username: 'user@domain.tld' })
   */
  function sets (input: FormInput | FormValues, value?: FormValue | InputSetter): void {
    // Require use of two strings arguments or one argument that's an object of strings
    if (!is.str(input) && !is.obj(input)) {
      throw new TypeError('set requires `input` to be an object of changes or a string')
    }

    // Allow set(name, value)
    if (is.str(input)) {
      // Only allow setting values defined as validators
      if (!is.key(validators, input)) throw new GenericError('set requires `input` to be a validator key')

      // Require value to be a string, an array of strings, or a setter function
      if (!is.str(value) && (!is.arr(value) || !value.every((v): boolean => is.str(v))) && !is.fn(value)) {
        throw new GenericError('set requires `value` to be a string, an array of strings, or a setter function')
      }

      // Update the state
      setState('inputs', input, value)
    }

    // Allow set({ [name]: value })
    if (is.obj(input)) {
      // Iterate through each input being updated
      Object.keys(input).forEach((key: FormInput): void => {
        // Ensure each of them have validators
        if (!is.key(validators, key)) throw new GenericError('set requires `input` object keys to be validator keys')

        // Ensure the values are all strings
        if (!is.str(input[key]) && (!is.arr(input[key]) || !(input[key] as unknown[]).every((v): boolean => is.str(v)))) {
          throw new GenericError('set requires `input` object values to be strings or arrays of strings')
        }
      })

      // All inputs have validators so update the state
      setState('inputs', input)
    }
  }

  /**
   * Validates the form or a set of inputs.
   *
   * @function ValidatedForm#validate
   * @example
   * // Validating the entire form
   * F.validate()
   * // or
   * F.validate([])
   * @example
   * // Validating a single input
   * F.validate('username')
   * // or
   * F.validate(['username'])
   * @example
   * // Validating multiple inputs
   * F.validate('username', 'nickname')
   * // or
   * F.validate(['username', 'nickname'])
   */
  function validate (...inputs: FormInputs | [FormInputs]): boolean {
    // Iterate over the given inputs or fallback to the entire form
    return getInputs(inputs).reduce((valid: boolean, name: string): boolean => {
      // Run each validator
      updateState(name)

      // Return the valid state, carrying previous failed validation
      return valid && state.errors[name] === undefined
    }, true)
  }

  // Defining an API object so both `ValidatedForm()` and `configure()` return it
  const api: FormInstance = {
    /**
     * Reactive property that changes based on the dirty state of all named inputs.
     *
     * Newly created forms are considered clean.
     *
     * @readonly
     * @instance
     * @memberof ValidatedForm
     * @example
     * //
     * // JS
     * //
     * const onClickCancel = () => {
     *   if (F.isDirty && confirm('Discard changes?')) F.reset()
     * }
     * @example
     * <!--
     *   - JSX
     *   -->
     * <Show when={F.isDirty}>
     *   <div>You have unsaved changes.</div>
     * </Show>
     */
    get isDirty (): boolean {
      return getDirty()
    },
    /**
     * Reactive property that changes based on the valid state of all named inputs.
     *
     * The valid state depends on {@link ValidatedForm#validate} being called. Newly
     * created forms are considered valid.
     *
     * @readonly
     * @instance
     * @memberof ValidatedForm
     * @example
     * //
     * // JS
     * //
     * const onClickSave = () => {
     *   // Prefer calling `F.validate()` to ensure validation is run at least once
     *   if (F.isValid) F.save()
     * }
     * @example
     * <!--
     *   - JSX
     *   -->
     * <Show when={!F.isValid}>
     *   <div>Please correct the errors below.</div>
     * </Show>
     */
    get isValid (): boolean {
      return getValid()
    },
    /**
     * Form configuration for a single instance.
     *
     * These are not meant to be reactive attributes.
     *
     * Using instance properties will override values set in {@link ValidatedForm.globals}.
     *
     * @instance
     * @memberof ValidatedForm
     * @see ValidatedForm~InstanceConfiguration
     */
    instance,
    /**
     * Reactive dirty form attributes.
     *
     * Keys on the dirty object are only defined when inputs differ from their reset
     * state.
     *
     * @readonly
     * @instance
     * @memberof ValidatedForm
     * @example
     * //
     * // State
     * //
     * F.dirty == {
     *   username: {
     *     old: '',
     *     new: 'user@domain.tld'
     *   }
     * }
     * @example
     * //
     * // Usage
     * //
     * const onSubmit = () => {
     *   if (F.dirty.username) {
     *     console.log(F.dirty.username.old + ' is now ' + F.dirty.username.new)
     *   } else {
     *     console.log('no username changes')
     *   }
     * }
     */
    dirty: state.dirty,
    /**
     * Reactive form errors.
     *
     * Keys on the error object are only defined when inputs are invalid.
     *
     * @readonly
     * @instance
     * @memberof ValidatedForm
     * @example
     * //
     * // State
     * //
     * F.errors == {
     *   username: 'is required'
     * }
     * @example
     * //
     * // Usage
     * //
     * const onSubmit = () => {
     *   if (F.errors.username) {
     *     console.log('submit failed because username ' + F.errors.username)
     *   } else {
     *     console.log('submit success')
     *   }
     * }
     */
    errors: state.errors,
    /**
     * Reactive form values.
     *
     * @readonly
     * @instance
     * @memberof ValidatedForm
     * @example
     * //
     * // State
     * //
     * F.values == {
     *   username: 'user@domain.tld'
     * }
     * @example
     * //
     * // Usage
     * //
     * const onSubmit = () => {
     *   if (F.values.username === '') {
     *     console.log('submit as guest')
     *   } else {
     *     console.log('submit as user ' + F.values.username)
     *   }
     * }
     */
    values: state.inputs,
    // Helper functions
    clear,
    configure,
    fresh,
    id,
    reset,
    save,
    validate,
    get: gets,
    set: sets,
    // Bound components
    Button,
    Checkbox,
    CheckboxList,
    Color,
    Date,
    DatetimeLocal,
    Email,
    Error,
    Field,
    File,
    Form,
    Hidden,
    Image,
    Input,
    Label,
    List,
    Month,
    Number,
    Password,
    Radio,
    RadioList,
    Range,
    Reset,
    Search,
    Select,
    Submit,
    Tel,
    Text,
    Textarea,
    Time,
    Url,
    Week,
    Group
  }

  // Returns the API when calling ValidatedForm()
  return api
}
