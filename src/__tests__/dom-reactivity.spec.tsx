/* global describe expect jest test */
// Package imports
import '@testing-library/jest-dom/extend-expect'
import userEvent from '@testing-library/user-event'
import { createResource, Suspense } from 'solid-js'
import { fireEvent, render } from 'solid-testing-library'

// Application imports
import ValidatedForm, { FormValue } from '..'

// Helper function that simply waits one tick to let reactivity settle
const timeout = async (): Promise<void> => await new Promise((resolve) => setTimeout(resolve))

describe('DOM reactivity', (): void => {
  // Ensure transformers run
  test('transforms', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Mocked validator that accepts any value
    const validator = jest.fn((): void => {})

    // Mocked tranformer that uppercases the value(transformers must return a value)
    const transformer = jest.fn((value) => value.toUpperCase())

    // Need a validated input and transformer to run transformations
    const F = ValidatedForm({ input: validator }, { input: transformer })

    // Rendering a text input
    const { getByRole } = render(() => <F.Input id='test' name='input' type='text' />)

    // Getting the input DOM element
    const ref = getByRole('textbox') as Element

    // Clicking on the input and typing hello doesn't run validation or transformation
    await user.type(ref, 'hello')

    // Ensuring both validator and transformer weren't called
    expect(validator).not.toBeCalled()
    expect(transformer).not.toBeCalled()

    // Blurring the input will trigger transformation then validation
    fireEvent.blur(ref)

    // Ensuring both validator and transformer were called(blur and change)
    expect(validator).toBeCalledTimes(2)
    expect(transformer).toBeCalledTimes(2)

    // Ensuring the state value was transformed to uppercase
    expect(F.values).toHaveProperty('input', 'HELLO')
  })

  // Ensure errors are reactive
  test('<Error>', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Mocked validator
    const input = jest.fn((value) => {
      if (value === '') return 'is required'
    })

    // Need a validated input to test errors
    const F = ValidatedForm({ input })

    // Rendering a text input and an error associated to the same input
    const { container, getByRole } = render(() => (
      <>
        <F.Input name='input' type='text' />
        <F.Error name='input' />
      </>
    ))

    // Getting the input DOM element
    const ref = getByRole('textbox') as Element

    // Ensuring only the input is rendered
    expect(container).toMatchSnapshot('init <input>')

    // Triggering the validation error using onChange since it runs on empty inputs
    fireEvent.change(ref)

    // Ensuring the validator was called using on change
    expect(input).toHaveBeenCalledTimes(1)

    // Ensuring the error is displayed when the input becomes invalid
    expect(container).toMatchSnapshot('updates to show <span.error>')

    // Clicking the input and typing hello to set a value
    await user.type(ref, 'hello')

    // Blurring the input to trigger validation since it now has a value
    fireEvent.blur(ref)

    // Ensuring the validator was called again except on blur and change this time
    expect(input).toHaveBeenCalledTimes(3)

    // Ensuring the error is hidden when the input becomes valid
    expect(container).toMatchSnapshot('updates to hide <span.error>')
  })

  describe('<Form>', (): void => {
    // Ensure forms receive reactive classes
    test('reactive CSS classes', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validated input to test dirty and invalid classes
      const F = ValidatedForm({ input }).configure({ dirtyCssClass: 'drt', invalidCssClass: 'inv' })

      // Rendering a form with a text input(don't need to set the input ID since the form's is now static)
      const { container, getByRole } = render(() => (
        <F.Form id='test'>
          <F.Input name='input' type='text' />
        </F.Form>
      ))

      // Getting the input DOM element
      const ref = getByRole('textbox') as Element

      // Ensuring the form and inputs don't have either class on render
      expect(container).toMatchSnapshot('init <form> <input>')

      // Clicking on the input and typing hello to trigger onKeyUp which updates our state and values
      await user.type(ref, 'hello')

      // Ensuring the dirty state is computed
      expect(F.dirty).toHaveProperty('input')

      // Ensuring the input and form both received the dirty class
      expect(container).toMatchSnapshot('updates <input> to .drt')

      // Simulating blurring an input with a value to run validation on it
      fireEvent.change(ref)

      // Ensuring both form and input have the dirty and invalid class
      expect(container).toMatchSnapshot('updates <input> to .drt.inv')

      // Clicking the input, selecting all, and deleting the value only fires onKeyUp
      await user.clear(ref)

      // Simulating blurring the input to trigger validation
      fireEvent.change(ref)

      // Ensuring the form and input are no longer dirty but still invalid
      expect(container).toMatchSnapshot('updates <input> to .inv')
    })

    test('forwards onReset event', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()
      const input = jest.fn()
      const F = ValidatedForm({ input })
      const { getByRole } = render(() => (
        <F.Form id='test' onReset={input}>
          <F.Reset />
        </F.Form>
      ))

      const reset = getByRole('button') as Element

      await user.click(reset)

      // Ensure reset was called
      expect(input).toHaveBeenCalledTimes(1)
      // Ensure the default behaviour is to call preventDefault()
      expect(input.mock.calls[0][0]).toMatchObject({ defaultPrevented: true })
    })
  })

  // Ensure groups receive reactive classes
  test('<Group>', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Validator that always returns an error
    const input = (): string => 'error'

    // Need a validated input to test dirty and invalid classes
    const F = ValidatedForm({ input }).configure({ dirtyCssClass: 'drt', invalidCssClass: 'inv' })

    // Rendering a group with a text input
    const { container, getByRole } = render(() => (
      <F.Group name={['input']}>
        <F.Input name='input' type='text' />
      </F.Group>
    ))

    // Getting the input DOM element
    const ref = getByRole('textbox') as Element

    // Ensuring the input is wrapped with a div
    expect(container).toMatchSnapshot('init <div> <input>')

    // Clicking on the input and typing hello to trigger dirty computation
    await user.type(ref, 'hello')

    // Ensuring the input and group both receive the dirty class
    expect(container).toMatchSnapshot('updates to .drt')

    // Simulate blurring the input to trigger validation
    fireEvent.change(ref)

    // Ensuring the input and group both receive the invalid class
    expect(container).toMatchSnapshot('updates to .drt.inv')

    // Clicking on the input, selecting all, and deleting the value
    await user.clear(ref)

    // Simulate blurring the input to trigger validation
    fireEvent.change(ref)

    // Ensuring the input and group both remove the dirty class
    expect(container).toMatchSnapshot('updates to .inv')
  })

  // Ensure various input types set the state values
  describe('<Input>', (): void => {
    describe('<Checkbox>', (): void => {
      test('handles onClick changes', async (): Promise<void> => {
        // Setup complex user interaction
        const user = userEvent.setup()

        // Validator that accepts any value
        const input = (): void => {}

        // Need a validated input to test checkbox input DOM
        const F = ValidatedForm({ input })

        // Rendering two checkbox inputs
        const { getByRole } = render(() => (
          <>
            <F.Checkbox name='input' value='en' label='English' />
            <F.Checkbox name='input' value='fr' label='francais' />
          </>
        ))

        // Getting the English checkbox DOM element
        const en = getByRole('checkbox', { name: 'English' }) as Element

        // Getting the French checkbox DOM element
        const fr = getByRole('checkbox', { name: 'francais' }) as Element

        // Ensuring rendering doesn't convert default string values to arrays
        expect(F.values).toHaveProperty('input', '')
        expect(F.dirty).toMatchObject({})

        // Selecting the English checkbox
        await user.click(en)

        // Ensuring the English checkbox converts our value into an array and adds en to it
        expect(F.values).toHaveProperty('input', expect.arrayContaining(['en']))
        expect(F.dirty).toMatchObject({ input: { new: ['en'], old: [] } })

        // Selecting the French checkbox
        await user.click(fr)

        // Ensuring the state value contains both English and French
        expect(F.values).toHaveProperty('input', expect.arrayContaining(['en', 'fr']))
        expect(F.dirty).toMatchObject({ input: { new: ['en', 'fr'], old: [] } })

        // Deselecting the English checkbox
        await user.click(en)

        // Ensuring en is no longer in our input array
        expect(F.values).toHaveProperty('input', expect.arrayContaining(['fr']))
        expect(F.dirty).toMatchObject({ input: { new: ['fr'], old: [] } })

        // Deselecting the French checkbox
        await user.click(fr)

        // Ensuring the state value is now an empty array
        expect(F.values).toHaveProperty('input', expect.arrayContaining([]))
        expect(F.dirty).toMatchObject({})
      })

      // Ensuring specifying a default value as an array checks all the appropriate boxes
      test('onChange value out of range', (): void => {
        const any = (): void => {}
        const F = ValidatedForm({ any }, { any: ['zz'] })
        const { getByRole } = render(() => (
          <>
            <F.Checkbox name='any' value='en' label='English' />
            <F.Checkbox name='any' value='fr' label='francais' />
          </>
        ))

        const en = getByRole('checkbox', { name: 'English' })
        const fr = getByRole('checkbox', { name: 'francais' })

        fireEvent.change(en as Element)

        expect(en).not.toBeChecked()
        expect(fr).not.toBeChecked()
        expect(F.values).toMatchObject({ any: ['zz'] })
      })

      // Ensuring specifying a default value as an array checks all the appropriate boxes
      test('onChange string to string', (): void => {
        const any = (): void => {}
        const F = ValidatedForm({ any }, { any: 'fr' })
        const { getByRole } = render(() => (
          <>
            <F.Checkbox name='any' value='en' label='English' />
            <F.Checkbox name='any' value='fr' label='francais' />
          </>
        ))

        const en = getByRole('checkbox', { name: 'English' })
        const fr = getByRole('checkbox', { name: 'francais' })

        expect(en).not.toBeChecked()
        expect(fr).toBeChecked()
        expect(F.values.any).toEqual('fr')

        // Firing a change event shouldn't modify the checked value or the form value
        fireEvent.change(fr as Element)

        // Firing a change event that doesn't update the state (value exists)
        expect(en).not.toBeChecked()
        expect(fr).toBeChecked()
        expect(F.values.any).toEqual('fr')
      })

      // Ensuring specifying a default value as an array checks all the appropriate boxes
      test('onChange string to Array<string>', (): void => {
        const any = (): void => {}
        const F = ValidatedForm({ any }, { any: 'en' })
        const { getByRole } = render(() => (
          <>
            <F.Checkbox name='any' value='en' label='English' />
            <F.Checkbox name='any' value='fr' label='francais' />
          </>
        ))

        const en = getByRole('checkbox', { name: 'English' })
        const fr = getByRole('checkbox', { name: 'francais' }) as HTMLInputElement

        // Checked value set via console or something, not our form instance
        fr.checked = true

        expect(en).toBeChecked()
        expect(fr).toBeChecked()
        expect(F.values.any).toEqual('en')

        // Firing a change event so the French option is added to our form instance
        fireEvent.change(fr)

        expect(en).toBeChecked()
        expect(fr).toBeChecked()
        expect(F.values.any).toEqual(['en', 'fr'])
      })
    })

    describe('<Radio>', () => {
      // Ensure radio inputs updates single value
      test('handles onClick', async (): Promise<void> => {
        // Setup complex user interaction
        const user = userEvent.setup()

        // Validator that accepts any value
        const input = (): void => {}

        // Need a validated input to test radio input DOM
        const F = ValidatedForm({ input })

        // Rendering two radio inputs
        const { getByRole } = render(() => (
          <>
            <F.Radio name='input' value='en' label='English' />
            <F.Radio name='input' value='fr' label='francais' />
          </>
        ))

        // Getting the English radio DOM element
        const en = getByRole('radio', { name: 'English' }) as Element

        // Getting the French radio DOM element
        const fr = getByRole('radio', { name: 'francais' }) as Element

        // Selecting the English radio
        await user.click(en)

        // Ensuring the English radio sets the en value
        expect(F.values).toHaveProperty('input', 'en')

        // Selecting the French radio
        await user.click(fr)

        // Ensuring the state value is updated to French
        expect(F.values).toHaveProperty('input', 'fr')
      })

      // Ensure Radio components handle default values correctly
      test('handles onChange checked', (): void => {
        const any = (): void => {}
        const F = ValidatedForm({ any })
        const { getByRole } = render(() => (
          <>
            <F.Radio name='any' value='en' label='English' />
            <F.Radio name='any' value='fr' label='francais' checked />
          </>
        ))

        const en = getByRole('radio', { name: 'English' })
        const fr = getByRole('radio', { name: 'francais' }) as HTMLInputElement

        fr.checked = true

        expect(en).not.toBeChecked()
        expect(fr).toBeChecked()
        expect(F.values.any).toEqual('')

        fireEvent.change(fr)
        fireEvent.change(en as Element)

        expect(en).not.toBeChecked()
        expect(fr).toBeChecked()
        expect(F.values.any).toEqual('fr')
      })
    })

    // Ensure select one updates single value
    test('<Select>', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Validator that accepts any value
      const input = (): void => {}

      // Need a validated input to test select DOM
      const F = ValidatedForm({ input })

      // Rendering a select with two options
      const { getByRole } = render(() => (
        <F.Select name='input' options={{ en: 'English', fr: 'francais' }} />
      ))

      // Getting the select DOM element
      const ref = getByRole('combobox') as Element

      // Getting the English option DOM element
      const en = getByRole('option', { name: 'English' }) as HTMLOptionElement

      // Getting the French option DOM element
      const fr = getByRole('option', { name: 'francais' }) as HTMLOptionElement

      // Selecting the English option
      await user.selectOptions(ref, en)

      // Ensuring the English option sets the en value
      expect(F.values).toHaveProperty('input', 'en')

      // Selecting the French option
      await user.selectOptions(ref, fr)

      // Ensuring the state value is updated to French
      expect(F.values).toHaveProperty('input', 'fr')
    })

    test('<Select multiple>', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Validator that accepts any value
      const input = (): void => {}

      // Need a validated input to test select DOM
      const F = ValidatedForm({ input })

      // Rendering a select multiple with two options
      const { getByRole } = render(() => (
        <F.Select multiple name='input' options={{ en: 'English', fr: 'francais' }} />
      ))

      // Getting the select DOM element
      const ref = getByRole('listbox') as Element

      // Getting the English option DOM element
      const en = getByRole('option', { name: 'English' }) as HTMLOptionElement

      // Getting the French option DOM element
      const fr = getByRole('option', { name: 'francais' }) as HTMLOptionElement

      // Selecting the English option
      await user.selectOptions(ref, en)

      // Multiple selects always return arrays
      expect(F.values).toHaveProperty('input', expect.arrayContaining(['en']))

      // Selecting the French option
      await user.selectOptions(ref, fr)

      // Ensuring both English and French options are selected
      expect(F.values).toHaveProperty('input', expect.arrayContaining(['en', 'fr']))

      // Deselecting the English option but not the French one
      await user.deselectOptions(ref, en)

      // Ensuring only the French option remains selected
      expect(F.values).toHaveProperty('input', expect.arrayContaining(['fr']))

      // Deselecting everything returns an empty array
      await user.deselectOptions(ref, fr)

      // Ensuring empty multiple select still return arrays
      expect(F.values).toHaveProperty('input', expect.arrayContaining([]))
    })

    test('<Text>', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Validator that accepts any value
      const input = (): void => {}

      // Need a validated input to test state values
      const F = ValidatedForm({ input })

      // Rendering a label and input
      const { getByRole } = render(() => <F.Text name='input' />)

      // Getting the input DOM element
      const ref = getByRole('textbox') as Element

      // Clicking on the input and typing hello sets state values by onKeyUp
      await user.type(ref, 'hello')

      // Ensuring value state reflects input
      expect(F.values).toHaveProperty('input', 'hello')

      // Clicking on the input, selecting all, and deleting to clear the value
      await user.clear(ref)

      // Simulating blurring the input to trigger a change event
      fireEvent.change(ref)

      // Ensuring the value state reflects the change
      expect(F.values).toHaveProperty('input', '')
    })

    test('calls on events', (): void => {
      const any = (): void => {}
      const F = ValidatedForm({ any })
      const blur = jest.fn()
      const change = jest.fn()
      const keyUp = jest.fn()
      const { getByRole } = render(() => (
        <F.Input
          name='any'
          type='text'
          onBlur={blur}
          onChange={change}
          onKeyUp={keyUp}
        />
      ))

      const el = getByRole('textbox') as Element

      fireEvent.keyUp(el)

      expect(keyUp).toHaveBeenCalledTimes(1)

      fireEvent.focus(el)
      fireEvent.blur(el)

      expect(blur).toHaveBeenCalledTimes(1)

      fireEvent.change(el)

      expect(change).toHaveBeenCalledTimes(1)
    })
  })
})

// Ensure <Suspense> behaves with forms
describe('triggers <Suspense> fallback', (): void => {
  // Ensure setting initial values via the Form component triggers <Suspense>
  test('<Form> values', async (): Promise<void> => {
    // Validator that accepts any input
    const input = (): void => {}

    // Creating our form instance
    const F = ValidatedForm({ input }).configure({ id: 'test' })

    // Creating the deferred resource
    const [resource] = createResource<string>(async () => await new Promise((resolve) => resolve('hello')))

    // Rendering the form
    const { container, getByRole } = render(() => (
      <Suspense fallback={<p>loading</p>}>
        <F.Form values={{ input: resource() as string }}>
          <F.Text name='input' />
        </F.Form>
      </Suspense>
    ))

    // Helper function that returns the text input element
    const ref = (): unknown => getByRole('textbox')

    // Since <Suspense> hides our text input, trying to access it throws an error
    expect(ref).toThrow()

    // Ensure the form is being hidden
    expect(container).toMatchSnapshot('shows loading')

    // Need to wait a tick for reactive updateState
    await timeout()

    // Ensure the input is accessible and has the right value
    expect(ref()).toHaveValue('hello')

    // Ensure resource value is returned
    expect(resource()).toEqual('hello')

    // Ensure the form is now visible
    expect(container).toMatchSnapshot('shows input')
  })
})

// Ensure read only signals are reactive to input
describe('signal reactivity', (): void => {
  // Ensure user input is required to update isDirty
  test('F.isDirty reflects dirty state', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Validator that accepts any value
    const input = (): void => {}

    // Need a validated input to test state values
    const F = ValidatedForm({ input })

    // Rendering a label and input
    const { getByRole } = render(() => <F.Text name='input' />)

    // Getting the input DOM element
    const ref = getByRole('textbox') as Element

    // Form starts clean
    expect(F.isDirty).toEqual(false)

    // Clicking on the input and typing hello sets the input as dirty
    await user.type(ref, 'hello')

    // Ensure the dirty signal is properly reflected
    expect(F.isDirty).toEqual(true)

    // Clicking on the input, selecting all, and deleting to clear the value
    await user.clear(ref)

    // Simulating blurring the input to trigger a change event
    fireEvent.change(ref)

    // Ensure the cleared input resets the dirty state and signal
    expect(F.isDirty).toEqual(false)
  })

  // Ensure errors are required to update isValid
  test('F.isValid reflects error state', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Validator that requires at least 1 character
    const input = (value: FormValue): undefined | string => {
      if (value.length === 0) return 'is required'
    }

    // Need a validated input to test state values
    const F = ValidatedForm({ input })

    // Rendering a label and input
    const { getByRole } = render(() => <F.Text name='input' />)

    // Getting the input DOM element
    const ref = getByRole('textbox') as Element

    // Form starts valid
    expect(F.isValid).toEqual(true)

    // Clicking on the input and typing hello
    await user.type(ref, 'hello')

    // Blurring the input to run validation
    fireEvent.blur(ref)

    // Clicking on the input, selecting all, and deleting to clear the value
    await user.clear(ref)

    // Blurring the input to run validation
    fireEvent.blur(ref)

    // Ensure the valid signal is properly reflected
    expect(F.isValid).toEqual(false)

    // Clicking on the input and typing hello to make the form valid
    await user.type(ref, 'hello')

    // Blurring the input to run validation
    fireEvent.blur(ref)

    // Ensure the updated input changes the valid state and signal
    expect(F.isValid).toEqual(true)
  })
})
