/* global describe expect jest test */
// Package imports
import '@testing-library/jest-dom/extend-expect'
import userEvent from '@testing-library/user-event'
import { render } from 'solid-testing-library'

// Application imports
import ValidatedForm from '..'

// Testing contructing new forms
describe('configure defaults:', (): void => {
  // Ensure dirty class is disabled by default
  test('{ dirtyCssClass: false }', (): void => {
    // Validator that accepts any value
    const input = (): void => {}

    // Need a validated input for dirty class to work
    const F = ValidatedForm({ input })

    // Rendering a text input
    const { container } = render(() => <F.Input id='test' name='input' type='text' />)

    // Ensuring input isn't dirty by default
    expect(container).toMatchSnapshot('init <input>')

    // Setting any value triggers the dirty computation
    F.set('input', 'hello')

    // No dirty class to add
    expect(container).toMatchSnapshot('updates match init')
  })

  // Ensure error spans are displayed by default
  test('{ errorCssClass: error }', (): void => {
    // Validator that always returns an error
    const input = (): string => 'error'

    // Need a validated input for the error class to work
    const F = ValidatedForm({ input })

    // Rendering an error component directly
    const { container } = render(() => <F.Error name='input' />)

    // Ensuring it renders blank container since there are no errors
    expect(container).toMatchSnapshot('init empty')

    // Trigger the validation error
    F.validate()

    // Ensuring the error span is
    expect(container).toMatchSnapshot('shows <span.error>')
  })

  // Ensure grouping is disabled by default
  test('{ groupCssClass: false }', (): void => {
    // Don't need inputs to check grouping
    const F = ValidatedForm({})

    // Rendering a component that would usually wrap with a configured class
    const { container } = render(() => <F.Group>content</F.Group>)

    // Ensuring the content isn't wrapped
    expect(container).toMatchSnapshot('init <div>')
  })

  // Ensure inputs don't have classes by default
  test('{ inputCssClass: false }', (): void => {
    // Don't need inputs to check input class
    const F = ValidatedForm({})

    // Rendering a blank input that would inherit a configured class
    const { container } = render(() => <F.Input id='test' name='input' type='text' />)

    // Ensuring the input doesn't have a class by default
    expect(container).toMatchSnapshot('init <input>')
  })

  // Ensure invalid class is added to invalid inputs
  test('{ invalidCssClass: invalid }', (): void => {
    // Validator that always returns an error
    const input = (): string => 'error'

    // Need a validated input to test invalid inputs
    const F = ValidatedForm({ input })

    // Rendering an input to be invalidated
    const { container } = render(() => <F.Input id='test' name='input' type='text' />)

    // Ensuring the input doesn't start invalid
    expect(container).toMatchSnapshot('init <input>')

    // Triggering the validation error
    F.validate()

    // Ensuring input now has the invalid class
    expect(container).toMatchSnapshot('updates to <input.invalid>')
  })

  // Ensure labels don't have classes by default
  test('{ labelCssClass: false }', (): void => {
    // Don't need inputs to check labels
    const F = ValidatedForm({})

    // Rendering a label
    const { container } = render(() => <F.Label for='test' />)

    // Ensuring label doesn't contain a class
    expect(container).toMatchSnapshot('init <label>')
  })

  // Ensure form reset is prevented by default
  test('{ preventReset: true }', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Handler that checks if event.preventDefault was called
    const reset = (event: Event): void => {
      // The onReset handler prevents the default event before running anything else
      expect(event.defaultPrevented).toEqual(true)
    }

    // Don't need inputs to check form events
    const F = ValidatedForm({})

    // Rendering a form with a reset button
    const { getByRole } = render(() => (
      <F.Form onReset={reset}>
        <F.Reset />
      </F.Form>
    ))

    // Let the reset event handler run our expect()
    await user.click(getByRole('button') as Element)
  })

  // Ensure form submit is prevented by default
  test('{ preventSubmit: true }', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Handler that checks if event.preventDefault was called
    const submit = (event: SubmitEvent): void => {
      // The onSubmit handler prevents the default event before running anything else
      expect(event.defaultPrevented).toEqual(true)
    }

    // Don't need inputs to check form events
    const F = ValidatedForm({})

    // Rendering a form with a submit button
    const { getByRole } = render(() => (
      <F.Form onSubmit={submit}>
        <F.Submit />
      </F.Form>
    ))

    // Let the submit event handler run our expect()
    await user.click(getByRole('button') as Element)
  })

  // Ensure form submit runs validation by default
  test('{ validateSave: true }', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Validator that always returns an error
    const input = jest.fn((): string => 'error')

    // Need a validated input to check saving
    const F = ValidatedForm({ input })

    // Rendering a form with a submit button
    const { getByRole } = render(() => (
      <F.Form>
        <F.Submit />
      </F.Form>
    ))

    // Ensuring the form doesn't validate on render
    expect(F.errors).not.toHaveProperty('input', 'error')

    // Triggering the form validation via the submit button
    await user.click(getByRole('button') as Element)

    // Ensuring the validator was called
    expect(input).toHaveBeenCalled()

    // Ensuring the validator populated our error state
    expect(F.errors).toHaveProperty('input', 'error')
  })

  // Ensure error spans are visible by default
  test('{ visibleErrors: true }', (): void => {
    // Validator that always returns an error
    const input = (): string => 'error'

    // Need a validated input to check error spans
    const F = ValidatedForm({ input })

    // Rendering a field component containing a label, an input, and an invisible error
    const { container } = render(() => <F.Text id='test' name='input' />)

    // Ensuring our input doesn't trigger and render an error by default
    expect(container).toMatchSnapshot('init <label> <input>')

    // Triggering the validation error
    F.validate()

    // Ensuring the error span is made visible
    expect(container).toMatchSnapshot('shows <span.error>')
  })

  // Ensure labels are visible by default
  test('{ visibleLabels: true }', (): void => {
    // Validator that accepts any value
    const input = (): void => {}

    // Need a validated input to generate a label
    const F = ValidatedForm({ input })

    // Rendering a field component with a label, an input, and an invisible error
    const { container } = render(() => <F.Text id='test' name='input' />)

    // Ensuring the label was rendered
    expect(container).toMatchSnapshot('init <label> <input>')
  })
})
