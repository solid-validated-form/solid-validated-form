/* global describe expect jest test */
// Package imports
import '@testing-library/jest-dom/extend-expect'

// Application imports
import { call, capitalize, is } from '../utils'

// Testing our event forwarding function
describe('utils.call', (): void => {
  // Mock function to capture times called along with parameters passed
  const fn = jest.fn()

  // Generic click event
  const click = new MouseEvent('click', {
    bubbles: true,
    cancelable: true
  })

  // Optional parameter for array-based callbacks
  const param = 'param'

  // Passing anything other than a function or an array does nothing
  test('call(0, MouseEvent)', (): void => {
    // Making coverage happy
    call(0 as any, click)

    expect(fn.mock.calls.length).toEqual(0)
  })

  // Passing a function as the first argument will call it
  test('call(Function, MouseEvent)', (): void => {
    call(fn, click)

    expect(fn.mock.calls.length).toEqual(1)
    expect(fn.mock.calls[0].length).toEqual(1)
    expect(fn.mock.calls[0][0]).toBe(click)
  })

  // Passing an array without a function does nothing
  test('call([0], MouseEvent)', (): void => {
    // Making coverage happy
    call([0 as any], click)

    // Mock keeps its previous calls
    expect(fn.mock.calls.length).toEqual(1)
  })

  // Passing an array is also a valid solid-js paradigm
  test('call([Function], MouseEvent)', (): void => {
    call([fn], click)

    expect(fn.mock.calls.length).toEqual(2)
    expect(fn.mock.calls[1].length).toEqual(1)
    expect(fn.mock.calls[1][0]).toBe(click)
  })

  // Passing an array with an argument
  test('call([Function, String], MouseEvent)', (): void => {
    call([fn, param], click)

    expect(fn.mock.calls.length).toEqual(3)
    expect(fn.mock.calls[2].length).toEqual(2)
    expect(fn.mock.calls[2][0]).toBe(param)
    expect(fn.mock.calls[2][1]).toBe(click)
  })
})

// Testing our string case conversion and capitalization function
describe('utils.capitalize', (): void => {
  const result = 'Hello World'
  const validate = (str: string): any => expect(capitalize(str)).toEqual(result)

  // Testing our capitalization function
  test('capitalize(String)', (): void => {
    [
      // Converting capitalization
      'hello world',
      'Hello World',
      'HELLO WORLD',
      // Trimming whitespace
      'hello world ',
      ' hello world',
      ' hello world ',
      '  hello  world  '
    ].forEach(validate)
  })

  // Testing the kebab-case conversion
  test('capitalize(kebab-case)', (): void => {
    [
      // Converting kebab-case strings before capitalizing them
      'hello-world',
      'hello-world-',
      '-hello-world-',
      '--hello--world--',
      // Mixing casing and capitalization
      'Hello-World',
      'HELLO-WORLD'
    ].forEach(validate)
  })

  // Testing the snake_case conversion
  test('capitalize(snake_case)', (): void => {
    [
      // Converting kebab-case strings before capitalizing them
      'hello_world',
      'hello_world_',
      '_hello_world_',
      '__hello__world__',
      // Mixing casing and capitalization
      'Hello_World',
      'HELLO_WORLD'
    ].forEach(validate)
  })

  // Testing the camelCase conversion
  test('capitalize(camelCase)', (): void => {
    [
      // Converting camelCase strings before capitalizing them
      'helloWorld',
      'HelloWorld'
    ].forEach(validate)
  })
})

// Testing the type checker
describe('utils.is', (): void => {
  // Testing our shorthand array type checker
  test('is.arr()', (): void => {
    // An array is an array
    expect(is.arr([])).toEqual(true)

    // An object is not an array
    expect(is.arr({})).toEqual(false)

    // Everything else is not an array
    expect(is.arr(null)).toEqual(false)
  })

  // Testing our shorthand on event function type checker following Solid's paradigm
  test('is.call()', (): void => {
    // Passing anything other than a function or an array returns false
    expect(is.call(0)).toEqual(false)

    // A simple function is callable
    expect(is.call((): void => {})).toEqual(true)

    // Array must contain a function as the first value
    expect(is.call([0])).toEqual(false)

    // Don't need to worry about validating additional array entries
    expect(is.call([(): void => {}])).toEqual(true)
  })

  // Testing our shorthand function type checker
  test('is.fn()', (): void => {
    // Undefined is not a function
    expect(is.fn(0)).toEqual(false)

    // Named function for checking
    function named (): void {}

    // A named function is a function
    expect(is.fn(named)).toEqual(true)

    // An anonymous function is a function
    expect(is.fn(function () {})).toEqual(true)

    // An arrow function is a function
    expect(is.fn((): void => {})).toEqual(true)

    // An asynchronous function is a function
    expect(is.fn(async (): Promise<void> => {})).toEqual(true)

    // A promise is not a function
    expect(is.fn(new Promise<void>((resolve) => resolve()))).toEqual(false)

    // Everything else is not a function
    expect(is.fn(0)).toEqual(false)
  })

  // Testing our shortcut hasOwnProperty
  test('is.key()', (): void => {
    const obj = { a: 0 }

    expect(() => is.key(0 as any, 'a')).toThrow(/requires.+object/i)
    expect(is.key(obj, 'a')).toEqual(true)
    expect(is.key(obj, 'b')).toEqual(false)
  })

  // Testing our shortcut number type checker
  test('is.num()', (): void => {
    // A number is a number
    expect(is.num(0)).toEqual(true)

    // A numerical string is not a number
    expect(is.num('1')).toEqual(false)

    // NaN is not a number
    expect(is.num(0 / 0)).toEqual(false)
  })

  // Testing our shorthand object type checker
  test('is.obj()', (): void => {
    // And object is an object
    expect(is.obj({})).toEqual(true)

    // An array is not an object
    expect(is.obj([])).toEqual(false)

    // Null is not an object(even though it is)
    expect(is.obj(null)).toEqual(false)
  })

  // Testing our shorthand string type checker
  test('is.str()', (): void => {
    // Anything other than a string returns false
    expect(is.str(0)).toEqual(false)

    // An empty string is valid
    expect(is.str('')).toEqual(true)

    // Testing the minimum length required for a string
    expect(is.str('', 1)).toEqual(false)

    // Valid type and length
    expect(is.str('0', 1)).toEqual(true)
  })
})
