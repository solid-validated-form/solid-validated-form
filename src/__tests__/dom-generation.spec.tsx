/* global describe expect test */
//
// Package imports
//

import '@testing-library/jest-dom/extend-expect'
import { render } from 'solid-testing-library'
import type { JSX } from 'solid-js'

//
// Application imports
//

import ValidatedForm, { FieldProps } from '..'

describe('DOM generation:', (): void => {
  // Ensuring Textarea components render textareas
  test('<Textarea> generates <label> <textarea>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Textarea id='area' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Button components render button type buttons
  test('<Button> generates <button type=button>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Button />)

    // Buttons don't have labels by default
    expect(container).toMatchSnapshot()
  })

  // Ensuring checkboxes render correctly
  describe('<Checkbox>', (): void => {
    // Ensuring Checkbox components render checkbox inputs
    test('generates <label> <input type=checkbox>', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => <F.Checkbox id='checkbox' name='input' value='checked' />)

      expect(container).toMatchSnapshot()
    })

    // Ensuring multiple checkboxes don't use the same ID
    test('generates unique ID per checkbox', (): void => {
      const any = (): void => {}
      const F = ValidatedForm({ any }).configure({ id: 'test' })
      const { container } = render(() => (
        <>
          <F.Checkbox name='any' value='en' label='English' />
          <F.Checkbox name='any' value='fr' label='francais' />
          <F.Checkbox name='any' value='zz' label='Other' id='other' />
        </>
      ))

      expect(container).toMatchSnapshot()
    })

    // Ensuring specifying a default value as a string checks the appropriate box
    test('default value checks one', (): void => {
      const any = (): void => {}
      const F = ValidatedForm({ any }, { any: 'fr' })
      const { getByRole } = render(() => (
        <>
          <F.Checkbox name='any' value='en' label='English' />
          <F.Checkbox name='any' value='fr' label='francais' />
        </>
      ))

      const en = getByRole('checkbox', { name: 'English' })
      const fr = getByRole('checkbox', { name: 'francais' })

      expect(en).not.toBeChecked()
      expect(fr).toBeChecked()
    })

    // Ensuring specifying a default value as an array checks all the appropriate boxes
    test('default value checks many', (): void => {
      const any = (): void => {}
      const F = ValidatedForm({ any }, { any: ['en', 'fr'] })
      const { getByRole } = render(() => (
        <>
          <F.Checkbox name='any' value='en' label='English' />
          <F.Checkbox name='any' value='fr' label='francais' />
        </>
      ))

      const en = getByRole('checkbox', { name: 'English' })
      const fr = getByRole('checkbox', { name: 'francais' })

      expect(en).toBeChecked()
      expect(fr).toBeChecked()
    })
  })

  // Ensuring checkbox lists render correctly
  describe('<CheckboxList>', (): void => {
    test('options must be an object', (): void => {
      const input = (): void => {}
      const F = ValidatedForm({ input })

      expect(() => render(() => <F.CheckboxList name='input' options={['en', 'fr'] as any} />)).toThrowError(/options.+object/)
    })

    // Ensuring CheckboxList components render checkbox inputs without overlapping IDs
    test('generates <label> and multiple <input type=checkbox>', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => (
        <F.CheckboxList
          id='checkbox' name='input' options={{
            en: 'English',
            fr: 'francais'
          }}
        />
      ))

      expect(container).toMatchSnapshot()
    })

    // Ensuring specifying a default value as a string checks the appropriate box
    test('default value checks one', (): void => {
      const any = (): void => {}
      const F = ValidatedForm({ any }, { any: 'fr' })
      const { getByRole } = render(() => (
        <F.CheckboxList
          id='checkbox' name='any' options={{
            en: 'English',
            fr: 'francais'
          }}
        />
      ))

      const en = getByRole('checkbox', { name: 'English' })
      const fr = getByRole('checkbox', { name: 'francais' })

      expect(en).not.toBeChecked()
      expect(fr).toBeChecked()
    })

    // Ensuring specifying a default value as an array checks all the appropriate boxes
    test('default value checks many', (): void => {
      const any = (): void => {}
      const F = ValidatedForm({ any }, { any: ['en', 'fr'] })
      const { getByRole } = render(() => (
        <F.CheckboxList
          id='checkbox' name='any' options={{
            en: 'English',
            fr: 'francais'
          }}
        />
      ))

      const en = getByRole('checkbox', { name: 'English' })
      const fr = getByRole('checkbox', { name: 'francais' })

      expect(en).toBeChecked()
      expect(fr).toBeChecked()
    })
  })

  // Ensure Color components render color inputs
  test('<Color> generates <label> <input type=color>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Color id='color' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Date components render date inputs
  test('<Date> generates <label> <input type=date>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Date id='date' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure DatetimeLocal components render datetime-local inputs
  test('<DatetimeLocal> generates <label> <input type=datetime-local>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.DatetimeLocal id='datetime' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Email components render email inputs
  test('<Email> generates <label> <input type=email>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Email id='email' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Error components are removed when errors are cleared
  test('<Error> generates <span?>', (): void => {
    // Validator that always returns an error
    const error = (): string => 'error'
    const F = ValidatedForm({ error })
    const { container } = render(() => <F.Error name='error' />)

    // Creating DOM does not run validation so the error doesn't get immediately displayed
    expect(container).toMatchSnapshot('init empty')

    // Run validation that will always fail
    F.validate()

    // Now the error DOM is shown
    expect(container).toMatchSnapshot('update show <span.error>')

    // Clear errors
    F.clear()

    // No errors hides the DOM again
    expect(container).toMatchSnapshot('update hide <span.error>')
  })

  // Ensure Field components render DOM properly
  test('<Field> generates <label> <input> <span?>', (): void => {
    // Validator that always returns an error
    const field = (): string => 'error'
    const F = ValidatedForm({ field })
    const { container } = render(() => <F.Field id='field' name='field' type='text' />)

    // Not visible unless validation runs
    expect(container).toMatchSnapshot('init')

    // Forcing failing validation
    F.validate()

    // Both label and error span contain the capitalized label
    expect(container).toMatchSnapshot('update show <span.error>')

    // Clear errors
    F.clear()

    // No errors hides the DOM again
    expect(container).toMatchSnapshot('update hide <span.error>')
  })

  // Ensure File components render file inputs
  test('<File> generates <label> <input type=file>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.File id='file' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensuring Form components create forms with assigned ID
  test('<Form> generates <form>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Form id='form' />)

    expect(container).toMatchSnapshot()
  })

  // Ensuring Group components create divs
  describe('<Group> generates <div>:', (): void => {
    // Group does not wrap by default
    test('no props', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => <F.Group>content</F.Group>)

      expect(container).toMatchSnapshot('no wrap')
    })

    // Add class by specifying the class attribute
    test('props.class = grp', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => <F.Group class='grp'>content</F.Group>)

      expect(container).toMatchSnapshot('wrap with div.grp')
    })

    // Specifying the name property as a string does not wrap by default
    test('props.name = input (string)', (): void => {
      // Specifying a validator that would make the group reactive
      const input = (): void => {}
      const F = ValidatedForm({ input })
      const { container } = render(() => <F.Group name='input'>content</F.Group>)

      expect(container).toMatchSnapshot('no wrap')
    })

    // Specifying the name property as an array wraps with a reactive div(valid = no classes)
    test('props.name = [input] (array)', (): void => {
      // Specifying a validator to make the group reactive
      const input = (): void => {}
      const F = ValidatedForm({ input })
      const { container } = render(() => <F.Group name={['input']}>content</F.Group>)

      expect(container).toMatchSnapshot('wraps with div')
    })

    // Add class by specifying a configuration option
    test('configured no props', (): void => {
      const F = ValidatedForm({}).configure({ groupCssClass: 'grp' })
      const { container } = render(() => <F.Group>content</F.Group>)

      expect(container).toMatchSnapshot('wraps with div.grp')
    })

    // Disabling class added by configuration
    test('configured props.class = false', (): void => {
      const F = ValidatedForm({}).configure({ groupCssClass: 'grp' })
      const { container } = render(() => <F.Group class={false}>content</F.Group>)

      expect(container).toMatchSnapshot('no wrap')
    })
  })

  // Ensure Hidden components render hidden inputs and no errors or labels
  test('<Hidden> generates <input type=hidden>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Hidden id='hidden' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensuring Input components render the right input elements
  describe('<Input> types:', (): void => {
    test('requires `name` to be a string with length', (): void => {
      const F = ValidatedForm({})
      const error = /name.+must.+be.+string.+length/

      expect(() => render(() => <F.Input type='text' />)).toThrowError(error)
      expect(() => render(() => <F.Input type='text' name='' />)).toThrowError(error)
      expect(() => render(() => <F.Input type='text' name='test' />)).not.toThrowError()
    })

    // Ensuring we get an input tag by default
    test('text generates <input>', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => <F.Input id='input-text' name='input' type='text' />)

      expect(container).toMatchSnapshot()
    })

    // Ensuring we get a select instead of an input
    test('select generates <select>', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => <F.Input id='input-select' name='input' type='select' />)

      expect(container).toMatchSnapshot()
    })

    // Ensuring we get a textarea instead of an input
    test('textarea generates <textarea>', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => <F.Input id='input-textarea' name='input' type='textarea' />)

      expect(container).toMatchSnapshot()
    })

    test('checkbox and radio requires `value` to be a string', (): void => {
      const F = ValidatedForm({})
      const error = /value.+must.+be.+string/

      expect(() => render(() => <F.Input name='input' type='text' />)).not.toThrowError()
      expect(() => render(() => <F.Input name='input' type='checkbox' />)).toThrowError(error)
      expect(() => render(() => <F.Input name='input' type='checkbox' value='1' />)).not.toThrowError()
      expect(() => render(() => <F.Input name='input' type='radio' />)).toThrowError(error)
      expect(() => render(() => <F.Input name='input' type='radio' value='1' />)).not.toThrowError()
    })

    test('select requires `options` to be an object when defined', (): void => {
      const F = ValidatedForm({})
      const error = /options.+must.+be.+object/

      expect(() => render(() => <F.Input name='input' type='select' />)).not.toThrowError()
      expect(() => render(() => <F.Input name='input' type='select' options={['0', '1', '2'] as any} />)).toThrowError(error)
      expect(() => render(() => <F.Input name='input' type='select' options={{ 0: '0', 1: '1', 2: '2' }} />)).not.toThrowError()
    })

    test('handles classList property', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => (
        <F.Input classList={{ cls: true }} name='input' type='text' />
      ))

      expect(container).toMatchSnapshot()
    })
  })

  test('<Image> generates <label> <input type=image>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Image id='image' name='input' />)

    expect(container).toMatchSnapshot()
  })

  describe('<Label>', () => {
    // Ensure Label components render labels related to inputs
    test('generates <label>', (): void => {
      // const label = (): void => {}
      const F = ValidatedForm({ })
      const { container } = render(() => <F.Label for='label' name='label' />)

      expect(container).toMatchSnapshot()
    })

    test('accepts label property', (): void => {
      const label = (): void => {}
      const F = ValidatedForm({ label })
      const { container } = render(() => <F.Label for='label' label='custom label' />)

      expect(container).toMatchSnapshot()
    })

    test('accepts children property', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => <F.Label for='label'>custom label</F.Label>)

      expect(container).toMatchSnapshot()
    })
  })

  // Ensure List components render properly
  describe('<List>', (): void => {
    test('argument handling', (): void => {
      const F = ValidatedForm({})

      expect(() => <F.List name='input' component={() => <></>} options={undefined as any} />).toThrowError(/options.+object/)
      expect(() => <F.List name='input' component={{} as any} options={{}} />).toThrowError(/component.+function/)
    })

    test('generates one <label> with multiple <input>', (): void => {
      const F = ValidatedForm({}).configure({ id: 'test' })
      const Comp = (props: FieldProps): JSX.Element => (
        <input type='checkbox' name={props.name} value={props.value} />
      )
      const { container } = render(() => (
        <F.List name='input' component={Comp} options={{ en: 'English', fr: 'francais' }} />
      ))

      expect(container).toMatchSnapshot()
    })

    test('generates unique IDs per input', (): void => {
      const F = ValidatedForm({})
      const Comp = (props: FieldProps): JSX.Element => (
        <input type='checkbox' id={props.id} name={props.name} value={props.value} />
      )
      const { getAllByRole } = render(() => (
        <F.List name='input' component={Comp} options={{ en: 'English', fr: 'francais' }} />
      ))
      const [en, fr] = getAllByRole('checkbox') as Element[]

      expect(en).toBeDefined()
      expect(fr).toBeDefined()
      expect(en.id).not.toEqual(fr.id)
    })

    test('generates unique IDs with parent list ID', (): void => {
      const F = ValidatedForm({})
      const Comp = (props: FieldProps): JSX.Element => (
        <input type='checkbox' id={props.id} name={props.name} value={props.value} />
      )
      const { container } = render(() => (
        <F.List id='test' name='input' component={Comp} options={{ en: 'English', fr: 'francais' }} />
      ))

      expect(container).toMatchSnapshot()
    })
  })

  // Ensure Month components render month inputs
  test('<Month> generates <label> <input type=month>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Month id='month' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Number components render number inputs
  test('<Number> generates <label> <input type=number>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Number id='number' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Password components render password inputs
  test('<Password> generates <label> <input type=password>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Password id='password' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Radio components render radio inputs
  describe('<Radio>', (): void => {
    // Ensure Radio components render radio inputs
    test('generates <label> <input type=radio>', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => <F.Radio id='radio' name='input' value='checked' />)

      expect(container).toMatchSnapshot()
    })

    // Ensure Radio components handle default values correctly
    test('default value is checked', (): void => {
      const any = (): void => {}
      const F = ValidatedForm({ any }, { any: 'fr' })
      const { getByRole } = render(() => (
        <>
          <F.Radio name='any' value='en' label='English' />
          <F.Radio name='any' value='fr' label='francais' />
        </>
      ))

      const en = getByRole('radio', { name: 'English' })
      const fr = getByRole('radio', { name: 'francais' })

      expect(en).not.toBeChecked()
      expect(fr).toBeChecked()
    })
  })

  // Ensure RadioList components renders lists of radio inputs
  describe('<RadioList>', (): void => {
    // Ensure options prop is an object
    test('requires options to be an object', (): void => {
      const F = ValidatedForm({})
      const jsx = (): JSX.Element => (
        <F.RadioList id='radio' name='input' options={['English', 'francais'] as any} />
      )

      expect(() => render(jsx() as any)).toThrowError(/options.+object/)
    })

    // Ensure multiple inputs are generated with no overlaping IDs
    test('generates <label> and multiple <input type=radio>', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => (
        <F.RadioList id='radio' name='input' options={{ en: 'English', fr: 'francais' }} />
      ))

      expect(container).toMatchSnapshot()
    })

    // Ensure default value is checked
    test('default value is checked', (): void => {
      const any = (): void => {}
      const F = ValidatedForm({ any }, { any: 'fr' })
      const { getByRole } = render(() => (
        <F.RadioList id='radio' name='any' options={{ en: 'English', fr: 'francais' }} />
      ))

      const en = getByRole('radio', { name: 'English' })
      const fr = getByRole('radio', { name: 'francais' })

      expect(en).not.toBeChecked()
      expect(fr).toBeChecked()
    })
  })

  // Ensure Range components render range inputs
  test('<Range> generates <label> <input type=range>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Range id='range' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Reset components render reset button types
  test('<Reset> generates <button type=reset>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Reset />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Search components render search inputs
  test('<Search> generates <label> <input type=search>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Search id='search' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Select components render select inputs
  describe('<Select> generates <select>:', (): void => {
    // Select as dropdown
    test('select-one', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => <F.Select id='select-one' name='input' />)

      expect(container).toMatchSnapshot()
    })

    // Select as clickable list
    test('select-multiple', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => <F.Select id='select-multiple' name='input' multiple />)

      expect(container).toMatchSnapshot()
    })

    // Select options defined as a simple property object
    test('options as simple object property', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => (
        <F.Select
          id='select-simple'
          name='input'
          options={{
            en: 'English',
            fr: 'francais'
          }}
        />
      ))

      expect(container).toMatchSnapshot()
    })

    // Select options defined as a complex property object rendering optgroups
    test('options as complex object property', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => (
        <F.Select
          id='select-complex'
          name='input'
          options={{
            English: { 'en-ca': 'English Canada', 'en-us': 'English United States' },
            francais: { 'fr-ca': 'francais canada', 'fr-fr': 'francais france' }
          }}
        />
      ))

      expect(container).toMatchSnapshot()
    })

    // Select options as DOM
    test('options as child DOM', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => (
        <F.Select id='select-options' name='input'>
          <option value='en'>English</option>
          <option value='fr'>francais</option>
        </F.Select>
      ))

      expect(container).toMatchSnapshot()
    })

    // Select optgroups as DOM
    test('optgroups as child DOM', (): void => {
      const F = ValidatedForm({})
      const { container } = render(() => (
        <F.Select id='select-optgroups' name='input'>
          <optgroup label='English'>
            <option value='en-ca'>English Canada</option>
            <option value='en-us'>English United States</option>
          </optgroup>

          <optgroup label='francais'>
            <option value='fr-ca'>francais canada</option>
            <option value='fr-fr'>francais france</option>
          </optgroup>
        </F.Select>
      ))

      expect(container).toMatchSnapshot()
    })

    // Select one default value
    test('select-one default value', (): void => {
      const any = (): void => {}
      const F = ValidatedForm({ any }, { any: 'fr' })
      const { getByRole } = render(() => (
        <>
          <F.Select
            name='any'
            options={{
              en: 'English',
              fr: 'francais'
            }}
          />
        </>
      ))

      const en = getByRole('option', { name: 'English' })
      const fr = getByRole('option', { name: 'francais' })

      expect(en).toHaveProperty('selected', false)
      expect(fr).toHaveProperty('selected', true)
    })

    // Select multiple default value
    test('select-multiple default value', (): void => {
      const any = (): void => {}
      const F = ValidatedForm({ any }, { any: ['en', 'fr'] })
      const { getByRole } = render(() => (
        <>
          <F.Select
            name='any'
            options={{
              en: 'English',
              fr: 'francais'
            }}
            multiple
          />
        </>
      ))

      const en = getByRole('option', { name: 'English' })
      const fr = getByRole('option', { name: 'francais' })

      expect(en).toHaveProperty('selected', true)
      expect(fr).toHaveProperty('selected', true)
    })

    // test('select-one down arrow key', async (): void => {
    //   const user = userEvent.setup()
    //   const any = (): void => {}
    //   const F = ValidatedForm({ any })
    //   const { getByRole } = render(() => (
    //     <F.Select
    //       name='any'
    //       options={{
    //         en: 'English',
    //         fr: 'francais'
    //       }}
    //     />
    //   ))

    //   const el = getByRole('combobox') as HTMLSelectElement
    //   const en = getByRole('option', { name: 'English' }) as HTMLOptionElement

    //   expect(F.values.any).toEqual('')

    //   await user.selectOptions(el, en)

    //   expect(F.values.any).toEqual('en')

    //   await user.type(el, '{arrowdown}')

    //   // Arrow down doesn't work in emulation
    //   expect(F.values.any).toEqual('en')
    // })

    // test('select-multiple arrow down key', async (): void => {
    //   const user = userEvent.setup()
    //   const any = (): void => {}
    //   const F = ValidatedForm({ any })
    //   const { getByRole } = render(() => (
    //     <F.Select
    //       name='any'
    //       options={{
    //         en: 'English',
    //         fr: 'francais'
    //       }}
    //       multiple
    //     />
    //   ))

    //   const el = getByRole('listbox') as HTMLSelectElement
    //   const en = getByRole('option', { name: 'English' }) as HTMLOptionElement

    //   expect(F.values.any).toEqual('')

    //   await user.selectOptions(el, en)

    //   expect(F.values.any).toEqual(['en'])

    //   await user.type(el, '{arrowdown}')

    //   // Arrow down doesn't work in emulation
    //   expect(F.values.any).toEqual(['en'])
    // })

    // test('onBlur', (): void => {
    //   const user = userEvent.setup()
    //   const error = (): string => 'error'
    //   const F = ValidatedForm({ error }, { error: 'en' })
    //   const { getByRole } = render(() => (
    //     <F.Select
    //       name='error'
    //       options={{
    //         en: 'English',
    //         fr: 'francais'
    //       }}
    //       multiple
    //     />
    //   ))

    //   const el = getByRole('listbox') as HTMLSelectElement

    //   fireEvent.focus(el)
    //   fireEvent.blur(el)

    //   expect(F.errors).not.toHaveProperty('error', 'error')
    // })
  })

  // Ensure Submit components render submit buttons types
  test('<Submit> generates <button type=submit>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Submit />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Tel components render tel inputs
  test('<Tel> generates <label> <input type=tel>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Tel id='tel' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Text components render text inputs
  test('<Text> generates <label> <input type=text>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Text id='text' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Time components render time inputs
  test('<Time> generates <label> <input type=time>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Time id='time' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Url components render url inputs
  test('<Url> generates <label> <input type=url>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Url id='url' name='input' />)

    expect(container).toMatchSnapshot()
  })

  // Ensure Week components render week inputs
  test('<Week> generates <label> <input type=week>', (): void => {
    const F = ValidatedForm({})
    const { container } = render(() => <F.Week id='week' name='input' />)

    expect(container).toMatchSnapshot()
  })
})
