/* global describe expect jest test */
// Package imports
import '@testing-library/jest-dom/extend-expect'
import { render } from 'solid-testing-library'

// Application imports
import ValidatedForm, { FormValue } from '..'

// Testing contructing new forms
describe('configure basic:', (): void => {
  // Ensure argument handling
  test('requires objects', (): void => {
    // Mock attribute validator/transformer function
    const input = (): void => {}
    const transform = (value: FormValue): FormValue => value

    const validators = /requires.+object.+validation.+functions/i
    const transformers = /requires.+object.+transformer.+functions/i
    const values = /requires.+object.+strings.+array.+strings/i

    // // Require at least one argument
    // expect(() => ValidatedForm()).toThrowError(validators)

    // Validators must be an object
    expect(() => ValidatedForm([] as any)).toThrowError(validators)

    // Transformers must be an object
    expect(() => ValidatedForm({}, [] as any)).toThrowError(transformers)

    // Initial values must be an object
    expect(() => ValidatedForm({}, {}, [] as any)).toThrowError(values)

    // Each validator must be a function
    expect(() => ValidatedForm({ input: '' as any })).toThrowError(validators)

    // Ensure validation functions are valid
    expect(() => ValidatedForm({ input })).not.toThrow()

    // Each transformer must be a function(need to specify initialValues so it doesn't get flipped)
    expect(() => ValidatedForm({ input }, { input: '' }, { input: '' })).toThrowError(transformers)

    // Ensure transformer functions are valid
    expect(() => ValidatedForm({ input }, { input: transform })).not.toThrow()

    // Each initial value must be a string or an array of strings
    expect(() => ValidatedForm({ input }, { input: transform }, { input: transform })).toThrow(values)

    // Ensure initial values as strings are valid
    expect(() => ValidatedForm({ input }, { input: transform }, { input: '' })).not.toThrow()

    // Ensure initial values as arrays are valid
    expect(() => ValidatedForm({ input }, { input: transform }, { input: [] })).not.toThrow()
  })

  // Ensure generated form ID
  test('only has id property by default', (): void => {
    // Creating two forms to compare unique IDs
    const A = ValidatedForm({})
    const B = ValidatedForm({})

    // Using Math.random().toString(36) to generate IDs
    const id = expect.stringMatching(/f-[0-9a-z]{5}/)

    // Instance only contains the form ID by default
    expect(A.instance).toEqual({ id })
    expect(B.instance).toEqual({ id })

    // Ensuring each form has their own unique ID
    expect(A.instance.id).not.toEqual(B.instance.id)
  })

  // Ensure we can set the form's ID
  test('allows id to be set', (): void => {
    // Don't need inputs to test setting ID
    const F = ValidatedForm({})

    // Set ID via the instance property
    F.instance.id = 'test'

    // Rendering a form
    const { container } = render(() => <F.Form />)

    // Ensuring the form inherited the instance ID
    expect(container).toMatchSnapshot()
  })

  // Ensure configure updates the instance configuration
  test('using the configure method is preferred', (): void => {
    // Creating a configuration object with the form ID
    const config = { id: 'test' }

    // Creating the configured form
    const F = ValidatedForm({}).configure(config)

    // Ensuring the form instance isn't replaced(we use Object.assign)
    expect(F.instance).not.toBe(config)

    // Ensuring the form instance is properly configured
    expect(F.instance).toEqual(config)
  })

  // Ensure validators declare state keys
  test('declares accessible values', (): void => {
    // Validator that accepts any value
    const input = (): void => {}

    // Need a validated input to test state
    const F = ValidatedForm({ input })

    // Rendering a text field component
    const { getByLabelText } = render(() => <F.Text name='input' />)

    // Ensuring the form values state has a blank input defined
    expect(F.values).toHaveProperty('input', '')

    // Ensuring the dirty state is empty by default
    expect(F.dirty).not.toHaveProperty('input')

    // Ensuring the errors state is empty by default
    expect(F.errors).not.toHaveProperty('input')

    // Ensuring the input also started blank
    expect(getByLabelText(/input/i)).not.toHaveValue()
  })

  // Ensure default values populate state and inputs
  test('declares default values', (): void => {
    // Validator that accepts any values
    const input = (): void => {}

    // Need a validated input to test default value
    const F = ValidatedForm({ input }, { input: 'default' })

    // Rendering a text field component
    const { getByLabelText } = render(() => <F.Text name='input' />)

    // Ensuring the form values state is populated with our default values
    expect(F.values).toHaveProperty('input', 'default')

    // Ensuring the dirty state is also still empty
    expect(F.dirty).not.toHaveProperty('input')

    // Ensuring the errors state is still empty
    expect(F.errors).not.toHaveProperty('input')

    // Ensuring our rendered input has the default value as well
    expect(getByLabelText(/input/i)).toHaveValue('default')
  })

  // Ensure creating form elements doesn't run validation
  test('does not run validators', (): void => {
    // Mocked validator that accepts any value
    const input = jest.fn()

    // Need a validated input to test
    const F = ValidatedForm({ input })

    // Rendering a text field component
    render(() => <F.Text name='input' />)

    // Ensuring rendering does not call validators
    expect(input).not.toHaveBeenCalled()
  })

  // Ensure creating form elements doesn't run transformers
  test('does not run transformers', (): void => {
    // Mocked validator that accepts any value
    const input = jest.fn()

    // Need a validated input and transformer to test
    const F = ValidatedForm({ input }, { input })

    // Rendering a text field component
    render(() => <F.Text name='input' />)

    // Ensuring neither validator nor transformer are called
    expect(input).not.toHaveBeenCalled()
  })

  // Ensure transformers aren't run on default values
  test('does not run transformers on initial values', (): void => {
    // Mocked validator that accepts any value
    const input = jest.fn()

    // Need a validated input, a transformer, and a default value to test
    const F = ValidatedForm({ input }, { input }, { input: 'default' })

    // Rendering a text field component
    render(() => <F.Text name='input' />)

    // Ensuring neither validator nor transformer are called
    expect(input).not.toHaveBeenCalled()
  })
})
