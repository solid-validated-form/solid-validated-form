/* global describe expect test */
// Package imports
import '@testing-library/jest-dom/extend-expect'
import { render } from 'solid-testing-library'

// Application imports
import ValidatedForm from '..'

// Testing configuring single form instances
describe('configure DOM:', (): void => {
  // Ensure form instance configuration ID can be set via DOM
  test('<Form> id sets instance ID', (): void => {
    // Don't need inputs to test form ID
    const F = ValidatedForm({})

    // All forms start with an auto-generated ID(e.g. f-00000)
    expect(F.instance.id).not.toEqual('test')

    // Rendering assigns the ID
    render(() => <F.Form id='test' />)

    // Ensuring the ID was properly set
    expect(F.instance.id).toEqual('test')
  })

  test('<Form> values sets instance initial values', (): void => {
    // Validator that accepts any value
    const any = (): void => {}

    // Need at least one input to test initial values
    const F = ValidatedForm({ any })

    // Ensure form values/inputs are updated
    // expect(F.values.any).toEqual('')

    // Render the form to run its call to fresh()
    const { getByRole } = render(() => (
      <F.Form values={{ any: 'hello' }}>
        <F.Text name='any' />
      </F.Form>
    ))

    // Getting the text input element
    const input = getByRole('textbox')

    // Ensure its value is updated
    expect(input).toHaveValue('hello')

    // Ensure form values/inputs are also updated
    expect(F.values.any).toEqual('hello')
  })

  // Ensure error components can be configured
  describe('<Field> error sets <Error>:', (): void => {
    // Ensure setting errors can have custom messages
    test('custom error message', (): void => {
      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validated input to test errors
      const F = ValidatedForm({ input })

      // Rendering a text field component with a custom error message
      const { container } = render(() => (
        <F.Text error='custom' id='test' name='input' />
      ))

      // Triggering the validation error
      F.validate()

      // Ensuring the error span is rendered with the custom message
      expect(container).toMatchSnapshot()
    })

    // Ensure error spans can be disabled
    test('disable component', (): void => {
      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validated input to test errors
      const F = ValidatedForm({ input })

      // Rendering a text field component with the error disabled
      const { container } = render(() => (
        <F.Text error={false} id='test' name='input' />
      ))

      // Triggering the validation error
      F.validate()

      // Ensuring the error span was not rendered
      expect(container).toMatchSnapshot()
    })
  })

  // Ensure setting a form input group wraps
  describe('<Field> group sets <Group>:', (): void => {
    // Ensure the group property always wraps if it's not false
    test('wrap with group class', (): void => {
      // Validator that accepts any value
      const input = (): void => {}

      // Don't need inputs to test wrapping
      const F = ValidatedForm({ input })

      // Rendering a wrapped text field component
      const { container } = render(() => (
        <F.Text group='grp' id='test' name='input' />
      ))

      // Ensuring our text input is wrapped with the grp class
      expect(container).toMatchSnapshot()
    })

    // Ensure the group property can disable instance/global configured CSS class
    test('disable inheritance', (): void => {
      // Validator that accepts any value
      const input = (): void => {}

      // Need to configure the form with a group class to disable it
      const F = ValidatedForm({ input }).configure({ groupCssClass: 'grp' })

      // Rendering a text field component with its grouping disabled
      const { container } = render(() => (
        <F.Text group={false} id='test' name='input' />
      ))

      // Ensuring our text input is not wrapped
      expect(container).toMatchSnapshot()
    })
  })

  // Ensure labels and inputs are linked when setting a custom ID
  test('<Field> id sets <label> for <input>', (): void => {
    // Validator that accepts any value
    const input = (): void => {}

    // Don't need inputs to test labels and their for property
    const F = ValidatedForm({ input })

    // Rendering a text field with a custom ID
    const { container } = render(() => (
      <F.Text id='test' name='input' />
    ))

    // Ensuring label for matches input ID
    expect(container).toMatchSnapshot()
  })

  // Ensure label components can be configured
  describe('<Field> label sets <Label>:', (): void => {
    // Ensure setting labels can have custom messages
    test('custom label name', (): void => {
      // Validator that accepts any value
      const input = (): void => {}

      // Need a validated input to generate a label
      const F = ValidatedForm({ input })

      // Rendering a text field component with a custom label
      const { container } = render(() => (
        <F.Text label='custom' id='test' name='input' />
      ))

      // Ensuring the custom label is used instead of the auto-capitalized name
      expect(container).toMatchSnapshot()
    })

    // Ensure labels can be disabled
    test('disable component', (): void => {
      // Validator that accepts any value
      const input = (): void => {}

      // Need a validated input to generate a label
      const F = ValidatedForm({ input })

      // Rendering a text field component with the label disabled
      const { container } = render(() => (
        <F.Text label={false} id='test' name='input' />
      ))

      // Ensuring the label is not rendered
      expect(container).toMatchSnapshot()
    })
  })

  // Ensure classes property configures component class
  describe('<Field> classes sets individual component class:', (): void => {
    // Ensure an object is required if its truthy
    test('classes must be an object', (): void => {
      // Validator that accepts any value
      const input = (): void => {}

      // Need a validated input to generate a field
      const F = ValidatedForm({ input })

      // Helper function that creates a renderable field with customized classes
      const make = (classes: object) => () => (
        <F.Text id='test' name='input' classes={classes} />
      )

      // Ensure rendering a falsish value throws
      expect(() => render(make(false as any))).toThrow(/must.+be.+object/)

      // Ensure rendering classes as truthy and not an object throws an error
      expect(() => render(make('form-control' as any))).toThrow(/must.+be.+object/)

      // Ensure rendering an object is valid even if empty
      expect(() => render(make({}))).not.toThrow()
    })

    // Ensure Group class works
    test('classes.group sets <Group> class', (): void => {
      // Validator that accepts any value
      const input = (): void => {}

      // Need a validated input to generate a field
      const F = ValidatedForm({ input })

      // Rendering a text field component with a custom group class
      const { container } = render(() => (
        <F.Text id='test' name='input' classes={{ group: 'grp' }} />
      ))

      // Ensuring the input is wrapped with the group class
      expect(container).toMatchSnapshot()
    })

    // Ensure Label class works
    test('classes.label sets <Label> class', (): void => {
      // Validator that accepts any value
      const input = (): void => {}

      // Need a validated input to generate a field
      const F = ValidatedForm({ input })

      // Rendering a text field component with a custom label class
      const { container } = render(() => (
        <F.Text id='test' name='input' classes={{ label: 'lbl' }} />
      ))

      // Ensuring the label received the class
      expect(container).toMatchSnapshot()
    })

    // Ensure Input class works
    test('classes.input sets <Input> class', (): void => {
      // Validator that accepts any value
      const input = (): void => {}

      // Need a validated input to generate a field
      const F = ValidatedForm({ input })

      // Rendering a text field component with a custom input class
      const { container } = render(() => (
        <F.Text id='test' name='input' classes={{ input: 'put' }} />
      ))

      // Ensuring the input received the class
      expect(container).toMatchSnapshot()
    })

    // Ensure Error class works
    test('classes.error sets <Error> class', (): void => {
      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validated input to generate a field
      const F = ValidatedForm({ input })

      // Rendering a text field component with a custom error class
      const { container } = render(() => (
        <F.Text id='test' name='input' classes={{ error: 'err' }} />
      ))

      // Forcing validation to run and error DOM to show
      F.validate()

      // Ensuring the error received the class
      expect(container).toMatchSnapshot()
    })

    // Ensure setting multiple classes works
    test('classes sets multiple component class', (): void => {
      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validated input to generate a field
      const F = ValidatedForm({ input })

      // Rendering a text field component with all custom classes
      const { container } = render(() => (
        <F.Text
          id='test' name='input' classes={{
            group: 'grp',
            label: 'lbl',
            input: 'put',
            error: 'err'
          }}
        />
      ))

      // Forcing validation to run and error DOM to show
      F.validate()

      // Ensuring the all DOM received the classes
      expect(container).toMatchSnapshot()
    })
  })

  //   // Ensure List components can be configured
  //   describe('List components', (): void => {
  //     // Ensure components require options object
  //     test('requires options to be an object', (): void => {
  //       // Validator that always returns an error
  //       const input = (): string => 'error'
  //
  //       // Need a validated input to generate a field
  //       const F = ValidatedForm({ input })

//       // JSX render function
//       const check = () => <F.CheckboxList options={['en', 'fr']} name='input' />
//       const radio = () => <F.RadioList options={['en', 'fr']} />
//       const list = () => <F.List options={['en', 'fr']} component={F.Radio} name='input' />
//
//       expect(() => render(check)).toThrow(/options.+must.+be.+object/)
//       expect(() => render(radio)).toThrow(/options.+must.+be.+object/)
//       expect(() => render(list)).toThrow(/options.+must.+be.+object/)
//     })
//
//     // Ensure list component requires render function
//     test('<List> requires component function', (): void => {
//       // Validator that always returns an error
//       const input = (): string => 'error'
//
//       // Need a validated input to generate a field
//       const F = ValidatedForm({ input })
//
//       const list = () => <F.List options={{}} name='input' />
//
//       expect(() => render(list)).toThrow(/component.+must.+be.+function/)
//     })
//   })
})
