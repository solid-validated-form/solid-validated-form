/* global beforeEach describe expect jest test */
// Package imports
import '@testing-library/jest-dom/extend-expect'
import userEvent from '@testing-library/user-event'
import { render } from 'solid-testing-library'

// Application imports
import ValidatedForm from '..'

// Testing configuring single form instances
describe('configure globals:', (): void => {
  // Copying default global values
  const globals = { ...ValidatedForm.globals }

  // Resetting global values before each test
  beforeEach(() => Object.assign(ValidatedForm.globals, globals))

  // Ensure configuring globals affects all forms
  test('affects all forms', async (): Promise<void> => {
    // Configuring the global input class
    ValidatedForm.globals.inputCssClass = 'input'

    // Creating two forms that have no instance configuration
    const A = ValidatedForm({})
    const B = ValidatedForm({})

    // Rendering an input for both forms
    const { container: a } = render(() => <A.Input id='test-a' name='input-a' type='text' />)
    const { container: b } = render(() => <B.Input id='test-b' name='input-b' type='text' />)

    // Need to check container children since getBy would return both inputs and throw an error
    expect(a.firstChild).toHaveClass('input')
    expect(b.firstChild).toHaveClass('input')
  })

  // Ensure the configure helper is available on the globals object as well
  test('using the configure() method', (): void => {
    // Configuring the global input class
    ValidatedForm.globals.configure({
      inputCssClass: 'input'
    })

    // Creating two forms that have no instance configuration
    const A = ValidatedForm({})
    const B = ValidatedForm({})

    // Rendering an input for both forms
    const { container: a } = render(() => <A.Input id='test-a' name='input-a' type='text' />)
    const { container: b } = render(() => <B.Input id='test-b' name='input-b' type='text' />)

    // Need to check container children since getBy would return both inputs and throw an error
    expect(a.firstChild).toHaveClass('input')
    expect(b.firstChild).toHaveClass('input')
  })

  describe('dirtyCssClass', (): void => {
    // Ensure dirty class is disabled by default
    test('default (false) disables dirty class', (): void => {
      // Validator that accepts any value
      const input = (): void => {}

      // Need a validator for dirty class
      const F = ValidatedForm({ input })

      // Rendering a text field component
      const { container } = render(() => <F.Input id='test' name='input' type='text' />)

      // Ensuring the input doesn't contain the global dirty class on render
      expect(container).toMatchSnapshot('init <input>')

      // Setting any value triggers the dirty computation
      F.set('input', 'hello')

      // Ensuring the dirty class wasn't added to the text input
      expect(container).toMatchSnapshot('updates to <input> with .drt')
    })

    // Ensure dirty class is added to dirty inputs
    test('string enables dirty class', (): void => {
      // Configuring the global dirty input class
      ValidatedForm.globals.dirtyCssClass = 'drt'

      // Validator that accepts any value
      const input = (): void => {}

      // Need a validator for dirty class
      const F = ValidatedForm({ input })

      // Rendering a text field component
      const { container } = render(() => <F.Input id='test' name='input' type='text' />)

      // Ensuring the input doesn't contain the global dirty class on render
      expect(container).toMatchSnapshot('init <input>')

      // Setting any value triggers the dirty computation
      F.set('input', 'hello')

      // Ensuring the dirty class was added to the text input
      expect(container).toMatchSnapshot('updates to <input.drt>')
    })
  })

  // Ensure error class is applied to error span
  describe('errorCssClass', (): void => {
    // Ensure default behaviour persists
    test('default ("error") enables error class', (): void => {
      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validator to test errors
      const F = ValidatedForm({ input })

      // Rendering an invisible error component
      const { container } = render(() => <F.Error name='input' />)

      // Ensuring the error component is invisible
      expect(container).toMatchSnapshot('init empty')

      // Trigger the validation error
      F.validate()

      // Ensuring the error span is displayed with the correct class
      expect(container).toMatchSnapshot('shows <span.error>')
    })

    // Ensure error class is added to error spans
    test('string changes error class', (): void => {
      // Configuring the global error span class
      ValidatedForm.globals.errorCssClass = 'err'

      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validator to test errors
      const F = ValidatedForm({ input })

      // Rendering an invisible error component
      const { container } = render(() => <F.Error name='input' />)

      // Ensuring the error component is invisible
      expect(container).toMatchSnapshot('init empty')

      // Trigger the validation error
      F.validate()

      // Ensuring the error span is displayed with the correct class
      expect(container).toMatchSnapshot('shows <span.err>')
    })

    // Ensure error class is removed from error spans
    test('false disables error class', (): void => {
      // Configuring the global error span class
      ValidatedForm.globals.errorCssClass = false

      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validator to test errors
      const F = ValidatedForm({ input })

      // Rendering an invisible error component
      const { container } = render(() => <F.Error name='input' />)

      // Ensuring the error component is invisible
      expect(container).toMatchSnapshot('init empty')

      // Trigger the validation error
      F.validate()

      // Ensuring the error span is displayed without a class
      expect(container).toMatchSnapshot('shows <span> without .error')
    })
  })

  // Ensure group class is applied to group divs
  describe('groupCssClass', (): void => {
    // Ensuring default behaviour persists
    test('default (false) disables group', (): void => {
      // Don't need a validator to check grouping
      const F = ValidatedForm({})

      // Rendering a group component with content to wrap
      const { container } = render(() => <F.Group>content</F.Group>)

      // Ensuring the content isn't wrapped with a classless div
      expect(container).toMatchSnapshot('init content without wrapping div')
    })

    // Ensure group class is added to group divs
    test('string changes group class', (): void => {
      // Configuring the global group class
      ValidatedForm.globals.groupCssClass = 'grp'

      // Don't need a validator to check grouping
      const F = ValidatedForm({})

      // Rendering a group component with content to wrap
      const { container } = render(() => <F.Group>content</F.Group>)

      // Ensuring the content is wrapped with the group class
      expect(container).toMatchSnapshot('init <div.grp>')
    })
  })

  // Ensure input class is applied to inputs
  describe('inputCssClass', (): void => {
    // Ensure default behaviour persists
    test('default (false) disables input class', (): void => {
      // Don't need a validator to test the input class
      const F = ValidatedForm({})

      // Rendering an input with no properties
      const { container } = render(() => <F.Input id='test' name='input' type='text' />)

      // Ensuring the input doesn't have a class
      expect(container).toMatchSnapshot('init <input> with no class')
    })

    // Ensure input class is added to inputs
    test('string changes input class', (): void => {
      // Configuring the global input class
      ValidatedForm.globals.inputCssClass = 'txt'

      // Don't need a validator to test the input class
      const F = ValidatedForm({})

      // Rendering an input with no properties
      const { container } = render(() => <F.Input id='test' name='input' type='text' />)

      // Ensuring the input has the txt class
      expect(container).toMatchSnapshot('init <input.txt>')
    })
  })

  // Ensure invalid class is applied to inputs and groups
  describe('invalidCssClass', (): void => {
    // Ensure default behaviour persists
    test('default ("invalid") enables invalid class', (): void => {
      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validator to test invalid inputs
      const F = ValidatedForm({ input })

      // Rndering a text input and group div
      const { container } = render(() => (
        <>
          <F.Input id='test' name='input' type='text' />
          <F.Group class='content' name='input'>content</F.Group>
        </>
      ))

      // Ensuring the input doesn't become invalid by rendering
      expect(container).toMatchSnapshot('init <input> and <div>')

      // Triggering validation error
      F.validate()

      // Ensuring the input now has the default class
      expect(container).toMatchSnapshot('updates to <input.invalid> and <div.invalid>')
    })

    // Ensure invalid class is added to invalid inputs
    test('string changes invalid class', (): void => {
      // Configuring the invalid input class
      ValidatedForm.globals.invalidCssClass = 'inv'

      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validator to test invalid inputs
      const F = ValidatedForm({ input })

      // Rndering a text input and group div
      const { container } = render(() => (
        <>
          <F.Input id='test' name='input' type='text' />
          <F.Group class='content' name='input'>content</F.Group>
        </>
      ))

      // Ensuring the input doesn't become invalid by rendering
      expect(container).toMatchSnapshot('init <input> and <div>')

      // Triggering validation error
      F.validate()

      // Ensuring the input now has the invalid class
      expect(container).toMatchSnapshot('updates to <input.inv> and <div.inv>')
    })

    // Ensure invalid class can be disabled
    test('false disables invalid class', (): void => {
      // Configuring the invalid input class
      ValidatedForm.globals.invalidCssClass = false

      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validator to test invalid inputs
      const F = ValidatedForm({ input })

      // Rndering a text input and group div
      const { container } = render(() => (
        <>
          <F.Input id='test' name='input' type='text' />
          <F.Group class='content' name='input'>content</F.Group>
        </>
      ))

      // Ensuring the input doesn't become invalid by rendering
      expect(container).toMatchSnapshot('init <input> and <div>')

      // Triggering validation error
      F.validate()

      // Ensuring the input doesn't have a class
      expect(container).toMatchSnapshot('updates to <input> and <div> without .invalid')
    })
  })

  // Ensure label class is applied to labels
  describe('labelCssClass', (): void => {
    // Ensure default behaviour persists
    test('default (false) disables label class', (): void => {
      // Don't need a validator to test label classes
      const F = ValidatedForm({})

      // Rendering a label for a test input
      const { container } = render(() => <F.Label for='test' />)

      // Ensuring the label has the lbl class
      expect(container).toMatchSnapshot('init <label> with no class')
    })

    // Ensure label class is added to labels
    test('string changes label class', (): void => {
      // Configuring the global label class
      ValidatedForm.globals.labelCssClass = 'lbl'

      // Don't need a validator to test label classes
      const F = ValidatedForm({})

      // Rendering a label for a test input
      const { container } = render(() => <F.Label for='test' />)

      // Ensuring the label has the lbl class
      expect(container).toMatchSnapshot('init <label.lbl>')
    })
  })

  // Ensure prevent reset is applied to form reset events
  describe('preventReset', (): void => {
    // Ensure default behaviour persists
    test('default (true) enables event.preventDefault', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Handler that checks if event.preventDefault was called
      const reset = (event: Event): void => {
        // The onReset handler prevents the default event before running anything else
        expect(event.defaultPrevented).toEqual(true)
      }

      // Don't need a validator to test form reset
      const F = ValidatedForm({})

      // Rendering a form with a reset button
      const { getByRole } = render(() => (
        <F.Form id='test' onReset={reset}>
          <F.Reset />
        </F.Form>
      ))

      // Let the reset event handler run our expect()
      await user.click(getByRole('button') as Element)
    })

    // Ensure form reset prevent default can be disabled
    test('false disables event.preventDefault', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Configuring the global prevent form reset to disable it
      ValidatedForm.globals.preventReset = false

      // Handler that checks if event.preventDefault was called
      const reset = (event: Event): void => {
        // The onReset handler prevents the default event before running anything else
        expect(event.defaultPrevented).toEqual(false)
      }

      // Don't need a validator to test form reset
      const F = ValidatedForm({})

      // Rendering a form with a reset button
      const { getByRole } = render(() => (
        <F.Form id='test' onReset={reset}>
          <F.Reset />
        </F.Form>
      ))

      // Let the reset event handler run our expect()
      await user.click(getByRole('button') as Element)
    })
  })

  // Ensure prevent submit is applied to form submit events
  describe('preventSubmit', (): void => {
    // Ensure default behaviour persists
    test('default (true) enables event.preventDefault', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Handler that checks if event.preventDefault was called
      const submit = (event: SubmitEvent): void => {
        // The onSubmit handler prevents the default event before running anything else
        expect(event.defaultPrevented).toEqual(true)
      }

      // Don't need a validator to test form submit
      const F = ValidatedForm({})

      // Rendering a form with a submit button
      const { getByRole } = render(() => (
        <F.Form id='test' onSubmit={submit}>
          <F.Submit />
        </F.Form>
      ))

      // Let the submit event handler run our expect()
      await user.click(getByRole('button') as Element)
    })

    // Ensure form submit prevent default can be disabled
    test('false disables event.preventDefault', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Configuring the global prevent form submit to disable it
      ValidatedForm.globals.preventSubmit = false

      // Handler that checks if event.preventDefault was called
      const submit = (event: SubmitEvent): void => {
        // The onSubmit handler prevents the default event before running anything else
        expect(event.defaultPrevented).toEqual(false)

        // Need to prevent form submit otherwise JSDOM throws an error
        event.preventDefault()
      }

      // Don't need a validator to test form submit
      const F = ValidatedForm({})

      // Rendering a form with a submit button
      const { getByRole } = render(() => (
        <F.Form id='test' onSubmit={submit}>
          <F.Submit />
        </F.Form>
      ))

      // Let the submit event handler run our expect()
      await user.click(getByRole('button') as Element)
    })
  })

  // Ensure validate save is applied to form submit events
  describe('validateSave', (): void => {
    // Ensure default behaviour persists
    test('default (true) enables validation on submit', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Validator that always returns an error
      const input = jest.fn((): string => 'error')

      // Need a validator to test errors
      const F = ValidatedForm({ input })

      // Rendering a form with a submit button
      const { getByRole } = render(() => (
        <F.Form id='test'>
          <F.Submit />
        </F.Form>
      ))

      // If validate save is disabled, then submitting the form won't trigger validation
      await user.click(getByRole('button') as Element)

      // Ensuring the validator was called
      expect(input).toHaveBeenCalled()

      // Validating populates the error state
      expect(F.errors).toHaveProperty('input', 'error')
    })

    // Ensure submit can save the form state without requiring validation
    test('false disables validation on submit', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Configuring the global validate form submit to disable it
      ValidatedForm.globals.validateSave = false

      // Validator that always returns an error
      const input = jest.fn((): string => 'error')

      // Need a validator to test errors
      const F = ValidatedForm({ input })

      // Rendering a form with a submit button
      const { getByRole } = render(() => (
        <F.Form id='test'>
          <F.Submit />
        </F.Form>
      ))

      // If validate save is disabled, then submitting the form won't trigger validation
      await user.click(getByRole('button') as Element)

      // Ensuring the validator wasn't called
      expect(input).not.toHaveBeenCalled()

      // No validation means no errors
      expect(F.errors).not.toHaveProperty('input')
    })
  })

  // Ensure visible errors is applied to error spans
  describe('visibleErrors', (): void => {
    // Ensure default behaviour persists
    test('default (true) enables errors', (): void => {
      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validated input to test errors
      const F = ValidatedForm({ input })

      // Rendering a text field component
      const { container } = render(() => <F.Text id='test' name='input' />)

      // Ensuring the component does't display the error just by renderin
      expect(container).toMatchSnapshot('init <input type=text>')

      // Trigger the validation which would usually show the error span
      F.validate()

      // Ensuring the error span is displayed
      expect(container).toMatchSnapshot('shows <span.error>')
    })

    // Ensure error spans can be disabled
    test('false disables errors', (): void => {
      // Configuring the global error visibility
      ValidatedForm.globals.visibleErrors = false

      // Validator that always returns an error
      const input = (): string => 'error'

      // Need a validated input to test errors
      const F = ValidatedForm({ input })

      // Rendering a text field component
      const { container } = render(() => <F.Text id='test' name='input' />)

      // Ensuring the component does't display the error just by renderin
      expect(container).toMatchSnapshot('init <input type=text>')

      // Trigger the validation which would usually show the error span
      F.validate()

      // Ensuring the error span is not displayed
      expect(container).toMatchSnapshot('does not show <span.error>')
    })
  })

  // Ensure visible labels is applied to labels
  describe('visibleLabels', (): void => {
    // Ensure default behaviour persists
    test('default (true) enables labels', (): void => {
      // Don't need a validated input to generate a label
      const F = ValidatedForm({})

      // Rendering a text field component
      const { container } = render(() => <F.Text id='test' name='input' />)

      // Ensuring the label is rendered
      expect(container).toMatchSnapshot('init <input type=text> with <label>')
    })

    // Ensure labels can be disabled
    test('false disables labels', (): void => {
      // Configuring the global label visibility
      ValidatedForm.globals.visibleLabels = false

      // Don't need a validated input to generate a label
      const F = ValidatedForm({})

      // Rendering a text field component
      const { container } = render(() => <F.Text id='test' name='input' />)

      // Ensuring the label isn't rendered
      expect(container).toMatchSnapshot('init <input type=text> without <label>')
    })
  })
})
