/* global beforeEach describe expect test */
// Package imports
import '@testing-library/jest-dom/extend-expect'
import userEvent from '@testing-library/user-event'
import { render } from 'solid-testing-library'

// Application imports
import ValidatedForm from '..'

// Testing configuring single form instances
describe('configure instance:', (): void => {
  // Copying default global values
  const globals = { ...ValidatedForm.globals }

  // Resetting global values before each test
  beforeEach(() => Object.assign(ValidatedForm.globals, globals))

  // Ensure configuring an instance affects one form
  test('affects one form', async (): Promise<void> => {
    // Creating two forms with distinct configurations
    const A = ValidatedForm({}).configure({ inputCssClass: 'input' })
    const B = ValidatedForm({})

    // Rendering an input for both forms
    const { container: a } = render(() => <A.Input id='test-a' name='input-a' type='text' />)
    const { container: b } = render(() => <B.Input id='test-b' name='input-b' type='text' />)

    // Need to check container children since getBy would return both inputs and throw an error
    expect(a.firstChild).toHaveClass('input')

    // Ensuring the second form doesn't have the same input class
    expect(b.firstChild).not.toHaveClass('input')
  })

  // Ensure dirty class is added to dirty inputs
  test('{ dirtyCssClass: drt }', (): void => {
    // Validator that accepts any value
    const input = (): void => {}

    // Need a validated input to test configuring the dirty class
    const F = ValidatedForm({ input }).configure({ dirtyCssClass: 'drt' })

    // Rendering an input
    const { container } = render(() => <F.Input name='input' type='text' />)

    // Ensuring the input isn't dirty by default
    expect(container).toMatchSnapshot('init <input>')

    // Setting any value triggers the dirty computation
    F.set('input', 'hello')

    // Ensuring the dirty class was added
    expect(container).toMatchSnapshot('updates to <input.drt>')
  })

  // Ensure dirty class can be disabled
  test('{ dirtyCssClass: false }', (): void => {
    // Setting the global config which should be disabled
    ValidatedForm.globals.dirtyCssClass = 'drt'

    // Validator that accepts any value
    const input = (): void => {}

    // Need a validated input to test configuring the dirty class
    const F = ValidatedForm({ input }).configure({ dirtyCssClass: false })

    // Rendering an input
    const { container } = render(() => <F.Input name='input' type='text' />)

    // Ensuring the input isn't dirty by default
    expect(container).toMatchSnapshot('init <input>')

    // Setting any value triggers the dirty computation
    F.set('input', 'hello')

    // Ensuring the dirty class wasn't added
    expect(container).toMatchSnapshot('updates to <input> without .drt')
  })

  // Ensure error class is added to error spans
  test('{ errorCssClass: err }', (): void => {
    // Validator that always returns an error
    const input = (): string => 'error'

    // Need a validated input to test configuring the error span class
    const F = ValidatedForm({ input }).configure({ errorCssClass: 'err' })

    // Rendering an invisible error span
    const { container } = render(() => <F.Error name='input' />)

    // Ensuring the error isn't visible by default
    expect(container).toMatchSnapshot('init empty')

    // Triggering the validation error
    F.validate()

    // Ensuring the error span is displayed with the err class
    expect(container).toMatchSnapshot('shows <span.err>')
  })

  // Ensure error class can be disabled
  test('{ errorCssClass: false }', (): void => {
    // Setting the global config which should be disabled
    ValidatedForm.globals.errorCssClass = 'err'

    // Validator that always returns an error
    const input = (): string => 'error'

    // Need a validated input to test configuring the error span class
    const F = ValidatedForm({ input }).configure({ errorCssClass: false })

    // Rendering an invisible error span
    const { container } = render(() => <F.Error name='input' />)

    // Ensuring the error isn't visible by default
    expect(container).toMatchSnapshot('init empty')

    // Triggering the validation error
    F.validate()

    // Ensuring the error span is displayed without the err class
    expect(container).toMatchSnapshot('shows <span> without .err')
  })

  // Ensure group class is added to group divs
  test('{ groupCssClass: grp }', (): void => {
    // Don't need inputs to test configuring input grouping
    const F = ValidatedForm({}).configure({ groupCssClass: 'grp' })

    // Rendering a group with some content to wrap
    const { container } = render(() => <F.Group>content</F.Group>)

    // Ensuring the content is wrapped by the grp class
    expect(container).toMatchSnapshot('init <div.grp>')
  })

  // Ensure group class can be disabled
  test('{ groupCssClass: false }', (): void => {
    // Setting the global config which should be disabled
    ValidatedForm.globals.groupCssClass = 'grp'

    // Don't need inputs to test configuring input grouping
    const F = ValidatedForm({}).configure({ groupCssClass: false })

    // Rendering a group with some content to wrap
    const { container } = render(() => <F.Group>content</F.Group>)

    // Ensuring the content isn't wrapped by the grp class
    expect(container).toMatchSnapshot('init <div> without .grp')
  })

  // Ensure input class is added to inputs
  test('{ inputCssClass: txt }', (): void => {
    // Don't need inputs to test configuring input class
    const F = ValidatedForm({}).configure({ inputCssClass: 'txt' })

    // Rendering a blank input
    const { container } = render(() => <F.Input name='input' type='text' />)

    // Ensuring the input contains the txt class
    expect(container).toMatchSnapshot('init <input.txt>')
  })

  // Ensure input class can be disabled
  test('{ inputCssClass: false }', (): void => {
    // Setting the global config which should be disabled
    ValidatedForm.globals.inputCssClass = 'txt'

    // Don't need inputs to test configuring input class
    const F = ValidatedForm({}).configure({ inputCssClass: false })

    // Rendering a blank input
    const { container } = render(() => <F.Input name='input' type='text' />)

    // Ensuring the input doesn't contain the txt class
    expect(container).toMatchSnapshot('init <input> without .txt')
  })

  // Ensure invalid class is added to invalid inputs
  test('{ invalidCssClass: inv }', (): void => {
    // Validator that always returns an error
    const input = (): string => 'error'

    // Need a validated input to test configuring the invalid input class
    const F = ValidatedForm({ input }).configure({ invalidCssClass: 'inv' })

    // Rendering a text input to invalidate
    const { container } = render(() => <F.Input name='input' type='text' />)

    // Ensuring input isn't invalid by default
    expect(container).toMatchSnapshot('init <input>')

    // Triggering the validation error
    F.validate()

    // Ensuring the input received the invalid class
    expect(container).toMatchSnapshot('updates to <input.inv>')
  })

  // Ensure invalid class can be disabled
  test('{ invalidCssClass: false }', (): void => {
    // Setting the global config which should be disabled
    ValidatedForm.globals.invalidCssClass = 'inv'

    // Validator that always returns an error
    const input = (): string => 'error'

    // Need a validated input to test configuring the invalid input class
    const F = ValidatedForm({ input }).configure({ invalidCssClass: false })

    // Rendering a text input to invalidate
    const { container } = render(() => <F.Input name='input' type='text' />)

    // Ensuring input isn't invalid by default
    expect(container).toMatchSnapshot('init <input>')

    // Triggering the validation error
    F.validate()

    // Ensuring the input didn't received the invalid class
    expect(container).toMatchSnapshot('updates to <input> without .inv')
  })

  // Ensure label class is added to labels
  test('{ labelCssClass: lbl }', (): void => {
    // Don't need inputs to test configuring label class
    const F = ValidatedForm({}).configure({ labelCssClass: 'lbl' })

    // Rendering a label
    const { container } = render(() => <F.Label for='test' />)

    // Ensuring the label has the lbl class
    expect(container).toMatchSnapshot('init <label.lbl>')
  })

  // Ensure label class can be disabled
  test('{ labelCssClass: false }', (): void => {
    // Setting the global config which should be disabled
    ValidatedForm.globals.labelCssClass = 'lbl'

    // Don't need inputs to test configuring label class
    const F = ValidatedForm({}).configure({ labelCssClass: false })

    // Rendering a label
    const { container } = render(() => <F.Label for='test' />)

    // Ensuring the label doesn't have the lbl class
    expect(container).toMatchSnapshot('init <label> without .lbl')
  })

  // Ensure form reset prevent default can be disabled
  test('{ preventReset: false }', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Handler that checks if event.preventDefault was called
    const reset = (event: Event): void => {
      // The onReset handler prevents the default event before running anything else
      expect(event.defaultPrevented).toEqual(false)
    }

    // Don't need inputs to test configuring prevent reset
    const F = ValidatedForm({}).configure({ preventReset: false })

    // Rendering a form with a reset button
    const { getByRole } = render(() => (
      <F.Form id='test' onReset={reset}>
        <F.Reset />
      </F.Form>
    ))

    // Let the reset event handler run our expect()
    await user.click(getByRole('button') as Element)
  })

  // Ensure form reset prevent default can be restored
  test('{ preventReset: true }', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Setting the global config which should be disabled
    ValidatedForm.globals.preventReset = false

    // Handler that checks if event.preventDefault was called
    const reset = (event: Event): void => {
      // The onReset handler prevents the default event before running anything else
      expect(event.defaultPrevented).toEqual(true)
    }

    // Don't need inputs to test configuring prevent reset
    const F = ValidatedForm({}).configure({ preventReset: true })

    // Rendering a form with a reset button
    const { getByRole } = render(() => (
      <F.Form id='test' onReset={reset}>
        <F.Reset />
      </F.Form>
    ))

    // Let the reset event handler run our expect()
    await user.click(getByRole('button') as Element)
  })

  // Ensure form submit prevent default can be disabled
  test('{ preventSubmit: false }', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Handler that checks if event.preventDefault was called
    const submit = (event: SubmitEvent): void => {
      // The onSubmit handler prevents the default event before running anything else
      expect(event.defaultPrevented).toEqual(false)

      // Need to prevent form submit otherwise JSDOM throws an error
      event.preventDefault()
    }

    // Don't need inputs to test configuring prevent submit
    const F = ValidatedForm({}).configure({ preventSubmit: false })

    // Rendering a form with a submit button
    const { getByRole } = render(() => (
      <F.Form id='test' onSubmit={submit}>
        <F.Submit />
      </F.Form>
    ))

    // Let the submit event handler run our expect()
    await user.click(getByRole('button') as Element)
  })

  // Ensure form submit prevent default can be restored
  test('{ preventSubmit: true }', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Setting the global config which should be disabled
    ValidatedForm.globals.preventSubmit = false

    // Handler that checks if event.preventDefault was called
    const submit = (event: SubmitEvent): void => {
      // The onSubmit handler prevents the default event before running anything else
      expect(event.defaultPrevented).toEqual(true)
    }

    // Don't need inputs to test configuring prevent submit
    const F = ValidatedForm({}).configure({ preventSubmit: true })

    // Rendering a form with a submit button
    const { getByRole } = render(() => (
      <F.Form id='test' onSubmit={submit}>
        <F.Submit />
      </F.Form>
    ))

    // Let the submit event handler run our expect()
    await user.click(getByRole('button') as Element)
  })

  // Ensure submit can save the form state without requiring validation
  test('{ validateSave: false }', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Validator that always returns an error
    const input = (): string => 'error'

    // Need a validated input to test configuring validated save
    const F = ValidatedForm({ input }).configure({ validateSave: false })

    // Rendering a form with a submit button
    const { getByRole } = render(() => (
      <F.Form id='test'>
        <F.Submit />
      </F.Form>
    ))

    // If validate save is disabled, then submitting the form won't trigger validation
    await user.click(getByRole('button') as Element)

    // No validation means no errors
    expect(F.errors).not.toHaveProperty('input')
  })

  // Ensure submit and save functionality can be restored
  test('{ validateSave: true }', async (): Promise<void> => {
    // Setup complex user interaction
    const user = userEvent.setup()

    // Setting the global config which should be disabled
    ValidatedForm.globals.validateSave = false

    // Validator that always returns an error
    const input = (): string => 'error'

    // Need a validated input to test configuring validated save
    const F = ValidatedForm({ input }).configure({ validateSave: true })

    // Rendering a form with a submit button
    const { getByRole } = render(() => (
      <F.Form id='test'>
        <F.Submit />
      </F.Form>
    ))

    // If validate save is disabled, then submitting the form won't trigger validation
    await user.click(getByRole('button') as Element)

    // Validation means errors
    expect(F.errors).toHaveProperty('input', 'error')
  })

  // Ensure error spans can be disabled
  test('{ visibleErrors: false }', (): void => {
    // Validator that always returns an error
    const input = (): string => 'error'

    // Need a validated input to test configuring error span visibility
    const F = ValidatedForm({ input }).configure({ visibleErrors: false })

    // Rendering a text field component
    const { container } = render(() => <F.Text id='test' name='input' />)

    // Ensuring the component doesn't show the error span by default
    expect(container).toMatchSnapshot('init <input type=text>')

    // Triggering the validation which would usually show the error span
    F.validate()

    // Ensuring the error span is not rendered
    expect(container).toMatchSnapshot('does not show <span.error>')
  })

  // Ensure error spans can be restored
  test('{ visibleErrors: true }', (): void => {
    // Setting the global config which should be disabled
    ValidatedForm.globals.visibleErrors = false

    // Validator that always returns an error
    const input = (): string => 'error'

    // Need a validated input to test configuring error span visibility
    const F = ValidatedForm({ input }).configure({ visibleErrors: true })

    // Rendering a text field component
    const { container } = render(() => <F.Text id='test' name='input' />)

    // Ensuring the component doesn't show the error span by default
    expect(container).toMatchSnapshot('init <input type=text>')

    // Triggering the validation which would usually show the error span
    F.validate()

    // Ensuring the error span is rendered
    expect(container).toMatchSnapshot('shows <span.error>')
  })

  // Ensure labels can be disabled
  test('{ visibleLabels: false }', (): void => {
    // Don't need a validated input to generate a label
    const F = ValidatedForm({}).configure({ visibleLabels: false })

    // Rendering a text field component
    const { container } = render(() => <F.Text id='test' name='input' />)

    // Ensuring the label isn't rendered
    expect(container).toMatchSnapshot('init <input type=text> without <label>')
  })

  // Ensure labels can be restored
  test('{ visibleLabels: true }', (): void => {
    // Setting the global config which should be disabled
    ValidatedForm.globals.visibleLabels = false

    // Don't need a validated input to generate a label
    const F = ValidatedForm({}).configure({ visibleLabels: true })

    // Rendering a text field component
    const { container } = render(() => <F.Text id='test' name='input' />)

    // Ensuring the label is rendered
    expect(container).toMatchSnapshot('init <input type=text> with <label>')
  })
})
