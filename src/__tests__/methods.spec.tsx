/* global describe expect test */
// Package imports
import '@testing-library/jest-dom/extend-expect'
import userEvent from '@testing-library/user-event'
import { render, fireEvent } from 'solid-testing-library'
import type { UserEvent } from '@testing-library/user-event/dist/types/setup/setup'

// Application imports
import ValidatedForm, { FormInstance, FormValue } from '..'

// Testing all of the form methods
describe('ValidatedForm methods', (): void => {
  // Helper function to map complex interactions on multiple elements as async functions
  const dirty = (user: UserEvent, text: string) => async (element: Element) => {
    // Clearing the existing value
    await user.clear(element)

    // Updating the input text
    await user.type(element, text)

    // Simulating blurring the input to trigger validation
    fireEvent.change(element)
  }

  // Helper function to generate a validated form with three inputs
  const form = (values = {}): FormInstance => ValidatedForm({
    any (): void {
      // Validator that accepts any value
    },
    error (): string {
      // Validator that always returns an error
      return 'exception'
    },
    required (value): string | undefined {
      // Validator that returns an error if the value is empty
      if (value === '') return 'is required'
    }
  }, values)

  // Helper function to generate the three inputs as JSX
  const jsx = (F: FormInstance) => () => (
    <>
      <F.Text id='any' name='any' />
      <F.Text id='error' name='error' />
      <F.Text id='required' name='required' />
    </>
  )

  // Helper function that returns the three input DOM elements
  const textboxes = (getByRole: Function): { any: Element, error: Element, required: Element } => ({
    any: getByRole('textbox', { name: 'Any' }),
    error: getByRole('textbox', { name: 'Error' }),
    required: getByRole('textbox', { name: 'Required' })
  })

  // Testing the clear() method which differs from fresh() and reset()
  describe('clear() clears inputs and errors', (): void => {
    // Ensure clear() can target one input
    test('clears one', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting references to the three input DOM elements
      const { any, error, required } = textboxes(getByRole)

      // Collecting inputs to quickly iterate over
      const all = [any, error, required]

      // Making all inputs dirty with hello
      for (const element of all) await dirty(user, 'hello')(element)

      // Ensuring each DOM element has its value set
      all.forEach((dom) => expect(dom).toHaveValue('hello'))

      // Clearing a single input
      F.clear('any')

      // Ensuring the cleared input has no value
      expect(any).toHaveValue('')

      // Ensuring the error input wasn't cleared
      expect(error).toHaveValue('hello')

      // Ensuring the required input wasn't cleared
      expect(required).toHaveValue('hello')
    })

    // Ensure clear() can target multiple inputs
    test('clears many', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting references to the three input DOM elements
      const { any, error, required } = textboxes(getByRole)

      // Collecting inputs to quickly iterate over
      const all = [any, error, required]

      // Making all inputs dirty with hello
      for (const element of all) await dirty(user, 'hello')(element)

      // Ensuring each DOM element has its value set
      all.forEach((dom) => expect(dom).toHaveValue('hello'))

      // Clearing multiple inputs
      F.clear('any', 'error')

      // Ensuring the any input was cleared
      expect(any).toHaveValue('')

      // Ensuring the error input was cleared
      expect(error).toHaveValue('')

      // Ensuring the required input wasn't cleared
      expect(required).toHaveValue('hello')
    })

    // Ensure clear() can target all form inputs
    test('clears all', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting references to the three input DOM elements
      const { any, error, required } = textboxes(getByRole)

      // Collecting inputs to quickly iterate over
      const all = [any, error, required]

      // Making all inputs dirty with hello
      for (const element of all) await dirty(user, 'hello')(element)

      // Ensuring each DOM element has its value set
      all.forEach((dom) => expect(dom).toHaveValue('hello'))

      // Clearing all inputs
      F.clear()

      // Ensuring each DOM element has its value cleared
      all.forEach((dom) => expect(dom).toHaveValue(''))
    })

    // Ensure clear() clears default values
    test('clears default values', (): void => {
      // Creating our validated form with a default value for the any input
      const F = form({ any: 'hello' })

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting the any input DOM element
      const { any } = textboxes(getByRole)

      // Ensuring its default value is set
      expect(any).toHaveValue('hello')

      // Clearing the entire form but could also just target the any input
      F.clear()

      // Ensuring the default value was cleared
      expect(any).toHaveValue('')
    })

    // Ensure clear() removes error DOM
    test('clears errors', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole, queryByText } = render(jsx(F))

      // Getting the error input DOM element
      const { error } = textboxes(getByRole)

      // Dirtying the error input
      await dirty(user, 'hello')(error)

      // Ensuring the error input value is updated
      expect(error).toHaveValue('hello')

      // Ensuring the error input shows the error span
      expect(queryByText(/exception/i)).not.toBeNull()

      // Clearing all inputs
      F.clear()

      // Ensuring the error input value was cleared
      expect(error).toHaveValue('')

      // Ensuring the error span was removed since its error was cleared
      expect(queryByText(/exception/i)).toBeNull()
    })

    // Ensure clear() doesn't revert to saved values
    test('clears saved', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs with a configured dirty CSS class
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting the any input DOM element
      const { any } = textboxes(getByRole)

      // Dirtying the input with hello
      await dirty(user, 'hello')(any)

      // Ensuring the input is dirty
      expect(any).toHaveValue('hello')

      // Saving the form to update the commit state
      F.save()

      // Usually you would configure a dirty CSS class to check if a value was saved
      expect(any).toHaveValue('hello')

      // Clearing the form changes values which triggers a dirty computation
      F.clear()

      // Ensuring the value was cleared
      expect(any).toHaveValue('')
    })
  })

  // Testing the fresh() method which differs from clear() and reset()
  describe('fresh() resets a form to its initial state', (): void => {
    // Ensure fresh() overrides the
    test('overrides initial values', (): void => {
      // Creating our validated form with a default value
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting the any input DOM element
      const { any } = textboxes(getByRole)

      // Setting a new initial form state
      F.fresh({ any: 'bonjour' })

      // Ensuring the input value was updated
      expect(any).toHaveValue('bonjour')

      // Ensuring the any input was reverted to its initial value
      expect(F.dirty).not.toHaveProperty('any')
    })

    // Ensure fresh() throws errors with bad params
    test('only allows strings and arrays of strings', (): void => {
      // RegExp matching the type error message
      const values = /requires.+object.+strings.+array.+strings/i

      // Creating our validated form
      const F = form()

      // Don't accept any type other than string even if it can be casted
      expect(() => F.fresh({ any: true as any })).toThrowError(values)

      // Ensure strings a valid
      expect(() => F.fresh({ any: 'hello' })).not.toThrow()

      // Ensure arrays are valid
      expect(() => F.fresh({ any: [] })).not.toThrow()

      // Don't accept arrays that contain anything other than strings
      expect(() => F.fresh({ any: [true as any] })).toThrowError(values)

      // Ensure arrays of strings are valid
      expect(() => F.fresh({ any: ['hello'] })).not.toThrow()
    })

    // Ensure fresh() affects dirty inputs
    test('refresh dirty', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with a default value
      const F = form({ any: 'hello' })

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting the any input DOM element
      const { any } = textboxes(getByRole)

      // Making the input dirty
      await dirty(user, 'bonjour')(any)

      // Ensuring the input value was updated
      expect(any).toHaveValue('bonjour')

      // Resetting the form to its initial state
      F.fresh()

      // Ensuring the any input was reverted to its initial value
      expect(any).toHaveValue('hello')
    })

    // Ensure fresh() affects saved inputs
    test('refresh saved', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with a default value
      const F = form({ any: 'hello' })

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting the any input DOM element
      const { any } = textboxes(getByRole)

      // Making the input dirty
      await dirty(user, 'bonjour')(any)

      // Saving the any input
      F.save()

      // Ensuring the input value was updated
      expect(any).toHaveValue('bonjour')

      // Ensuring the input was saved otherwise it would have a dirty state
      expect(F.dirty).not.toHaveProperty('any')

      // Resetting the form to its initial state
      F.fresh()

      // Ensuring the any input was reverted to its initial value
      expect(any).toHaveValue('hello')

      // Ensuring even the commit state is reset
      expect(F.dirty).not.toHaveProperty('any')
    })

    // Fixed fresh()
    test('BUG FIX calling fresh() makes F.values no longer reactive', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with a default value
      const F = form({ any: 'hello' })

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting the any input DOM element
      const { any } = textboxes(getByRole)

      // Making the input dirty
      await dirty(user, 'bonjour')(any)

      // Ensuring the input value was updated
      expect(F.values.any).toEqual('bonjour')

      // Resetting the form to its initial state
      F.fresh()

      // Ensure the form was reset
      expect(F.values.any).toEqual('hello')

      // Making the input dirty
      await dirty(user, 'bonjour')(any)

      // Ensuring the input value was updated
      expect(F.values.any).toEqual('bonjour')
    })
  })

  // Testing the get() method
  describe('get() returns the current value of input(s)', (): void => {
    // Ensure get() returns a single input
    test('get one', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting references to the three input DOM elements
      const { any } = textboxes(getByRole)

      // Ensuring the returned value of get() matches the current input value
      expect(F.get('any')).toEqual({ any: '' })

      // Updating one of the inputs
      await dirty(user, 'hello')(any)

      // Ensuring the returned value is updated
      expect(F.get('any')).toEqual({ any: 'hello' })
    })

    // Ensure get() returns mulitple inputs as an object
    test('get many', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting references to the any input DOM element
      const { any } = textboxes(getByRole)

      // Ensuring each specified input is returned
      expect(F.get('any', 'error')).toEqual({ any: '', error: '' })

      // Updating the any input
      await dirty(user, 'hello')(any)

      // Ensuring the inputs are updated
      expect(F.get('any', 'error')).toEqual({ any: 'hello', error: '' })
    })

    // Ensure get() returns all form inputs as an object
    test('get all', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting references to the any input DOM element
      const { any } = textboxes(getByRole)

      // Ensuring each specified input is returned
      expect(F.get()).toEqual({ any: '', error: '', required: '' })

      // Updating the any input
      await dirty(user, 'hello')(any)

      // Ensuring each specified input is returned
      expect(F.get()).toEqual({ any: 'hello', error: '', required: '' })
    })
  })

  // Testing the id() method
  describe('id() returns the form ID', (): void => {
    // Ensure id() returns the form ID
    test('auto-generated', (): void => {
      // Don't need inputs to test form ID
      const F = ValidatedForm({})

      // Ensuring the ID is auto-generated with five alphanumerical characters prefixed with f-
      expect(F.id()).toEqual(expect.stringMatching(/^f-[0-9a-z]{5}$/))
    })

    // Ensure id() returns an input ID prefixed with the form ID
    test('returns an input id', (): void => {
      // Don't need inputs to test form ID
      const F = ValidatedForm({})

      // Ensuring the ID is prefixed with the auto-generated form ID
      expect(F.id('input')).toEqual(expect.stringMatching(/^f-[0-9a-z]{5}-input$/))
    })
  })

  // Testing the reset() method which differs from clear() and fresh()
  describe('reset() resets a form to its previous saved state', (): void => {
    // Ensure reset() can target one input
    test('resets one', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting references to the three input DOM elements
      const { any, error, required } = textboxes(getByRole)

      // Collecting inputs to quickly iterate over
      const all = [any, error, required]

      // Making all inputs dirty with hello
      for (const element of all) await dirty(user, 'hello')(element)

      // Ensuring each DOM element has its value set
      all.forEach((dom) => expect(dom).toHaveValue('hello'))

      // Resetting a single input
      F.reset('any')

      // Ensuring the reset input has no value
      expect(any).toHaveValue('')

      // Ensuring the error input wasn't reset
      expect(error).toHaveValue('hello')

      // Ensuring the required input wasn't reset
      expect(required).toHaveValue('hello')
    })

    // Ensure reset() can target multiple inputs
    test('resets many', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting references to the three input DOM elements
      const { any, error, required } = textboxes(getByRole)

      // Collecting inputs to quickly iterate over
      const all = [any, error, required]

      // Making all inputs dirty with hello
      for (const element of all) await dirty(user, 'hello')(element)

      // Ensuring each DOM element has its value set
      all.forEach((dom) => expect(dom).toHaveValue('hello'))

      // Resetting multiple inputs
      F.reset('any', 'error')

      // Ensuring the any input was reset
      expect(any).toHaveValue('')

      // Ensuring the error input was reset
      expect(error).toHaveValue('')

      // Ensuring the required input wasn't reset
      expect(required).toHaveValue('hello')
    })

    // Ensure reset() can target all form inputs
    test('resets all', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting references to the three input DOM elements
      const { any, error, required } = textboxes(getByRole)

      // Collecting inputs to quickly iterate over
      const all = [any, error, required]

      // Making all inputs dirty with hello
      for (const element of all) await dirty(user, 'hello')(element)

      // Ensuring each DOM element has its value set
      all.forEach((dom) => expect(dom).toHaveValue('hello'))

      // Resetting the form
      F.reset()

      // Ensuring each DOM element has its value reset
      all.forEach((dom) => expect(dom).toHaveValue(''))
    })

    // Ensure reset() accounts for the form's initial values
    test('resets default values', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with a default value for the any input
      const F = form({ any: 'hello' })

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting the any input DOM element
      const { any } = textboxes(getByRole)

      // Ensuring its default value is set
      expect(any).toHaveValue('hello')

      // Making the input dirty with hello
      await dirty(user, 'bonjour')(any)

      // Ensuring the input value was updated
      expect(any).toHaveValue('bonjour')

      // Resetting the form before saving it
      F.reset()

      // Ensuring the input value was reset to its previous commit state(init)
      expect(any).toHaveValue('hello')
    })

    // Ensure reset() removes error spans
    test('resets errors', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole, queryByText } = render(jsx(F))

      // Getting the error input DOM element
      const { error } = textboxes(getByRole)

      // Dirtying the error input
      await dirty(user, 'hello')(error)

      // Ensuring the error input value is updated
      expect(error).toHaveValue('hello')

      // Ensuring the error input shows the error span
      expect(queryByText(/exception/i)).not.toBeNull()

      // Resetting the form
      F.reset()

      // Ensuring the error input value was cleared
      expect(error).toHaveValue('')

      // Ensuring the error span was removed since its error was cleared
      expect(queryByText(/exception/i)).toBeNull()
    })

    // Ensure reset() accounts for saved(committed) form states
    test('resets saved', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting the any input DOM element
      const { any } = textboxes(getByRole)

      // Making the input dirty
      await dirty(user, 'hello')(any)

      // Ensuring the input value was updated
      expect(any).toHaveValue('hello')

      // Saving the input value
      F.save()

      // Clearing the input which makes it dirty
      await user.clear(any)

      // Simulating blurring the input to trigger updates
      fireEvent.change(any)

      // Ensuring the value was cleared
      expect(any).toHaveValue('')

      // Resetting will restore the values to the previous saved state
      F.reset()

      // Ensuring the any input value was restored
      expect(any).toHaveValue('hello')
    })
  })

  // Testing the save() method
  describe('save() saves the current form state', (): void => {
    // Ensure save() can target a single input
    test('saves one', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // DRYing up checking the dirty state
      const dirtied = expect.objectContaining({ new: expect.any(String), old: expect.any(String) })

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting the input DOM elements
      const { any, error, required } = textboxes(getByRole)

      // Collecting all inputs for quick iteration
      const all = [any, error, required]

      // Ensuring the inputs don't start dirty
      expect(F.dirty).toMatchObject({})

      // Making all inputs dirty
      for (const element of all) await dirty(user, 'hello')(element)

      // Ensuring the dirty computation was triggered
      expect(F.dirty).toMatchObject({ any: dirtied, error: dirtied, required: dirtied })

      // Saving one input
      F.save('any')

      // Ensuring the any input is no longer dirty
      expect(F.dirty).not.toHaveProperty('any')

      // Ensuring the other properties weren't saved
      expect(F.dirty).toMatchObject({ error: dirtied, required: dirtied })
    })

    // Ensure save() can target multiple inputs
    test('saves many', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // DRYing up checking the dirty state
      const dirtied = expect.objectContaining({ new: expect.any(String), old: expect.any(String) })

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting the input DOM elements
      const { any, error, required } = textboxes(getByRole)

      // Collecting all inputs for quick iteration
      const all = [any, error, required]

      // Ensuring the inputs don't start dirty
      expect(F.dirty).toMatchObject({})

      // Making all inputs dirty
      for (const element of all) await dirty(user, 'hello')(element)

      // Ensuring the dirty computation was triggered
      expect(F.dirty).toMatchObject({ any: dirtied, error: dirtied, required: dirtied })

      // Saving one input
      F.save('any', 'error')

      // Ensuring the any input isn't dirty
      expect(F.dirty).not.toHaveProperty('any')

      // Ensuring the error input is also not dirty
      expect(F.dirty).not.toHaveProperty('error')

      // Ensuring the other properties weren't saved
      expect(F.dirty).toMatchObject({ required: dirtied })
    })

    // Ensuring save() can target all inputs
    test('saves all', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // DRYing up checking the dirty state
      const dirtied = expect.objectContaining({ new: expect.any(String), old: expect.any(String) })

      // Creating our validated form with three inputs
      const F = form()

      // Rendering the three inputs
      const { getByRole } = render(jsx(F))

      // Getting the input DOM elements
      const { any, error, required } = textboxes(getByRole)

      // Collecting all inputs for quick iteration
      const all = [any, error, required]

      // Ensuring the inputs don't start dirty
      expect(F.dirty).toMatchObject({})

      // Making all inputs dirty
      for (const element of all) await dirty(user, 'hello')(element)

      // Ensuring the dirty computation was triggered
      expect(F.dirty).toMatchObject({ any: dirtied, error: dirtied, required: dirtied })

      // Saving all inputs
      F.save()

      // Ensuring the inputs are no longer dirty
      expect(F.dirty).toMatchObject({})
    })

    // Ensure save() will save inputs that fail validation
    test('saves invalid', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Validator that requires a value to have at least 4 characters
      const min = (str: string | string[]): undefined | string => String(str).length < 4 ? 'minimum 4 characters' : undefined

      // Need a validated input to test the save method
      const F = ValidatedForm({ min })

      // Rendering a text input
      const { getByRole } = render(() => (<F.Input name='min' type='text' />))

      // Getting the input DOM element
      const input = getByRole('textbox') as Element

      // Ensuring the input doesn't start dirty
      expect(F.dirty).not.toHaveProperty('min')

      // Ensuring the input doesn't invalid
      expect(F.errors).not.toHaveProperty('min')

      // Dirtying the input with string that fails validation
      await dirty(user, 'bob')(input)

      // Ensuring the dirty state was updated
      expect(F.dirty).toHaveProperty('min', expect.objectContaining({ new: 'bob', old: '' }))

      // Ensuring the error state was also updated
      expect(F.errors).toHaveProperty('min', 'minimum 4 characters')

      // Saving the entire form but saving just the input also works
      F.save()

      // Ensuring the dirty state no longer has our minimum input
      expect(F.dirty).not.toHaveProperty('min')

      // Ensuring the error state wasn't updated
      expect(F.errors).toHaveProperty('min', 'minimum 4 characters')
    })

    // Ensure save() removes the dirty CSS class from inputs
    test('removes dirty CSS class', async (): Promise<void> => {
      // Setup complex user interaction
      const user = userEvent.setup()

      // Using one of three validated input and setting the dirty class to test save
      const F = form().configure({ dirtyCssClass: 'drt' })

      // Rendering a text input
      const { container, getByRole } = render(() => (<F.Input id='any' name='any' type='text' />))

      // Getting the input DOM element
      const input = getByRole('textbox') as Element

      // Ensuring the input doesn't start dirty
      expect(container).toMatchSnapshot('input init without .drt')

      // Updating the input value to make it dirty
      await dirty(user, 'hello')(input)

      // Ensuring the dirty class is added
      expect(container).toMatchSnapshot('input receives .drt')

      // Saving the input to remove the dirty class
      F.save('any')

      // Ensuring the dirty class was removed
      expect(container).toMatchSnapshot('save removes .drt')
    })
  })

  // Testing the set() method
  describe('set() updates input values', (): void => {
    // Ensure proper argument handling
    test('argument handling', (): void => {
      // Need a validated input to test the set method
      const F = form()

      // Ensuring the first argument is a string or an object
      expect(() => F.set(undefined as any)).toThrowError(/requires.+(object|string)/)

      // Ensuring the input argument has a validator
      expect(() => F.set('exception', '')).toThrowError(/requires.+validator/)

      // Ensuring the second argument is required if input is a string
      expect(() => F.set('any' as any)).toThrowError(/requires.+value.+string/)

      // Ensuring the second argument can't be undefined
      expect(() => F.set('any', undefined as any)).toThrowError(/requires.+value.+string/)

      // Ensuring the object keys are validators
      expect(() => F.set({ exception: '' })).toThrowError(/requires.+validator/)

      // Ensuring the object values are strings or array of strings
      expect(() => F.set({ any: undefined as any })).toThrowError(/requires.+values.+string/)
    })

    // Ensure set(name, value) updates the input's value
    test('set value', (): void => {
      // Need a validated input to test the set method
      const F = form()

      // Rendering the form inputs
      const { getByRole } = render(jsx(F))

      // Getting the any input DOM element
      const { any } = textboxes(getByRole)

      // Ensuring the any input doesn't have an initial value
      expect(any).toHaveValue('')

      // Calling the set method with the value argument
      F.set('any', 'hello')

      // Ensuring the input value is updated
      expect(any).toHaveValue('hello')
    })

    // Ensure set({ [name]: value }) updates the input's value
    test('set object string values', (): void => {
      // Need a validated input to test the set method
      const F = form()

      // Rendering the form inputs
      const { getByRole } = render(jsx(F))

      // Getting the any input DOM element
      const { any } = textboxes(getByRole)

      // Ensuring the any input doesn't have an initial value
      expect(any).toHaveValue('')

      // Calling the set method using an object
      F.set({ any: 'hello' })

      // Ensuring the input value is updated
      expect(any).toHaveValue('hello')
    })

    // Ensure set({ [name]: value }) updates the input's value
    test('set object array values', (): void => {
      // Need a validated input to test the set method
      const F = form()

      // Rendering two checkbox inputs with the same name
      const { getByRole } = render(() => (
        <>
          <F.Checkbox name='any' value='en' label='English' />
          <F.Checkbox name='any' value='fr' label='francais' />
        </>
      ))

      // Getting the English checkbox DOM element
      const en = getByRole('checkbox', { name: 'English' })

      // Getting the French checkbox DOM element
      const fr = getByRole('checkbox', { name: 'francais' })

      // Ensuring the English checkboxes isn't checked since no initial value was passed
      expect(en).not.toBeChecked()

      // Ensuring the same for the French checkbox
      expect(fr).not.toBeChecked()

      // Setting a single value for a checkbox is valid
      F.set({ any: ['fr'] })

      // Ensuring the English box wasn't checked
      expect(en).not.toBeChecked()

      // Ensuring the French box is checked
      expect(fr).toBeChecked()
    })

    // Ensure set(name, () => value) updates the input's value
    test('set function', (): void => {
      // Need a validated input to test the set method, also specifying an initial value
      const F = form({ any: 'hello' })

      // Rendering the form inputs
      const { getByRole } = render(jsx(F))

      // Getting the any input DOM element
      const { any } = textboxes(getByRole)

      // Ensuring the any input started with our initial value
      expect(any).toHaveValue('hello')

      // Calling the set method using a function as the value argument
      F.set('any', (value: FormValue): FormValue => `${typeof value === 'string' ? value : value.join('')}!`)

      // Ensuring the input value is updated
      expect(any).toHaveValue('hello!')
    })

    // Ensure set() updates the checked property on one or multiple checkboxes
    test('set <checkbox> checked', (): void => {
      // Need a validated input to test the set method
      const F = form()

      // Rendering two checkbox inputs with the same name
      const { getByRole } = render(() => (
        <>
          <F.Checkbox name='any' value='en' label='English' />
          <F.Checkbox name='any' value='fr' label='francais' />
        </>
      ))

      // Getting the English checkbox DOM element
      const en = getByRole('checkbox', { name: 'English' })

      // Getting the French checkbox DOM element
      const fr = getByRole('checkbox', { name: 'francais' })

      // Ensuring the English checkboxes isn't checked since no initial value was passed
      expect(en).not.toBeChecked()

      // Ensuring the same for the French checkbox
      expect(fr).not.toBeChecked()

      // Setting a single value for a checkbox is valid
      F.set('any', 'fr')

      // Ensuring the English box wasn't checked
      expect(en).not.toBeChecked()

      // Ensuring the French box is checked
      expect(fr).toBeChecked()

      // Setting a single value will uncheck all other checkboxes
      F.set('any', ['en'])

      // Ensure the English box is checked
      expect(en).toBeChecked()

      // Ensuring the French box is now unchecked
      expect(fr).not.toBeChecked()

      // Setting an array of values will check all relevant inputs
      F.set('any', ['en', 'fr'])

      // Ensuring the English box is checked
      expect(en).toBeChecked()

      // Ensuring the French box is also checked
      expect(fr).toBeChecked()

      // Setting an empty array will uncheck all checkboxes
      F.set('any', [])

      // Ensuring the English box is unchecked
      expect(en).not.toBeChecked()

      // Ensuring the French box is unchecked
      expect(fr).not.toBeChecked()
    })

    test('set <radio> checked', (): void => {
      // Need a validated input to test the set method
      const F = form()

      // Rendering two radio inputs with the same name
      const { getByRole } = render(() => (
        <>
          <F.Radio name='any' value='en' label='English' />
          <F.Radio name='any' value='fr' label='francais' />
        </>
      ))

      // Getting the English radio DOM element
      const en = getByRole('radio', { name: 'English' })

      // Getting the French radio DOM element
      const fr = getByRole('radio', { name: 'francais' })

      // Ensuring the English radioes isn't checked since no initial value was passed
      expect(en).not.toBeChecked()

      // Ensuring the same for the French radio
      expect(fr).not.toBeChecked()

      // Setting the radio to French
      F.set('any', 'fr')

      // Ensuring the English box wasn't checked
      expect(en).not.toBeChecked()

      // Ensuring the French box is checked
      expect(fr).toBeChecked()

      // Setting the radio to English
      F.set('any', 'en')

      // Ensure the English box is checked
      expect(en).toBeChecked()

      // Ensuring the French box is now unchecked
      expect(fr).not.toBeChecked()

      // Setting an empty string will uncheck all radios
      F.set('any', '')

      // Ensuring the English box is unchecked
      expect(en).not.toBeChecked()

      // Ensuring the French box is unchecked
      expect(fr).not.toBeChecked()
    })

    // Ensure set() updates the selected property on a single select options
    test('set <select one> option', (): void => {
      // Need a validated input to set the set method
      const F = form()

      // Rendering a select tag that generates its option tags via the options property
      const { getByLabelText, getByRole } = render(() => (
        <F.Select id='any' name='any' options={{ en: 'English', fr: 'francais' }} />
      ))

      // Getting the any input DOM element
      const any = getByLabelText('Any')

      // Getting the English option DOM element
      const en = getByRole('option', { name: 'English' })

      // Getting the French option DOM element
      const fr = getByRole('option', { name: 'francais' })

      // Single select elements start with their first option selected
      expect(any).toHaveValue('en')

      // Calling set to change the option to French
      F.set('any', 'fr')

      // Can't use toBeChecked for some reason
      expect(en).toHaveProperty('selected', false)

      // Ensure only the french option is selected
      expect(fr).toHaveProperty('selected', true)

      // Ensuring the value of the select is also updated
      expect(any).toHaveValue('fr')

      // Changing the option back to English
      F.set('any', 'en')

      // Ensuring the English option is selected
      expect(en).toHaveProperty('selected', true)

      // Ensuring the French option is no longer selected
      expect(fr).toHaveProperty('selected', false)

      // Ensuring the value of the select updated
      expect(any).toHaveValue('en')
    })

    // Ensure set() updates the selected property on multiple select options
    test('set <select multiple> options', (): void => {
      // Creating our validated form with three inputs
      const F = form()

      // Rendering a select multiple tag
      const { getByLabelText, getByRole } = render(() => (
        <F.Select multiple name='any' options={{ en: 'English', fr: 'francais' }} />
      ))

      // Getting the select DOM element
      const any = getByLabelText('Any')

      // Getting the English select option
      const en = getByRole('option', { name: 'English' })

      // Getting the French select option
      const fr = getByRole('option', { name: 'francais' })

      // Multiple select elements start with no options selected
      expect(any).toHaveValue([])

      // Setting only the English option as selected
      F.set('any', 'en')

      // Ensuring select's value has the English option selected
      expect(any).toHaveValue(['en'])

      // Ensuring the English option is selected
      expect(en).toHaveProperty('selected', true)

      // Ensuring the French option wasn't selected
      expect(fr).toHaveProperty('selected', false)

      // Setting only the French option as selected
      F.set('any', ['fr'])

      // Ensuring the select value is also just French
      expect(any).toHaveValue(['fr'])

      // Ensuring the English option is no longer selected
      expect(en).toHaveProperty('selected', false)

      // Ensuring the French option is now selected
      expect(fr).toHaveProperty('selected', true)

      // Selecting both English and French options
      F.set('any', ['en', 'fr'])

      // Ensuring the select value has both options selected
      expect(any).toHaveValue(['en', 'fr'])

      // Ensuring the English option is selected
      expect(en).toHaveProperty('selected', true)

      // Ensuring the French option is selected
      expect(fr).toHaveProperty('selected', true)

      // Clearing the selected options
      F.set('any', [])

      // Ensuring the select value has also been cleared
      expect(any).toHaveValue([])

      // Ensuring the English option is no longer selected
      expect(en).toHaveProperty('selected', false)

      // Ensuring the French option is no longer selected
      expect(fr).toHaveProperty('selected', false)
    })
  })

  // Testing the validate() method
  describe('validate() runs validation', (): void => {
    // Ensure validate() can target one input
    test('validates one', (): void => {
      // Validator that always returns an error
      const errors = (): string => 'error'

      // Need validated inputs to test validation
      const F = ValidatedForm({ any: errors, error: errors, required: errors })

      // Ensuring our error state is empty
      expect(F.errors).toMatchObject({})

      // Validating a single input
      F.validate('any')

      // Ensuring the validated input has the expected error message
      expect(F.errors).toHaveProperty('any', 'error')

      // Ensuring the error input wasn't validated
      expect(F.errors).not.toHaveProperty('error')

      // Ensuring the required input wasn't validated
      expect(F.errors).not.toHaveProperty('required')
    })

    // Ensure validate() can target multiple inputs
    test('validates many', (): void => {
      // Validator that always returns an error
      const errors = (): string => 'error'

      // Need validated inputs to test validation
      const F = ValidatedForm({ any: errors, error: errors, required: errors })

      // Ensuring our error state is empty
      expect(F.errors).toMatchObject({})

      // Validating multiple inputs
      F.validate('any', 'error')

      // Ensuring the any input was validated
      expect(F.errors).toHaveProperty('any', 'error')

      // Ensuring the error input was validated
      expect(F.errors).toHaveProperty('error', 'error')

      // Ensuring the required input wasn't validated
      expect(F.errors).not.toHaveProperty('required')
    })

    // Ensure validate() can target all inputs
    test('validates all', (): void => {
      // Validator that always returns an error
      const errors = (): string => 'error'

      // Need validated inputs to test validation
      const F = ValidatedForm({ any: errors, error: errors, required: errors })

      // Ensuring our error state is empty
      expect(F.errors).toMatchObject({})

      // Validating all inputs
      F.validate()

      // Ensuring the any input was validated
      expect(F.errors).toHaveProperty('any', 'error')

      // Ensuring the error input was validated
      expect(F.errors).toHaveProperty('error', 'error')

      // Ensuring the required input was validated
      expect(F.errors).toHaveProperty('required', 'error')
    })

    // Ensure validate() shows the error DOM
    test('shows errors', (): void => {
      // Validator that always returns an error
      const errors = (): string => 'error'

      // Need a validated input to test validation
      const F = ValidatedForm({ error: errors })

      // Rendering the error input
      const { container } = render(() => (<F.Text id='error' name='error' />))

      // Ensuring the error isn't visible until validation is ran
      expect(container).toMatchSnapshot('no error span')

      // Validating the input that always returns an error
      F.validate()

      // Ensuring the error is now visible
      expect(container).toMatchSnapshot('visible error span')
    })
  })
})
