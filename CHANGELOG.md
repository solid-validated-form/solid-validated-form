# Changelog

## 1.5.0 - 2022-02-26

- Removed `dirtyCssClass` and `invalidCssClass` inheritance from `Form` components
- Fixed `Form` components not applying `classList`

Although initially released as a feature, real life application revealed issues with styles cascading to all form components when it would otherwise be unwanted. The `dirtyCssClass` and `invalidCssClass` classes are no longer applied automatically to the `Form` component since they couldn't be disabled. This functionality can be restored using `F.isDirty` and `F.isValid`:

```js
const dirtyCssClass = 'border border-warning'
const invalidCssClass = 'border border-red'
const F = ValidatedForm({}).configure({ dirtyCssClass, invalidCssClass })
```

```tsx
<F.Form classList={{ [dirtyCssClass]: F.isDirty, [invalidCssClass]: !F.isValid }}>
  ...
</F.Form>
```

## 1.4.2 - 2022-02-17

- Fixed inputs not reacting to change events triggered by browser spinners
- Fixed checkbox inputs pushing a null value in its state
- Fixed `List` components generating non-matching IDs for labels and inputs

## 1.4.1 - 2022-02-15

- Updated README.md and demo examples with v1.4.0 changes

## 1.4.0 - 2022-02-06

- Fixed CSS class inheritance causing duplicate/unwanted classes
- Added `classes` to `Field` components for specifying group, label, input, and error component CSS classes
- Added `List` component to create lists of inputs
- Added `CheckboxList` and `RadioList` helper components
- Improved a few error messages
- Improved global and instance configuration testing
- Improved tutorials

### `<Field classes={...}>`

```tsx
<F.Text name='username' classes={{
  group: 'mb-3',
  label: 'form-label',
  input: 'form-control',
  error: 'invalid-feedback'
}} />
```

```html
<!-- Generated DOM -->
<div class="mb-3">
  <label class="form-label">Username</label>
  <input name="username" type="text" class="form-control" />
</div>
```

```html
<!-- Generated DOM when validation fails -->
<div class="mb-3 invalid">
  <label class="form-label">Username</label>
  <input name="username" type="text" class="form-control invalid" />
  <span class="invalid-feedback">Username is required</span>
</div>
```

### `<List>`

```tsx
<F.List
  name='accept'
  component={F.Checkbox}
  options={{
    tos: 'I accept the Terms of Use',
    metrics: 'I accept cookies for site performance',
    third: 'I accept cookies for 3rd party tracking'
  }}
  classes={{
    group: 'mb-3',
    label: 'form-label',
    component: {
      group: 'form-check',
      label: 'form-check-label',
      input: 'form-check-input'
    },
    error: 'invalid-feedback'
  }}
/>
```

```html
<!-- Generated DOM when validation fails -->
<div class="mb-3 invalid">
  <label for="f-00000-accept" class="form-label">Accept</label>

  <div class="form-check">
    <label for="f-00000-accept-1" class="form-check-label">I accept the Terms of Use</label>
    <input id="f-00000-accept-1" type="checkbox" name="accept" value="tos" class="form-check-input invalid" />
  </div>

  <div class="form-check">
    <label for="f-00000-accept-2" class="form-check-label">I accept cookies for site performance</label>
    <input id="f-00000-accept-2" type="checkbox" name="accept" value="metrics" class="form-check-input invalid" />
  </div>

  <div class="form-check">
    <label for="f-00000-accept-3" class="form-check-label">I accept cookies for 3rd party tracking</label>
    <input id="f-00000-accept-3" type="checkbox" name="accept" value="third" class="form-check-input invalid" />
  </div>

  <span class="invalid-feedback">Accept must include Terms of Use</span>
</div>
```

### `<CheckboxList>`

```tsx
<F.CheckboxList name='accept' options={{
  tos: 'I accept the Terms of Use',
  metrics: 'I accept cookies for site performance',
  third: 'I accept cookies for 3rd party tracking'
}} />
```

```html
<!-- Generated DOM -->
<label for="f-00000-accept">Accept</label>

<label for="f-00000-accept-1">I accept the Terms of Use</label>
<input id="f-00000-accept-1" type="checkbox" name="accept" value="tos" />

<label for="f-00000-accept-2">I accept cookies for site performance</label>
<input id="f-00000-accept-2" type="checkbox" name="accept" value="metrics" />

<label for="f-00000-accept-3">I accept cookies for 3rd party tracking</label>
<input id="f-00000-accept-3" type="checkbox" name="accept" value="third" />
```

### `<RadioList>`

```tsx
<F.RadioList name='language' options={{
  en: 'English',
  fr: 'francais',
  both: 'Bilingual/bilingue'
}} />
```

```html
<!-- Generated DOM -->
<label for="f-00000-language">Language</label>

<label for="f-00000-language-1">English</label>
<input id="f-00000-language-1" type="radio" name="language" value="en" />

<label for="f-00000-language-2">francais</label>
<input id="f-00000-language-2" type="radio" name="language" value="fr" />

<label for="f-00000-language-3">Bilingual/bilingue</label>
<input id="f-00000-language-3" type="radio" name="language" value="both" />
```

## 1.3.0 - 2022-01-31

- Updated solid-js versions for dev and peer dependencies
- Added `is.fn()` to utils
- Removed `is.set()` from utils
- Improved initialization code and comment coverage
- Expanded functionality of `fresh()` allowing it to override initial values
- Added `values` property to `<Form>` components for setting initial values
- Added reactive `isDirty` property to ValidatedForm instances
- Added reactive `isValid` property to ValidatedForm instances

### `F.fresh()`

Setting initial values when constructing the form:

```js
const F = ValidatedForm({
  username () {},
  nickname () {},
  password () {}
}, {
  username: 'user@domain.tld'
})

// Resets the form to its initial values
// { username: 'user@domain.tld', nickname: '', password: '' }
F.fresh()

// Replaces the initial values and resets the form to said values
// { username: '', nickname: 'root', password: '' }
F.fresh({ nickname: 'root' })

// Resets the form to the new initial values
// { username: '', nickname: 'root', password: '' }
F.fresh()
```

### `<Form values={...}>`

Creating a form with no initial values:

```js
const F = ValidatedForm({
  username () {}
})
```

Passing an object of strings as initial values:

```tsx
<F.Form values={{ username: 'user@domain.tld' }}>
  <F.Text name='username' />
</F.Form>
```

Using `createResource` and `Suspense`:

```js
// Mock resource that emulates a two second API call
const [payload] = createResource(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve({ username: 'user@domain.tld' }), 2000)
  })
})
```

```tsx
<Suspense fallback={<div>Loading...</div>}>
  <F.Form values={payload()}>
    <F.Text name='username' />
  </F.Form>
</Suspense>
```

### `F.isDirty`

```js
const onClickCancel = () => {
  if (F.isDirty && confirm('Discard changes?')) F.reset()
}
```

```tsx
<Show when={F.isDirty}>
  <div>You have unsaved changes.</div>
</Show>
```

### `F.isValid`

```js
const onClickSave = () => {
  // Prefer calling `F.validate()` to ensure validation is run at least once
  if (F.isValid) F.save()
}
```

```tsx
<Show when={!F.isValid}>
  <div>Please correct the errors below.</div>
</Show>
```

## 1.2.3 - 2022-01-15

- Fixed `fresh()` breaking `F.values` reactivity

## 1.2.2 - 2022-01-15

- Fixed Jest throwing ESM error
- Updated keywords and a few packages in package.json

## 1.2.1 - 2021-09-18

- Updated documentation
- Updated packages in package.json

## 1.2.0 - 2021-09-03

- Added `get()` to return one or more form values as an object
- Improved README and updated documentation

### `F.get()`

```js
const F = ValidatedForm({ username () {}, nickname () {}, password () {} })

// Returns { username: '' }
F.get('username')

// Returns { username: '', password: '' }
F.get('username', 'password')

// Returns { username: '', nickname: '', password: '' }
F.get()
```

## 1.1.1 - 2021-09-01

- Replaced `expunge` with Solid's `splitProps`

## 1.1.0 - 2021-08-29

- Added the `configure` helper method to the `ValidatedForm.globals` object

### `ValidatedForm.globals.configure()`

```js
// Configuring the global object
ValidatedForm.globals.configure({
  inputCssClass: 'form-control',
  labelCssClass: 'control-label'
})

// Manually configuring each global
ValidatedForm.globals.inputCssClass = 'form-control'
ValidatedForm.globals.labelCssClass = 'control-label'
```

## 1.0.2 - 2021-08-28

- Fixed `createState` renamed to `createStore` and removed redundant imports
- Fixed Parcel complaining about browser import/exports
- Fixed some documentation

## 1.0.1 - 2021-08-23

- Created GitLab Pages
- Fixed tutorial URLs in README
- Fixed some documentation

## 1.0.0 - 2021-08-23

- Bumped to version 1.0.0 since `solid-js` is already past its major release

## 0.1.1 - 2021-08-23

- Fixed missing dev dependencies
- Fixed GitLab URLS in README

## 0.1.0 - 2021-08-23

- Initial public release
